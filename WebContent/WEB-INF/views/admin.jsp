<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title><spring:message code="title" /></title>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/bootstrap.css" />" />
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<!-- NAVIGATION BAR -->
	<jsp:include page="navbar.jsp"></jsp:include>

	<div class="jumbotron">
		<h1 class="display-3 text-container-main">
			<spring:message code="admin.welcome" />
		</h1>
		<p class="text-container-main">
			<spring:message code="admin.description" />
		</p>
		<hr class="my-4 text-container-main">
		
		<c:if test="${not empty deleteWorkshopSuccess }">
			<div
				class="alert alert-dismissible alert-success text-container-main">
				<spring:message code="${deleteWorkshopSuccess }" /></div>
		</c:if>
		<c:if test="${not empty deleteWorkshopError}">
			<div
				class="alert alert-dismissible alert-primary text-container-main">
				<spring:message code="${deleteWorkshopError }" /></div>
		</c:if>
		<c:if test="${not empty deleteControlPointSuccess }">
			<div
				class="alert alert-dismissible alert-success text-container-main">
				<spring:message code="${deleteControlPointSuccess }" /></div>
		</c:if>
		<c:if test="${not empty deleteControlPointError}">
			<div
				class="alert alert-dismissible alert-primary text-container-main">
				<spring:message code="${deleteControlPointError }" /></div>
		</c:if>
		<c:if test="${not empty requestResolveSuccess}">
			<div
				class="alert alert-dismissible alert-success text-container-main">
				<spring:message code="${requestResolveSuccess }" /></div>
		</c:if>

		<div class="Row text-container-main">
			<div class="Column">
				<img src="resources/images/workshop_icon.png" class="icon">
			</div>
			<div class="Column">
				<h3>
					<spring:message code="workshops" />
				</h3>
				<p class="lead">
					<spring:message code="home.workshops.description" />
				</p>
				<form:form class="form-inline my-2 my-lg-0"
					action="adminGetWorkshops" method="POST">
					<input class="form-control mr-sm-2" type="text" name="workshopName" />
					<br>
					<div>
						<input class="btn btn-info btn-lg" type="submit"
							value=<spring:message code="search" /> />
					</div>
				</form:form>
			</div>
		</div>
		<c:if test="${showWorkshops == true}">
			<br>
			<c:set var="showWorkshops" scope="session" value="${false}" />
			<c:choose>
				<c:when test="${empty workshops}">
					<div
						class="alert alert-dismissible alert-primary text-container-main">
						<spring:message code="${getWorkshopsResponse }" /></div>
				</c:when>
				<c:otherwise>
					<div class="text-container-main">
						<table class="table table-hover">
							<thead>
								<tr class="table-info">
									<th scope="col"><spring:message code="id" /></th>
									<th scope="col"><spring:message code="name" /></th>
									<th scope="col"><spring:message code="address.street" /></th>
									<th scope="col"><spring:message code="address.city" /></th>
									<th scope="col"><spring:message code="owner" /></th>
									<th scope="col"><spring:message code="status" /></th>
									<th scope="col"></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${workshops}" var="workshop">
									<tr class="table-light">
										<td><c:out value="${workshop.id}" /></td>
										<td><c:out value="${workshop.name}" /></td>
										<td><c:out value="${workshop.address.street}" /></td>
										<td><c:out value="${workshop.address.city}" /></td>
										<td><c:out value="${workshop.owner.userName}" /></td>
										<td><spring:message code="${workshop.status }"/></td>
										<td><a
											href="/CarWorkshop/adminDeleteWorkshop/${workshop.id }"
											onclick="return confirm('Are you sure you want to delete this item?')">
												<button type="button" id="deleteWorkshop"
													class="btn btn-outline-primary">
													<spring:message code="delete" />
												</button>
										</a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</c:otherwise>
			</c:choose>
		</c:if>
		<hr class="my-4 text-container-main">

		<div class="Row text-container-main">
			<div class="Column">
				<img src="resources/images/control-point_icon.png" class="icon">
			</div>
			<div class="Column">
				<h3>
					<spring:message code="control_points" />
				</h3>
				<p class="lead">
					<spring:message code="home.control_points.description" />
				</p>
				<form:form class="form-inline my-2 my-lg-0"
					action="getAdminVehicleControlPoints" method="POST">
					<input class="form-control mr-sm-2" type="text"
						name="controlPointName" width="200" />
					<br>
					<div>
						<input class="btn btn-info btn-lg" type="submit"
							value=<spring:message code="search" /> />
					</div>
				</form:form>
			</div>
		</div>
		<c:if test="${showControlPointsResponse == true}">
			<br>
			<c:set var="showControlPointsResponse" scope="session"
				value="${false}" />
			<c:choose>
				<c:when test="${empty controlPoints}">
					<div
						class="alert alert-dismissible alert-primary text-container-main">
						<spring:message code="${getControlPointsResponse }" /></div>
				</c:when>
				<c:otherwise>
					<div class="text-container-main">
						<table class="table table-hover">
							<thead>
								<tr class="table-info">
									<th scope="col"><spring:message code="id" /></th>
									<th scope="col"><spring:message code="name" /></th>
									<th scope="col"><spring:message code="address.street" /></th>
									<th scope="col"><spring:message code="address.city" /></th>
									<th scope="col"><spring:message code="owner" /></th>
									<th scope="col"><spring:message code="status" /></th>
									<th scope="col"></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${controlPoints}" var="controlPoint">
									<tr class="table-light">
										<td><c:out value="${controlPoint.id}" /></td>
										<td><c:out value="${controlPoint.name}" /></td>
										<td><c:out value="${controlPoint.address.street}" /></td>
										<td><c:out value="${controlPoint.address.city}" /></td>
										<td><c:out value="${controlPoint.owner.userName}" /></td>
										<td><spring:message code="${controlPoint.status }"/></td>
										<td><a
											href="/CarWorkshop/adminDeleteControlPoint/${controlPoint.id }"><button type="button" class="btn btn-outline-primary">
												<spring:message code="delete" />
											</button></a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</c:otherwise>
			</c:choose>
		</c:if>
		<hr class="my-4 text-container-main">

		<!-- ADD NEW ELEMENTS -->
		<div class="text-container-main">
			<h3>
				<spring:message code="admin.add" />
			</h3>
			<p>
				<spring:message code="admin.add.description" />
			</p>
			<div class="Column" style="padding-left: 0px">
				<a href="/CarWorkshop/getAddWorkshopView"
					class="btn btn-info btn-lg"><spring:message
						code="admin.add.workshop" /></a>
			</div>
			<div class="Column">
				<a href="/CarWorkshop/getAddControlPointView"
					class="btn btn-info btn-lg" href="#"><spring:message
						code="admin.add.control_point" /></a>
			</div>
		</div>
		<hr class="my-4 text-container-main">

		<!-- REQUESTS -->
		<div class="text-container-main">
			<h3>
				<spring:message code="requests" />
			</h3>
			<p>
				<spring:message code="requests.current" />
			</p>
			<form:form class="form-inline my-2 my-lg-0" action="getRequests"
				method="GET">
				<div>
					<input class="btn btn-info btn-lg" type="submit"
						value=<spring:message code="search" /> />
				</div>
			</form:form>
		</div>
		<c:if test="${showRequests == true}">
			<br>
			<c:set var="showRequests" scope="session" value="${false}" />
			<c:choose>
				<c:when test="${empty requests}">
					<div
						class="alert alert-dismissible alert-primary text-container-main">
						<spring:message code="admin.request.none" />
					</div>
				</c:when>
				<c:otherwise>
					<div class="text-container-main">
						<table class="table table-hover">
							<thead>
								<tr class="table-info">
									<th scope="col"><spring:message code="id" /></th>
									<th scope="col"><spring:message code="request.subject" /></th>
									<th scope="col"><spring:message code="request.content" /></th>
									<th scope="col"><spring:message code="date" /></th>
									<th scope="col"><spring:message code="time" /></th>
									<th scope="col"><spring:message code="sender" /></th>
									<th scope="col"><spring:message code="status" /></th>
									<th scope="col"></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${requests}" var="request">
									<tr class="table-light">
										<td><c:out value="${request.id}" /></td>
										<td><c:out value="${request.subject}" /></td>
										<td><c:out value="${request.content}" /></td>
										<td><c:out value="${request.date.toLocalDate()}" /></td>
										<td><c:out value="${request.date.toLocalTime()}" /></td>
										<td><c:out value="${request.sender.userName}" /></td>
										<td><spring:message code="${request.status}" /></td>
										<td><a
											href="/CarWorkshop/adminResolveRequest/${request.id }"
											onclick="return confirm('Are you sure you want to delete this item?')"><button
													type="button" class="btn btn-outline-primary">
													<spring:message code="request.resolve" />
												</button></a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</c:otherwise>
			</c:choose>
		</c:if>
	</div>

	<!-- FOOTER -->
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>