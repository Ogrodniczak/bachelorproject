<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${ not empty reserveVisitLoginError}">
	<div class="alert alert-dismissible alert-primary">
		<spring:message code="${reserveVisitLoginError }" />
	</div>
</c:if>
<c:if test="${ not empty reserveVisitOrderError}">
	<div class="alert alert-dismissible alert-primary">
		<spring:message code="${reserveVisitOrderError }" />
	</div>
</c:if>
<c:if test="${ not empty reserveVisitTimeError}">
	<div class="alert alert-dismissible alert-primary">
		<spring:message code="${reserveVisitTimeError }" />
	</div>
</c:if>
<c:if test="${ not empty reserveVisitError}">
	<div class="alert alert-dismissible alert-primary">
		<spring:message code="${reserveVisitError }" />
	</div>
</c:if>
<c:if test="${ not empty reserveVisitSuccess}">
	<div class="alert alert-dismissible alert-success">
		<spring:message code="${reserveVisitSuccess }" />
	</div>
</c:if>
<c:if test="${ not empty cancelVisitLoginError}">
	<div class="alert alert-dismissible alert-primary">
		<spring:message code="${cancelVisitLoginError }" />
	</div>
</c:if>
<c:if test="${ not empty cancelVisitError}">
	<div class="alert alert-dismissible alert-primary">
		<spring:message code="${cancelVisitError }" />
	</div>
</c:if>
<c:if test="${ not empty cancelVisitSuccess}">
	<div class="alert alert-dismissible alert-success">
		<spring:message code="${cancelVisitSuccess }" />
	</div>
</c:if>