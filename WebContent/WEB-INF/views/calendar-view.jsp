<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title><spring:message code="title" /></title>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/bootstrap.css" />" />
</head>
<body>
	<!-- NAVIGATION BAR -->
	<jsp:include page="navbar.jsp"></jsp:include>

	<!-- MAIN CONTAINER -->
	<div class="jumbotron">
		<h1 class="display-3">
			&#128197
			<spring:message code="calendars" />
		</h1>
		<c:if test="${not empty itemName }">
			<h1>${itemName }</h1>
		</c:if>
		<c:choose>
			<c:when test="${not empty order }">
				<hr class="my-4">
				<div class="alert alert-dismissible alert-info">
					<h3>
						<spring:message code="order" />
						: <strong>${order.id }</strong>
					</h3>
					<h3>
						<spring:message code="accessories" />
						:
						<c:forEach items="${order.accessories }" var="accessory"
							varStatus="status">
							<strong>${accessory.key.name}</strong>
							<c:if test="${not status.last}">
								<c:out value=", " />
							</c:if>
						</c:forEach>
					</h3>
					<h3>
						<spring:message code="services" />
						:
						<c:forEach items="${order.services}" var="service"
							varStatus="status">
							<strong>${service.key.name}
							<c:if test="${not status.last}">
								<c:out value=", " />
							</c:if>
							</strong>
						</c:forEach>
					</h3>
					<h3>
						<spring:message code="price" />
						: <strong>${order.cost }</strong>
					</h3>
				</div>
				<c:set var="orderId" value="${order.id }"></c:set>
			</c:when>
			<c:otherwise>
				<c:set var="orderId" value="0"></c:set>
			</c:otherwise>

		</c:choose>

		<!-- MESSAGES -->
		<jsp:include page="calendar-view-messages.jsp"></jsp:include>

		<c:choose>
			<c:when test="${not empty stations}">
				<c:forEach items="${stations}" var="station">
					<hr class="my-4">
					<h2>
						&#9873
						<spring:message code="station" />
						${station.number}
					</h2>
					<br>
					<ul class="pagination">
						<c:if test="${station.workshop != null}">
							<c:choose>
								<c:when test="${currentIndex > 6 and station.days.size() > 7}">
									<li class="page-item"><a class="page-link"
										href="/CarWorkshop/clientGetCalendarView/workshop/${station.workshop.id}/${orderId}/${currentIndex - 7}">&laquo;
											<spring:message code="week.previous" />
									</a></li>
								</c:when>
								<c:when
									test="${currentIndex == -1 and station.days.size() >= 14 and station.days.size() > 7}">
									<li class="page-item"><a class="page-link"
										href="/CarWorkshop/clientGetCalendarView/workshop/${station.workshop.id}/${orderId}/${station.days.size() - 14}">&laquo;
											<spring:message code="week.previous" />
									</a></li>
								</c:when>
								<c:otherwise>
									<li class="page-item disabled"><a class="page-link"
										href="#">&laquo; <spring:message code="week.previous" /></a></li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when
									test="${currentIndex != -1 and currentIndex < station.days.size()-7 and station.days.size() > 7}">
									<li class="page-item"><a class="page-link"
										href="/CarWorkshop/clientGetCalendarView/workshop/${station.workshop.id}/${orderId}/${currentIndex + 7}"><spring:message
												code="week.next" /> &raquo;</a></li>
								</c:when>
								<c:otherwise>
									<li class="page-item disabled"><a class="page-link"
										href="#"><spring:message code="week.next" /> &raquo;</a></li>
								</c:otherwise>
							</c:choose>
						</c:if>
						<c:if test="${station.vehicleControlPoint != null}">
							<c:choose>
								<c:when test="${currentIndex > 6 and station.days.size() > 7}">
									<li class="page-item"><a class="page-link"
										href="/CarWorkshop/clientGetCalendarView/controlPoint/${station.vehicleControlPoint.id}/${orderId}/${currentIndex - 7}">&laquo;
											<spring:message code="week.previous" />
									</a></li>
								</c:when>
								<c:when
									test="${currentIndex == -1 and station.days.size() >= 14 and station.days.size() > 7}">
									<li class="page-item"><a class="page-link"
										href="/CarWorkshop/clientGetCalendarView/controlPoint/${station.vehicleControlPoint.id}/${orderId}/${station.days.size() - 14}">&laquo;
											<spring:message code="week.previous" />
									</a></li>
								</c:when>
								<c:otherwise>
									<li class="page-item disabled"><a class="page-link"
										href="#">&laquo; <spring:message code="week.previous" /></a></li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when
									test="${currentIndex != -1 and currentIndex < station.days.size()-7 and station.days.size() > 7}">
									<li class="page-item"><a class="page-link"
										href="/CarWorkshop/clientGetCalendarView/controlPoint/${station.vehicleControlPoint.id}/${orderId}/${currentIndex + 7}"><spring:message
												code="week.next" /> &raquo;</a></li>
								</c:when>
								<c:otherwise>
									<li class="page-item disabled"><a class="page-link"
										href="#"><spring:message code="week.next" /> &raquo;</a></li>
								</c:otherwise>
							</c:choose>
						</c:if>
					</ul>
					<hr class="my-4">
					<div class="Row">
						<c:forEach items="${station.calendarVisitMap}" var="outerMap">
							<div class="Column">
								<table class="table table-hover">
									<thead>
										<tr class="table-info">
											<th scope="col">${outerMap.key.date}</th>
											<th scope="col"><spring:message code="client" /></th>
											<th scope="col"><spring:message
													code="${outerMap.key.date.dayOfWeek }" /></th>
										</tr>
									</thead>
									<tbody>

										<c:forEach items="${outerMap.value}" var="innerMap">
											<c:if test="${empty innerMap.value }">
												<tr class="table-success">
													<td><c:out value="${innerMap.key}" /></td>
													<td></td>
													<c:choose>
														<c:when test="${not empty order && order != null}">
															<td><a
																href="/CarWorkshop/reserveVisit/${outerMap.key.id}/${innerMap.key }/${order.id}/${currentIndex}"><button
																		type="button" class="btn btn-outline-success">
																		<spring:message code="reserve" />
																	</button></a></td>
														</c:when>
														<c:otherwise>
															<td><a
																href="/CarWorkshop/reserveVisit/${outerMap.key.id}/${innerMap.key }/0/${currentIndex}"><button
																		type="button" class="btn btn-outline-success">
																		<spring:message code="reserve" />
																	</button></a></td>
														</c:otherwise>
													</c:choose>
												</tr>
											</c:if>
											<c:if test="${not empty innerMap.value }">
												<c:if test="${innerMap.value.status == 'SCHEDULED'}">
													<c:choose>
														<c:when
															test="${not empty sessionUser && sessionUser.type == 'CLIENT' && sessionUser.id == innerMap.value.client.id}">
															<tr class="table-light">
																<td><c:out value="${innerMap.key}" /></td>
																<td><c:out
																		value="${innerMap.value.client.userName }" /></td>
																<td><a
																	href="/CarWorkshop/cancelVisit/${item}/${innerMap.value.id}/${currentIndex}"><button
																			type="button" class="btn btn-outline-primary">
																			<spring:message code="cancel" />
																		</button></a></td>
															</tr>
														</c:when>
														<c:otherwise>
															<tr class="table-active">
																<td><c:out value="${innerMap.key}" /></td>
																<td><c:out
																		value="${innerMap.value.client.userName }" /></td>
																<td></td>
															</tr>
														</c:otherwise>
													</c:choose>
												</c:if>
											</c:if>
											<c:if test="${not empty innerMap.value }">
												<c:if test="${innerMap.value.status == 'UNAVAILABLE'}">
													<tr class="table-primary">
														<td><c:out value="${innerMap.key}" /></td>
														<td></td>
														<td><spring:message code="not_available" /></td>
													</tr>
												</c:if>
											</c:if>
											<c:if test="${not empty innerMap.value }">
												<c:if test="${innerMap.value.status == 'TERMINATED'}">
													<tr class="table-danger">
														<td><c:out value="${innerMap.key}" /></td>
														<td></td>
														<td><spring:message code="TERMINATED" /></td>
													</tr>
												</c:if>
											</c:if>
											<c:if test="${not empty innerMap.value }">
												<c:if test="${innerMap.value.status == 'OLD'}">
													<tr class="table-dark">
														<td><c:out value="${innerMap.key}" /></td>
														<td></td>
														<td></td>
													</tr>
												</c:if>
											</c:if>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</c:forEach>
					</div>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<div class="alert alert-dismissible alert-primary">
					<spring:message code="stations.none" />
				</div>
			</c:otherwise>
		</c:choose>
	</div>

	<!-- FOOTER -->
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>