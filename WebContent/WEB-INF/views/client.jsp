<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title><spring:message code="title" /></title>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/bootstrap.css" />" />
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<!-- NAVIGATION BAR -->
	<jsp:include page="navbar.jsp"></jsp:include>

	<div class="jumbotron">
		<h1 class="display-3 text-container-main">
			<spring:message code="client.page" />
		</h1>
		<p class="text-container-main">
			<spring:message code="client.description" />
		</p>
		<hr class="my-4 text-container-main">

		<h3 class="text-container-main">
			<spring:message code="orders.current" />
		</h3>
		<c:choose>
			<c:when test="${empty currentOrders}">
				<br>
				<div
					class="alert alert-dismissible alert-primary text-container-main">
					<spring:message code="${getCurrentOrdersResponse }" />
				</div>
			</c:when>
			<c:otherwise>
				<br>
				<div class="text-container-main">
					<table class="table table-hover">
						<thead>
							<tr class="table-info">
								<th scope="col"><spring:message code="id" /></th>
								<th scope="col"><spring:message code="date" /></th>
								<th scope="col"><spring:message code="accessories" /></th>
								<th scope="col"><spring:message code="services" /></th>
								<th scope="col"><spring:message code="price" /></th>
								<th scope="col"><spring:message code="status" /></th>
								<th scope="col"></th>
								<th scope="col"></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${currentOrders}" var="order">
								<tr class="table-light">
									<td><c:out value="${order.id}" /></td>
									<td><c:out
											value="${order.visit.dayOfVisit.date}
												${order.visit.hours}" /></td>
									<td><c:forEach items="${order.accessories }" var="map">
											<c:out value="${map.key.name} x${map.value}" />
										</c:forEach></td>
									<td><c:forEach items="${order.services }" var="map">
											<c:out value="${map.key.name} x${map.value}" />
										</c:forEach></td>
									<td><c:out value="${order.cost}" /> &#128</td>
									<td><spring:message code="${order.status}" /></td>
									<td><c:if test="${order.status == 'NEW'}">
											<a
												href="/CarWorkshop/clientGetCalendarView/workshop/${order.workshop.id}/${order.id }/-1">
												<button type="button" class="btn btn-outline-success">
													<spring:message code="proceed" />
												</button>
											</a>
										</c:if> <c:if test="${order.status == 'SCHEDULED'}">
											<a href="/CarWorkshop/visitDetails/${order.visit.id }">
												<button type="button" class="btn btn-outline-info">
													<spring:message code="details" />
												</button>
											</a>
										</c:if>
									</td>
									<td><a href="/CarWorkshop/cancelOrder/${order.id }"
										onclick="return confirm('Are you sure you want to delete this item?')">
											<button type="button" id="cancelOrder"
												class="btn btn-outline-primary">
												<spring:message code="delete" />
											</button>
									</a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</c:otherwise>
		</c:choose>
		<hr class="my-4 text-container-main">

		<h3 class="text-container-main">
			<spring:message code="orders.history" />
		</h3>
		<br>
		<c:choose>
			<c:when test="${empty previousOrders}">
				<div
					class="alert alert-dismissible alert-primary text-container-main">
					<spring:message code="${getPreviousOrdersResponse }" />
				</div>
			</c:when>
			<c:otherwise>
				<div class="text-container-main">
					<table class="table table-hover">
						<thead>
							<tr class="table-info">
								<th scope="col"><spring:message code="id" /></th>
								<th scope="col"><spring:message code="date" /></th>
								<th scope="col"><spring:message code="accessories" /></th>
								<th scope="col"><spring:message code="services" /></th>
								<th scope="col"><spring:message code="price" /></th>
								<th scope="col"><spring:message code="status" /></th>
								<th scope="col"></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${previousOrders}" var="order">
								<tr class="table-light">
									<td><c:out value="${order.id}" /></td>
									<td><c:out
											value="${order.visit.dayOfVisit.date}
												${order.visit.hours}" /></td>
									<td><c:forEach items="${order.accessories }" var="map">
											<c:out value="${map.key.name} x${map.value}" />
										</c:forEach></td>
									<td><c:forEach items="${order.services }" var="map">
											<c:out value="${map.key.name} x${map.value}" />
										</c:forEach></td>
									<td><c:out value="${order.cost}" /> &#128</td>
									<td><spring:message code="${order.status }"/></td>
									<td><a href="/CarWorkshop/visitDetails/${order.visit.id }">
											<button type="button" class="btn btn-outline-info">
												<spring:message code="details" />
											</button>
									</a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</c:otherwise>
		</c:choose>
		<hr class="my-4 text-container-main">

		<h3 class="text-container-main">
			&#9993
			<spring:message code="notifications" />
		</h3>
		<p class="text-container-main">
			<spring:message code="notifications.messages" />
		</p>
		<c:choose>
			<c:when test="${empty notifications }">
				<div
					class="alert alert-dismissible alert-primary text-container-main">
					<spring:message code="${getNotificationsResponse }" />
				</div>
			</c:when>
			<c:otherwise>
				<c:forEach items="${notifications }" var="notification">
					<div class="card text-white bg-info mb-3 text-container-main">
						<div class="card-header">${notification.subject}</div>
						<div class="card-body">
							<p class="card-text">${notification.content}</p>
							<p>
								<a
									href="/CarWorkshop/clientRemoveNotification/${notification.id }"
									onclick="return confirm('Are you sure you want to delete this item?')">
									<button type="button" class="btn btn-primary">
										<spring:message code="delete" />
									</button>
								</a>
							</p>
						</div>
					</div>
				</c:forEach>
			</c:otherwise>
		</c:choose>
		<hr class="my-4 text-container-main">
	</div>

	<!-- FOOTER -->
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>