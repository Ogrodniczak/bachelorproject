<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- NAVIGATION BAR -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="/CarWorkshop"><spring:message code="home" /></a>
		<c:if test="${not empty sessionUser }">
			<c:choose>
				<c:when test="${sessionUser.type == 'ADMIN' }">
					<a class="navbar-brand" href="/CarWorkshop/adminPage">
					<spring:message code="admin.page" /></a>
				</c:when>
				<c:when test="${sessionUser.type == 'CLIENT' }">
					<a class="navbar-brand" href="/CarWorkshop/clientPage"><spring:message code="client.page" /></a>
				</c:when>
				<c:when test="${sessionUser.type == 'OWNER' }">
					<a class="navbar-brand" href="/CarWorkshop/ownerPage"><spring:message code="owner.page" /></a>
				</c:when>
			</c:choose>
		</c:if>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarColor02" aria-controls="navbarColor02"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarColor02">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item"><a class="nav-link" href="#"><spring:message code="nav.help" /></a>
				</li>
				<li class="nav-item"><a class="nav-link" href="/CarWorkshop/about"><spring:message code="nav.about" /></a></li>
				<li class="nav-item"></li>
				<li class="nav-item"><a class="nav-link" href="/CarWorkshop/?locale=en"><spring:message code="nav.language.english" /></a></li>
				<li class="nav-item"><a class="nav-link" href="/CarWorkshop/?locale=pl"><spring:message code="nav.language.polish" /></a></li>
			</ul>
			<c:choose>
				<c:when test="${not empty sessionUser}">
					<a class="navbar-brand" href=#>${sessionUser.userName }</a>
					<a class="navbar-brand" href="/CarWorkshop/logOut"><spring:message code="nav.logout" /></a>
				</c:when>
				<c:otherwise>
					<a class="navbar-brand" href="/CarWorkshop/getLoginView"><spring:message code="nav.login" /></a>
					<a class="navbar-brand" href="/CarWorkshop/getRegistrationView"><spring:message code="nav.register" /></a>
				</c:otherwise>
			</c:choose>
		</div>
	</nav>