<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title><spring:message code="title" /></title>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/bootstrap.css" />" />
</head>
<body>
	<!-- NAVIGATION BAR -->
	<jsp:include page="navbar.jsp"></jsp:include>

	<!-- MAIN CONTAINER -->
	<div class="jumbotron">
		<h1 class="display-3">
			&#128197
			<spring:message code="calendars" />
		</h1>

		<c:choose>
			<c:when test="${not empty stations}">
				<c:forEach items="${stations}" var="station">
					<hr class="my-4">
					<h2>
						<spring:message code="station" />
						${station.number}
					</h2>
					<p>
						<spring:message code="calendars.generate" />
					</p>
					<a href="/CarWorkshop/generateStationCalendar/${station.id}">
						<button class="btn btn-info btn-lg">
							<spring:message code="add" />
						</button>
					</a>
					<br>
					<br>
					<ul class="pagination">
						<c:if test="${station.workshop != null}">
							<c:choose>
								<c:when test="${currentIndex > 6 and station.days.size() > 7}">
									<li class="page-item"><a class="page-link"
										href="/CarWorkshop/ownerGetCalendarView/workshop/${station.workshop.id}/${currentIndex - 7}">&laquo;
											<spring:message code="week.previous"/></a></li>
								</c:when>
								<c:when test="${currentIndex == -1 and station.days.size() >= 14 and station.days.size() > 7}">
									<li class="page-item"><a class="page-link"
										href="/CarWorkshop/ownerGetCalendarView/workshop/${station.workshop.id}/${station.days.size() - 14}">&laquo;
											<spring:message code="week.previous"/></a></li>
								</c:when>
							    <c:otherwise>
									<li class="page-item disabled"><a class="page-link" href="#">&laquo; <spring:message code="week.previous"/></a></li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${currentIndex != -1 and currentIndex < station.days.size()-7 and station.days.size() > 7}">
									<li class="page-item"><a class="page-link"
										href="/CarWorkshop/ownerGetCalendarView/workshop/${station.workshop.id}/${currentIndex + 7}"><spring:message code="week.next"/> &raquo;</a></li>
								</c:when>
								<c:otherwise>
									<li class="page-item disabled"><a class="page-link" href="#"><spring:message code="week.next"/> &raquo;</a></li>
								</c:otherwise>
							</c:choose>
						</c:if>
						<c:if test="${station.vehicleControlPoint != null}">
							<c:choose>
								<c:when test="${currentIndex > 6 and station.days.size() > 7}">
									<li class="page-item"><a class="page-link"
										href="/CarWorkshop/ownerGetCalendarView/controlPoint/${station.vehicleControlPoint.id}/${currentIndex - 7}">&laquo;
											<spring:message code="week.previous"/></a></li>
								</c:when>
								<c:when test="${currentIndex == -1 and station.days.size() >= 14 and station.days.size() > 7}">
									<li class="page-item"><a class="page-link"
										href="/CarWorkshop/ownerGetCalendarView/controlPoint/${station.vehicleControlPoint.id}/${station.days.size() - 14}">&laquo;
											<spring:message code="week.previous"/></a></li>
								</c:when>
							    <c:otherwise>
									<li class="page-item disabled"><a class="page-link" href="#">&laquo; <spring:message code="week.previous"/></a></li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${currentIndex != -1 and currentIndex < station.days.size()-7 and station.days.size() > 7}">
									<li class="page-item"><a class="page-link"
										href="/CarWorkshop/ownerGetCalendarView/controlPoint/${station.vehicleControlPoint.id}/${currentIndex + 7}"><spring:message code="week.next"/> &raquo;</a></li>
								</c:when>
								<c:otherwise>
									<li class="page-item disabled"><a class="page-link" href="#"><spring:message code="week.next"/> &raquo;</a></li>
								</c:otherwise>
							</c:choose>
						</c:if>
					</ul>
					<hr class="my-4">
					<div class="Row">
						<c:forEach items="${station.calendarVisitMap}" var="outerMap">
							<div class="Column">
								<table class="table table-hover">
									<thead>
										<tr class="table-info">
											<th scope="col">${outerMap.key.date}</th>
											<th scope="col"><spring:message code="client" /></th>
											<th scope="col"></th>
											<th scope="col"></th>
											<th scope="col"><spring:message
													code="${outerMap.key.date.dayOfWeek }" /></th>
										</tr>
									</thead>
									<tbody>

										<c:forEach items="${outerMap.value}" var="innerMap">
											<c:if test="${empty innerMap.value }">
												<tr class="table-success">
													<td><c:out value="${innerMap.key}" /></td>
													<td></td>
													<td></td>
													<td></td>
													<td><a
														href="/CarWorkshop/disableVisitHour/${outerMap.key.id}/${innerMap.key }/${currentIndex}"><button
																type="button" class="btn btn-outline-primary">
																<spring:message code="disable" />
															</button></a></td>
												</tr>
											</c:if>
											<c:if test="${not empty innerMap.value }">
												<c:if test="${innerMap.value.status == 'SCHEDULED'}">
													<tr class="table-light">
														<td><c:out value="${innerMap.key}" /></td>
														<td><c:out value="${innerMap.value.client.userName}" /></td>
														<td><a
															href="/CarWorkshop/visitDetails/${innerMap.value.id }"><button
																	type="button" class="btn btn-outline-info">
																	<spring:message code="details" />
																</button></a></td>
														<td><a
															href="/CarWorkshop/ownerTerminateVisit/${item}/${itemId}/${innerMap.value.id }/${currentIndex}"><button
																	type="button" class="btn btn-outline-success">
																	<spring:message code="proceed" />
																</button></a></td>
														<td><a
															href="/CarWorkshop/ownerCancelVisit/${innerMap.value.id }/${currentIndex}"><button
																	type="button" class="btn btn-outline-primary">
																	<spring:message code="cancel" />
																</button></a></td>
													</tr>
												</c:if>
											</c:if>
											<c:if test="${not empty innerMap.value }">
												<c:if test="${innerMap.value.status == 'TERMINATED'}">
													<tr class="table-danger">
														<td><c:out value="${innerMap.key}" /></td>
														<td><c:out value="${innerMap.value.client.userName}" /></td>
														<td></td>
														<td></td>
														<td><a
															href="/CarWorkshop/visitDetails/${innerMap.value.id }"><button
																	type="button" class="btn btn-outline-info">
																	<spring:message code="details" />
																</button></a></td>
													</tr>
												</c:if>
											</c:if>
											<c:if test="${not empty innerMap.value }">
												<c:if test="${innerMap.value.status == 'UNAVAILABLE'}">
													<tr class="table-primary">
														<td><c:out value="${innerMap.key}" /></td>
														<td></td>
														<td></td>
														<td></td>
														<td><a
															href="/CarWorkshop/enableVisitHour/${outerMap.key.id}/${innerMap.key }/${currentIndex}"><button
																	type="button" class="btn btn-outline-success">
																	<spring:message code="enable" />
																</button></a></td>
													</tr>
												</c:if>
											</c:if>
											<c:if test="${not empty innerMap.value }">
												<c:if test="${innerMap.value.status == 'OLD'}">
													<tr class="table-dark">
														<td><c:out value="${innerMap.key}" /></td>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
													</tr>
												</c:if>
											</c:if>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</c:forEach>
					</div>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<div class="alert alert-dismissible alert-primary">
					<spring:message code="stations.none" />
				</div>
			</c:otherwise>
		</c:choose>
	</div>

	<!-- FOOTER -->
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>