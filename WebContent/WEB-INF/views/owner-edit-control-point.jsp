<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
<title><spring:message code="title" /></title>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/bootstrap.css" />" />
</head>
<body>
	<!-- NAVIGATION BAR -->
	<jsp:include page="navbar.jsp"></jsp:include>

	<!-- MAIN CONTAINER -->
	<div class="jumbotron">
		<h1 class="display-3 text-container-main">${controlPoint.name}</h1>
		<hr class="my-4 text-container-main">
		<c:choose>
			<c:when test="${not empty editControlPointError}">
				<div
					class="alert alert-dismissible alert-primary text-container-main">
					<spring:message code="${editControlPointError}"/></div>
			</c:when>
			<c:when test="${not empty editControlPointSuccess}">
				<div
					class="alert alert-dismissible alert-success text-container-main">
					<spring:message code="${editControlPointSuccess}"/> <a href="/CarWorkshop/ownerPage"
						class="alert-link"><spring:message code="owner.back" /></a>
				</div>
			</c:when>
			<c:when test="${not empty addStationSuccess}">
				<div
					class="alert alert-dismissible alert-success text-container-main">
					<spring:message code="${addStationSuccess}"/></div>
			</c:when>
			<c:when test="${not empty addStationError}">
				<div
					class="alert alert-dismissible alert-primary text-container-main">
					<spring:message code="${addStationError}"/></div>
			</c:when>
			<c:when test="${not empty deleteStationSuccess}">
				<div
					class="alert alert-dismissible alert-success text-container-main">
					<spring:message code="${deleteStationSuccess}"/></div>
			</c:when>
			<c:when test="${not empty deleteStationError}">
				<div
					class="alert alert-dismissible alert-primary text-container-main">
					<spring:message code="${deleteStationError}"/></div>
			</c:when>
		</c:choose>


		<form:form
			action="/CarWorkshop/updateControlPointData/${controlPoint.id }"
			method="POST" modelAttribute="controlPoint">
			<h3 class="text-container-main">
				<spring:message code="info.general" />
			</h3>
			<div class="Row text-container-main">
				<div class="Column-label-long">
					<form:label class="lead" path="name">
						<spring:message code="name" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text" path="name"
						value="${controlPoint.name }" />
				</div>
				<form:errors path="name" />
			</div>
			<div class="Row text-container-main">
				<div class="Column-label-long">
					<form:label class="lead" path="description">
						<spring:message code="description" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text"
						path="description" value="${controlPoint.description }" />
				</div>
				<form:errors path="description" />
			</div>
			<div class="Row text-container-main">
				<div class="Column-label-long">
					<form:label class="lead" path="openingHour">
						<spring:message code="hour.opening" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text"
						path="openingHour" value="${controlPoint.openingHour}" />
				</div>
				<form:errors path="openingHour" />
			</div>
			<div class="Row text-container-main">
				<div class="Column-label-long">
					<form:label class="lead" path="closingHour">
						<spring:message code="hour.closing" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text"
						path="closingHour" value="${controlPoint.closingHour }" />
				</div>
				<form:errors path="closingHour" />
			</div>
			<br>
			<h3 class="text-container-main">
				<spring:message code="address" />
			</h3>
			<div class="Row text-container-main">
				<div class="Column-label-long">
					<form:label class="lead" path="address.street">
						<spring:message code="address.street" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text"
						path="address.street" value="${controlPoint.address.street }" />
				</div>
				<form:errors path="address.street" />
			</div>
			<div class="Row text-container-main">
				<div class="Column-label-long">
					<form:label class="lead" path="address.city">
						<spring:message code="address.city" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text"
						path="address.city" value="${controlPoint.address.city }" />
				</div>
				<form:errors path="address.city" />
			</div>
			<div class="Row text-container-main">
				<div class="Column-label-long">
					<form:label class="lead" path="address.postalCode">
						<spring:message code="address.postalcode" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text"
						path="address.postalCode"
						value="${controlPoint.address.postalCode }" />
				</div>
				<form:errors path="address.postalCode" />
			</div>
			<div>
				<input class="btn btn-info btn-lg text-container-main" type="submit"
					value="Save" />
			</div>
		</form:form>
		<hr class="my-4 text-container-main">

		<div class="text-container-main">
			<h3>
				&#9873
				<spring:message code="stations" />
			</h3>
			<p class="lead">
				<spring:message code="stations.amount" />
				: ${fn:length(controlPoint.stations)}
			</p>
			<a
				href="/CarWorkshop/ownerAddStation/controlPoint/${controlPoint.id }">
				<button class="btn btn-info btn-lg">
					<spring:message code="add" />
				</button>
			</a>
			<c:if test="${fn:length(controlPoint.stations) > 0}">
				<a
					href="/CarWorkshop/ownerRemoveStation/controlPoint/${controlPoint.id }">
					<button class="btn btn-info btn-lg">
						<spring:message code="delete" />
					</button>
				</a>
			</c:if>
		</div>
		<hr class="my-4 text-container-main">

		<h3 class="text-container-main">
			&#128197
			<spring:message code="calendar" />
		</h3>
		<p class="lead text-container-main">
			<spring:message code="calendar.control_point" />
		</p>
		<a
			href="/CarWorkshop/ownerGetCalendarView/controlPoint/${controlPoint.id }/-1">
			<button class="btn btn-info btn-lg text-container-main"
				id="showCalendar">
				<spring:message code="show" />
			</button>
		</a>
	</div>

	<!-- FOOTER -->
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>