<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- MESSAGES -->
<c:if test="${not empty deleteWorkshopSuccess }">
	<div class="alert alert-dismissible alert-success text-container-main">
		<spring:message code="${deleteWorkshopSuccess }" />
	</div>
</c:if>
<c:if test="${not empty deleteWorkshopError}">
	<div class="alert alert-dismissible alert-primary text-container-main">
		<spring:message code="${deleteWorkshopError }" />
	</div>
</c:if>
<c:if test="${not empty deleteControlPointSuccess }">
	<div class="alert alert-dismissible alert-success text-container-main">
		<spring:message code="${deleteControlPointSuccess }" />
	</div>
</c:if>
<c:if test="${not empty deleteControlPointError}">
	<div class="alert alert-dismissible alert-primary text-container-main">
		<spring:message code="${deleteControlPointError }" />
	</div>
</c:if>
<c:if test="${not empty deleteNotificationSuccess }">
	<div class="alert alert-dismissible alert-success text-container-main">
		<spring:message code="${deleteNotificationSuccess }" />
	</div>
</c:if>
<c:if test="${not empty deleteNotificationError}">
	<div class="alert alert-dismissible alert-primary text-container-main">
		<spring:message code="${deleteNotificationError }" />
	</div>
</c:if>
<c:if test="${not empty requestResponseSuccess }">
	<div class="alert alert-dismissible alert-success text-container-main">
		<spring:message code="${requestResponseSuccess }" />
	</div>
</c:if>
<c:if test="${not empty requestResponseError}">
	<div class="alert alert-dismissible alert-primary text-container-main">
		<spring:message code="${requestResponseError }" />
	</div>
</c:if>