<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title><spring:message code="title" /></title>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/bootstrap.css" />" />
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<!-- NAVIGATION BAR -->
	<jsp:include page="navbar.jsp"></jsp:include>

	<div class="jumbotron">
		<h1 class="display-3 text-container-main">
			<spring:message code="owner.page" />
		</h1>
		<h3 class="text-container-main">
			<spring:message code="owner.description" />
		</h3>
		<hr class="my-4 text-container-main">

		<!-- MESSAGES -->
		<jsp:include page="owner-messages.jsp"></jsp:include>

		<div class="Row text-container-main">
			<div class="Column">
				<h3>
					<spring:message code="user.workshops" />
				</h3>
				<p class="lead">
					<spring:message code="user.workshops.list" />
				</p>
				<form:form class="form-inline my-2 my-lg-0"
					action="/CarWorkshop/getOwnerWorkshops/${sessionUser.id}"
					method="POST">
					<br>
					<div>
						<input class="btn btn-info btn-lg" type="submit"
							value=<spring:message code="search" /> />
					</div>
				</form:form>
			</div>
		</div>
		<c:if test="${showWorkshops == true}">
			<br>
			<c:set var="showWorkshops" scope="session" value="${false}" />
			<c:choose>
				<c:when test="${empty workshops}">
					<div
						class="alert alert-dismissible alert-primary text-container-main">
						<spring:message code="${getWorkshopsResponse }"/></div>
				</c:when>
				<c:otherwise>
					<div class="text-container-main">
						<table class="table table-hover">
							<thead>
								<tr class="table-info">
									<th scope="col"><spring:message code="id" /></th>
									<th scope="col"><spring:message code="name" /></th>
									<th scope="col"><spring:message code="address.street" /></th>
									<th scope="col"><spring:message code="address.city" /></th>
									<th scope="col"><spring:message code="status" /></th>
									<th scope="col"></th>
									<th scope="col"></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${workshops}" var="workshop">
									<tr class="table-light text-container-main">
										<td><c:out value="${workshop.id}" /></td>
										<td><c:out value="${workshop.name}" /></td>
										<td><c:out value="${workshop.address.street}" /></td>
										<td><c:out value="${workshop.address.city}" /></td>
										<td><spring:message code="${workshop.status}" /></td>
										<td><a
											href="/CarWorkshop/ownerEditWorkshop/${workshop.id }"><button
													type="button" class="btn btn-outline-info">
													<spring:message code="edit" />
												</button></a></td>
										<td><c:if test="${workshop.status == 'ACTIVE'}"><a
											href="/CarWorkshop/ownerDeleteWorkshop/${workshop.id }"
											onclick="return confirm('Are you sure you want to delete this item?')">
												<button type="button" id="deleteWorkshop"
													class="btn btn-outline-primary">
													<spring:message code="delete" />
												</button>
										</a></c:if></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</c:otherwise>
			</c:choose>
		</c:if>
		<hr class="my-4 text-container-main">

		<div class="Row text-container-main">
			<div class="Column">
				<h3>
					<spring:message code="user.control_points" />
				</h3>
				<p class="lead">
					<spring:message code="user.control_points.list" />
				</p>
				<form:form class="form-inline my-2 my-lg-0"
					action="/CarWorkshop/getOwnerControlPoints/${sessionUser.id}"
					method="POST">
					<br>
					<div>
						<input class="btn btn-info btn-lg" type="submit" value="Search" />
					</div>
				</form:form>
			</div>
		</div>
		<c:if test="${showControlPoints == true}">
			<br>
			<c:set var="showControlPoints" scope="session" value="${false}" />
			<c:choose>
				<c:when test="${empty controlPoints}">
					<div
						class="alert alert-dismissible alert-primary text-container-main">
						<spring:message code="${getControlPointsResponse }"/></div>
				</c:when>
				<c:otherwise>
					<div class="text-container-main">
						<table class="table table-hover">
							<thead>
								<tr class="table-info">
									<th scope="col"><spring:message code="id" /></th>
									<th scope="col"><spring:message code="name" /></th>
									<th scope="col"><spring:message code="address.street" /></th>
									<th scope="col"><spring:message code="address.city" /></th>
									<th scope="col"><spring:message code="status" /></th>
									<th scope="col"></th>
									<th scope="col"></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${controlPoints}" var="controlPoint">
									<tr class="table-light text-container-main">
										<td><c:out value="${controlPoint.id}" /></td>
										<td><c:out value="${controlPoint.name}" /></td>
										<td><c:out value="${controlPoint.address.street}" /></td>
										<td><c:out value="${controlPoint.address.city}" /></td>
										<td><spring:message code="${controlPoint.status}" /></td>
										<td><a
											href="/CarWorkshop/ownerEditControlPoint/${controlPoint.id }"><button
													type="button" class="btn btn-outline-info">
													<spring:message code="edit" />
												</button></a></td>
										<td><c:if test="${controlPoint.status == 'ACTIVE' }"><a
											href="/CarWorkshop/ownerDeleteControlPoint/${controlPoint.id }"
											onclick="return confirm('Are you sure you want to delete this item?')">
												<button type="button" id="deletePoint"
													class="btn btn-outline-primary">
													<spring:message code="delete" />
												</button>
										</a></c:if></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</c:otherwise>
			</c:choose>
		</c:if>
		<hr class="my-4 text-container-main">

		<div class="Row text-container-main">
			<div class="Column">
				<h3>
					&#9993
					<spring:message code="notifications" />
				</h3>
				<p>
					<spring:message code="notifications.messages" />
				</p>

				<c:choose>
					<c:when test="${empty notifications }">
						<div class="alert alert-dismissible alert-primary">
							<spring:message code="${getNotificationsResponse }"/></div>
					</c:when>
					<c:otherwise>
						<c:forEach items="${notifications }" var="notification">
							<div class="card text-white bg-info mb-3">
								<div class="card-header">${notification.subject}</div>
								<div class="card-body">
									<p class="card-text">${notification.content}</p>
									<p>
										<a
											href="/CarWorkshop/ownerRemoveNotification/${notification.id }"
											onclick="return confirm('Are you sure you want to delete this item?')">
											<button type="button" class="btn btn-primary">
												<spring:message code="delete" />
											</button>
										</a>
									</p>
								</div>
							</div>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
		<hr class="my-4 text-container-main">


		<div class="Column">
			<h3 class="text-container-main">
				<spring:message code="requests" />
			</h3>
			<p class="text-container-main">
				<spring:message code="request.send" />
			</p>
			<form:form class="form-inline my-2 my-lg-0" action="/CarWorkshop/ownerAddRequest"
				modelAttribute="requestModel" method="POST">
				<div class="Row text-container-main">
					<div class="Column-label">
						<form:label path="subject">
							<spring:message code="request.subject" />
						</form:label>
					</div>
					<div class="Column-label">
						<form:input class="form-control mr-sm-2" path="subject"
							type="text" />
					</div>
					<div class="Column-label">
						<form:errors path="subject"></form:errors>
					</div>
				</div>
				<div class="Row text-container-main">
					<div class="Column-label">
						<form:label path="content">
							<spring:message code="request.content" />
						</form:label>
					</div>
					<div class="Column-label">
						<form:textarea class="form-control mr-sm-4" path="content" />
					</div>
					<div class="Column-label">
						<form:errors path="content"></form:errors>
					</div>
				</div>
				<hr class="my-4 text-container-main">
				<div class="Row text-container-main">
					<div class="Column">
						<input class="btn btn-info btn-lg" type="submit"
							value=<spring:message code="submit" /> />
					</div>
				</div>
			</form:form>
		</div>
		<hr class="my-4 text-container-main">
	</div>

	<!-- FOOTER -->
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>