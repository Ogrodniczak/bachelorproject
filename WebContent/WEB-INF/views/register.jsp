<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title><spring:message code="title" /></title>
<link href="resources/css/bootstrap.css" rel="stylesheet"
	type="text/css" />
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<!-- NAVIGATION BAR -->
	<jsp:include page="navbar.jsp"></jsp:include>

	<div class="jumbotron">
		<h1 class="display-3 text-container-main">
			<spring:message code="nav.register" />
		</h1>
		<hr class="my-4 text-container-main">

		<c:if test="${not empty registerError}">
			<div
				class="alert alert-dismissible alert-primary text-container-main">
				<spring:message code="${registerError}" />
			</div>
		</c:if>
		<c:if test="${not empty registerSuccess}">
			<div
				class="alert alert-dismissible alert-success text-container-main">
				<spring:message code="${registerSuccess}" />
				<a href="/CarWorkshop/getLoginView" class="alert-link"><spring:message
						code="register.login" /></a>
			</div>
		</c:if>

		<form:form action="submitRegister" method="POST"
			modelAttribute="registerForm">
			<div class="Row text-container-main">
				<div class="Column-label-long">
					<form:label path="firstName">
						<spring:message code="first_name" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text"
						path="firstName" />
				</div>
				<div class="Column error">
					<form:errors path="firstName"></form:errors>
				</div>
			</div>
			<div class="Row text-container-main">
				<div class="Column-label-long">
					<form:label path="surname">
						<spring:message code="surname" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text" path="surname" />
				</div>
				<div class="Column error">
					<form:errors path="surname"></form:errors>
				</div>
			</div>
			<div class="Row text-container-main">
				<div class="Column-label-long">
					<form:label path="userName">
						<spring:message code="user.name" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text"
						path="userName" />
				</div>
				<div class="Column error">
					<form:errors path="userName"></form:errors>
				</div>
			</div>
			<div class="Row text-container-main">
				<div class="Column-label-long">
					<form:label path="userType">
						<spring:message code="user.type" />:</form:label>
				</div>
				<div class="Column-input lead">
					<form:select class="custom-select lead" path="userType">
						<form:option value="GUEST">
							<spring:message code="select" />
						</form:option>
						<form:option value="ADMIN">
							<spring:message code="select.admin" />
						</form:option>
						<form:option value="OWNER">
							<spring:message code="select.owner" />
						</form:option>
						<form:option value="CLIENT">
							<spring:message code="select.client" />
						</form:option>
					</form:select>
				</div>
				<div class="Column error">
					<form:errors path="userType"></form:errors>
				</div>
			</div>
			<div class="Row text-container-main">
				<div class="Column-label-long">
					<form:label path="emailAddress">
						<spring:message code="email" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text"
						path="emailAddress" />
				</div>
				<div class="Column error">
					<form:errors path="emailAddress"></form:errors>
				</div>
			</div>
			<div class="Row text-container-main">
				<div class="Column-label-long">
					<form:label path="phoneNumber">
						<spring:message code="phone_number" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text"
						path="phoneNumber" />
				</div>
				<div class="Column error">
					<form:errors path="phoneNumber"></form:errors>
				</div>
			</div>
			<div class="Row text-container-main">
				<div class="Column-label-long">
					<form:label path="password">
						<spring:message code="password" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="password"
						path="password" />
				</div>
				<div class="Column error">
					<form:errors path="password"></form:errors>
				</div>
			</div>
			<div class="Row text-container-main">
				<div class="Column-label-long">
					<form:label path="confirmPassword">
						<spring:message code="password.confirm" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="password"
						path="confirmPassword" />
				</div>
				<div class="Column error">
					<form:errors path="confirmPassword"></form:errors>
				</div>
			</div>
			<hr class="my-4 text-container-main">
			<div class="Row text-container-main">
				<div class="Column-label">
					<input class="btn btn-info btn-lg" type="submit"
						value=<spring:message code="submit" /> />
				</div>
			</div>
		</form:form>
	</div>

	<!-- FOOTER -->
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>