<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
<title><spring:message code="title" /></title>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/bootstrap.css" />" />
</head>
<body>
	<!-- NAVIGATION BAR -->
	<jsp:include page="navbar.jsp"></jsp:include>

	<!-- MAIN CONTAINER -->
	<div class="jumbotron">
		<h1 class="display-3 text-container-main">
			<spring:message code="workshop" />
			${workshopModel.name}
		</h1>
		<hr class="my-4 text-container-main">
		<c:if test="${ not empty addAccessorySuccess}">
			<div
				class="alert alert-dismissible alert-success text-container-main">
				<spring:message code="${addAccessorySuccess }"/></div>
		</c:if>
		<c:if test="${ not empty addAccessoryError}">
			<div
				class="alert alert-dismissible alert-primary text-container-main">
				<spring:message code="${addAccessoryError }"/></div>
		</c:if>
		<c:if test="${ not empty addServiceSuccess}">
			<div
				class="alert alert-dismissible alert-success text-container-main">
				<spring:message code="${addServiceSuccess }"/></div>
		</c:if>
		<c:if test="${ not empty addServiceError}">
			<div
				class="alert alert-dismissible alert-primary text-container-main">
				<spring:message code="${addServiceError }"/></div>
		</c:if>

		<div class="text-container-main">
			<h2>
				<spring:message code="info.general" />
			</h2>
			<p>${workshopModel.description }</p>
			<p>
				<spring:message code="address" />
				: ${workshopModel.address.street} ${workshopModel.address.city}
				${workshopModel.address.postalCode}
			</p>
			<p>
				<spring:message code="hour.opening" />
				: ${workshopModel.openingHour }
			</p>
			<p>
				<spring:message code="hour.closing" />
				: ${workshopModel.closingHour }
			</p>
			<p>
				<spring:message code="owner.phone_number" />
				:
				<c:choose>
					<c:when test="${empty workshopModel.owner.phoneNumber }">
						<b style="color: red"><spring:message code="not_available" /></b>
					</c:when>
					<c:otherwise>
		${workshopModel.owner.phoneNumber }
		</c:otherwise>
				</c:choose>
			</p>
			<p>
				<spring:message code="owner.email" />
				: ${workshopModel.owner.emailAddress }
			</p>
			<p>
				<spring:message code="stations.amount" />
				: ${nrOfStations}
			</p>
		</div>
		<hr class="my-4 text-container-main">

		<h2 class="text-container-main">
			<spring:message code="services" />
		</h2>

		<br>
		<c:choose>
			<c:when test="${empty services}">
				<div
					class="alert alert-dismissible alert-primary text-container-main">
					<spring:message code="${getEmptyServicesResponse }"/></div>
			</c:when>
			<c:otherwise>
				<div class="text-container-main">
					<table class="table table-hover">
						<thead>
							<tr class="table-info">
								<th scope="col"><spring:message code="id" /></th>
								<th scope="col"><spring:message code="name" /></th>
								<th scope="col"><spring:message code="availability" /></th>
								<th scope="col"><spring:message code="discount" /></th>
								<th scope="col"><spring:message code="price" /></th>
								<th scope="col"><spring:message code="amount.sold" /></th>
								<th scope="col"></th>
								<th scope="col"></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${services}" var="service">
								<tr class="table-light">
									<td><c:out value="${service.id}" /></td>
									<td><c:out value="${service.name}" /></td>
									<td><spring:message code="${service.available}" /></td>
									<td><c:out value="${service.discount}" /></td>
									<td><c:out value="${service.cost * (100 - service.discount) * 0.01 }" /></td>
									<td><c:out value="${service.amountSold}" /></td>
									<td><c:if test="${service.available == true }"><a
										href="/CarWorkshop/addServiceToCart/workshop-view/${service.id }">
											<button type="button" class="btn btn-outline-success">
												<spring:message code="order.add" />
											</button>
									</a></c:if></td>
									<td><a href="/CarWorkshop/serviceView/${service.id }">
											<button type="button" class="btn btn-outline-info">
												<spring:message code="details" />
											</button>
									</a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</c:otherwise>
		</c:choose>

		<hr class="my-4 text-container-main">

		<h2 class="text-container-main">
			<spring:message code="accessories" />
		</h2>
		<br>
		<c:choose>
			<c:when test="${empty accessories}">
				<div
					class="alert alert-dismissible alert-primary text-container-main">
					<spring:message code="${getEmptyAccessoriesResponse }"/></div>
			</c:when>
			<c:otherwise>
				<div class="text-container-main">
					<table class="table table-hover">
						<thead>
							<tr class="table-info">
								<th scope="col"><spring:message code="id" /></th>
								<th scope="col"><spring:message code="name" /></th>
								<th scope="col"><spring:message code="availability" /></th>
								<th scope="col"><spring:message code="discount" /></th>
								<th scope="col"><spring:message code="price" /></th>
								<th scope="col"><spring:message code="amount.sold" /></th>
								<th scope="col"></th>
								<th scope="col"></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${accessories}" var="accessory">
								<tr class="table-light">
									<td><c:out value="${accessory.id}" /></td>
									<td><c:out value="${accessory.name}" /></td>
									<td><spring:message code="${accessory.available}" /></td>
									<td><c:out value="${accessory.discount}" /></td>
									<td><c:out value="${accessory.cost * (100 - accessory.discount) * 0.01}" /></td>
									<td><c:out value="${accessory.amountSold}" /></td>
									<td><c:if test="${accessory.available == true && accessory.amount > 0}"><a
										href="/CarWorkshop/addAccessoryToCart/workshop-view/${accessory.id }">
											<button type="button" class="btn btn-outline-success">
												<spring:message code="order.add" />
											</button>
									</a></c:if></td>
									<td><a href="/CarWorkshop/accessoryView/${accessory.id }">
											<button type="button" class="btn btn-outline-info">
												<spring:message code="details" />
											</button>
									</a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</c:otherwise>
		</c:choose>
		<hr class="my-4 text-container-main">

		<h2 class="text-container-main">
			&#128197 <spring:message code="calendar" />
		</h2>
		<p class="text-container-main">
			<spring:message code="calendar.description" />
		</p>
		<a
			href="/CarWorkshop/clientGetCalendarView/workshop/${workshopModel.id }/0/-1">
			<button class="btn btn-info btn-lg text-container-main"
				id="showCalendar">
				<spring:message code="show" />
			</button>
		</a>
		<hr class="my-4 text-container-main">
	</div>

	<!-- FOOTER -->
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>