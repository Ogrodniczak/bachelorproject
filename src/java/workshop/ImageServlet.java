package workshop;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

import workshop.home.dao.HomePageDao;
import workshop.home.dao.impl.HomePageDaoImpl;
import workshop.home.service.HomePageService;
import workshop.home.service.impl.HomePageServiceImpl;
import workshop.model.CarAccessory;

@WebServlet("/images/*")
@Component
public class ImageServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static HomePageService service;
	private static HomePageDao dao;

	static {
		service = new HomePageServiceImpl();
		dao = new HomePageDaoImpl();
		service.setDao(dao);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		byte[] image = (byte[]) request.getSession().getAttribute("uploadedImage");
		if (image == null) {
			String id = request.getPathInfo().substring(1);
			CarAccessory accessory = service.getAccessoryById(Long.parseLong(id));
			if (accessory != null)
				image = accessory.getImage();
			if (image == null) {
				return;
			}
			response.setContentType(getServletContext().getMimeType(accessory.getName()));
		} 
		response.setContentLength(image.length);
		response.getOutputStream().write(image);
	}
}

