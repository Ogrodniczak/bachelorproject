package workshop;

public enum MailResponseTypeEnum {
	DELIVERED, NO_RECIPIENTS, NO_SENDER, ERROR
}
