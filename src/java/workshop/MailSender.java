package workshop;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang3.StringUtils;

import workshop.model.Person;
import workshop.model.Request;

public class MailSender {

	private static final String HOST = "smtp.mailtrap.io";
	private static final String PORT = "2525";
	private static final String USERNAME = "79883e539fab3a";
	private static final String PASSWORD = "0a4095d611d73e";

	public static MailResponseTypeEnum sendMail(Request request) {
		String sender = request.getSender().getEmailAddress();

		if (StringUtils.isBlank(sender)) {
			return MailResponseTypeEnum.NO_SENDER;
		}

		List<InternetAddress> recipientAddressList = new ArrayList<>();

		for (Person recipient : request.getRecipients()) {
			if (!StringUtils.isBlank(recipient.getEmailAddress())) {
				try {
					recipientAddressList.add(new InternetAddress(recipient.getEmailAddress()));
				} catch (AddressException e) {
					e.printStackTrace(System.err);
					return MailResponseTypeEnum.ERROR;
				}
			}
		}

		if (recipientAddressList.isEmpty()) {
			return MailResponseTypeEnum.NO_RECIPIENTS;
		}

		Properties prop = new Properties();
		prop.put("mail.smtp.auth", true);
		prop.put("mail.smtp.starttls.enable", "true");
		prop.put("mail.smtp.host", HOST);
		prop.put("mail.smtp.port", PORT);
		prop.put("mail.smtp.ssl.trust", HOST);
		prop.put("mail.smtp.user", USERNAME);
		prop.put("mail.smtp.pass", PASSWORD);
		
		Session session = Session.getInstance(prop, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(USERNAME, PASSWORD);
			}
		});
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(sender));
			message.setRecipients(Message.RecipientType.TO, (Address[]) recipientAddressList.toArray(new Address[0]));
			message.setSubject(request.getSubject());

			MimeBodyPart mimeBodyPart = new MimeBodyPart();
			mimeBodyPart.setContent(request.getContent(), "text/html");

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(mimeBodyPart);

			message.setContent(multipart);

			Transport.send(message);
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return MailResponseTypeEnum.ERROR;
		}

		return MailResponseTypeEnum.DELIVERED;
	}
}
