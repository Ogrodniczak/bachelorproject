package workshop;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

public class UploadImageServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final int THRESHOLD_SIZE_MAX = 5_000_000;
	private static final int REQUEST_SIZE_MAX = 2_000_000;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		boolean isMultipart = ServletFileUpload.isMultipartContent(request);

		if (!isMultipart) {
			return;
		}

		long workshopId = (long) request.getSession().getAttribute("sessionWorkshopId");
		long accessoryId = (long) request.getSession().getAttribute("sessionAccessoryId");

		DiskFileItemFactory factory = new DiskFileItemFactory();
		factory.setSizeThreshold(THRESHOLD_SIZE_MAX);
		factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setSizeMax(REQUEST_SIZE_MAX);

		try {
			List<FileItem> items = upload.parseRequest(request);
			Iterator<FileItem> iter = items.iterator();
			byte[] byteArray = null;
			while (iter.hasNext()) {
				FileItem item = iter.next();
				if (!item.isFormField()) {
					BufferedImage img = ImageIO.read(item.getInputStream());
					if (img == null) {
						break;
					}
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					ImageIO.write(img, "jpg", baos);
					baos.flush();
					byteArray = baos.toByteArray();
					baos.close();
				}
			}
			request.setAttribute("uploadedImage", byteArray);
			getServletContext().getRequestDispatcher("/uploadImage/" + workshopId + "/" + accessoryId).forward(request, response);
		} catch (Exception ex) {
			throw new ServletException(ex);
		}
	}
}
