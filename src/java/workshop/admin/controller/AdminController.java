package workshop.admin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import workshop.admin.service.AdminService;
import workshop.admin.validator.ControlPointValidator;
import workshop.admin.validator.WorkshopValidator;
import workshop.home.service.HomePageService;
import workshop.model.Person;
import workshop.model.PersonTypeEnum;
import workshop.model.Request;
import workshop.model.VehicleControlPoint;
import workshop.model.VehicleControlPointStatusEnum;
import workshop.model.Workshop;
import workshop.model.WorkshopStatusEnum;

@Controller
public class AdminController {
	@Autowired
	@Qualifier("adminServiceImpl")
	private AdminService service;

	@Autowired
	@Qualifier("homePageServiceImpl")
	private HomePageService homeService;

	@Autowired
	private WorkshopValidator workshopValidator;

	@Autowired
	private ControlPointValidator controlPointValidator;

	@InitBinder("workshopModel")
	private void initWorkshopBinder(WebDataBinder binder) {
		binder.setValidator(workshopValidator);
	}

	@InitBinder("vehicleControlPointModel")
	private void initControlPointBinder(WebDataBinder binder) {
		binder.setValidator(controlPointValidator);
	}

	@RequestMapping("/adminGetWorkshops")
	public ModelAndView getWorkshops(HttpServletRequest request, ModelMap model) {
		ModelAndView mv = new ModelAndView("admin");
		String name = request.getParameter("workshopName");
		model.addAttribute("showWorkshops", true);
		List<Workshop> workshops = homeService.getWorkshops(name);
		model.addAttribute("workshops", workshops);
		if (workshops.isEmpty()) {
			model.addAttribute("getWorkshopsResponse", "admin.workshop.none");
			mv.addAllObjects(model);
			return mv;
		}
		mv.addAllObjects(model);
		return mv;
	}

	@RequestMapping("/getAdminVehicleControlPoints")
	public ModelAndView getAdminVehicleControlPoint(HttpServletRequest request, ModelMap model) {
		ModelAndView mv = new ModelAndView("admin");
		String name = request.getParameter("controlPointName");
		model.addAttribute("showControlPointsResponse", true);
		List<VehicleControlPoint> controlPoints = homeService.getControlPoints(name);
		if (controlPoints.isEmpty()) {
			model.addAttribute("getControlPointsResponse", "admin.control_point.none");
			mv.addAllObjects(model);
			return mv;
		}
		model.addAttribute("showControlPoints", true);
		model.addAttribute("controlPoints", controlPoints);
		mv.addAllObjects(model);
		return mv;
	}

	@RequestMapping("/adminPage")
	public ModelAndView adminPage(HttpServletRequest request, HttpSession session, ModelMap model) {
		Person user = (Person) session.getAttribute("sessionUser");
		if (user == null || !user.getType().equals(PersonTypeEnum.ADMIN)) {
			return new ModelAndView("redirect:/");
		}
		ModelAndView mv = new ModelAndView("admin");
		String delWorkshopSuccess = request.getParameter("deleteWorkshopSuccess");
		String delWorkshopError = request.getParameter("deleteWorkshopError");
		String delControlPointSuccess = request.getParameter("deleteControlPointSuccess");
		String delControlPointError = request.getParameter("deleteControlPointError");
		String requestResolveSuccess = request.getParameter("requestResolveSuccess");
		if (delWorkshopSuccess != null) {
			model.addAttribute("deleteWorkshopSuccess", delWorkshopSuccess);
		}
		if (delWorkshopError != null) {
			model.addAttribute("deleteWorkshopError", delWorkshopError);
		}
		if (delControlPointSuccess != null) {
			model.addAttribute("deleteControlPointSuccess", delControlPointSuccess);
		}
		if (delControlPointError != null) {
			model.addAttribute("deleteControlPointError", delControlPointError);
		}
		if (requestResolveSuccess != null) {
			model.addAttribute("requestResolveSuccess", requestResolveSuccess);
		}
		mv.addObject(model);
		return mv;
	}

	@GetMapping("/getRequests")
	public ModelAndView getRequests(HttpSession session, ModelMap model) {
		Person user = (Person) session.getAttribute("sessionUser");
		List<Request> requests = service.getRequests(user.getId());
		model.addAttribute("showRequests", true);
		model.addAttribute("requests", requests);
		return new ModelAndView("admin", model);
	}

	@RequestMapping("/getAddWorkshopView")
	public ModelAndView addWorkshopView(HttpServletRequest request, ModelMap model) {
		ModelAndView mv = new ModelAndView("add-workshop");
		String success = request.getParameter("addWorkshopSuccess");
		String error = request.getParameter("addWorkshopError");
		if (success != null) {
			model.addAttribute("addWorkshopSuccess", success);
		}
		if (error != null) {
			model.addAttribute("addWorkshopError", error);
		}
		model.addAttribute("workshopModel", new Workshop());
		mv.addObject(model);
		return mv;
	}

	@RequestMapping("/getAddControlPointView")
	public ModelAndView addControlPointView(HttpServletRequest request, ModelMap model) {
		ModelAndView mv = new ModelAndView("add-control-point");
		String success = request.getParameter("addControlPointSuccess");
		String error = request.getParameter("addControlPointError");
		if (success != null) {
			model.addAttribute("addControlPointSuccess", success);
		}
		if (error != null) {
			model.addAttribute("addControlPointError", error);
		}
		model.addAttribute("vehicleControlPointModel", new VehicleControlPoint());
		mv.addObject(model);
		return mv;
	}

	@PostMapping("/submitNewWorkshop")
	public ModelAndView addWorkshop(@ModelAttribute("workshopModel") @Valid Workshop workshop, BindingResult result,
			ModelMap model) {

		if (result.hasErrors()) {
			model.addAttribute("workshopErrorForm", "admin.workshop.add.error.form");
			return new ModelAndView("add-workshop");
		}

		Person owner = homeService.getUser(workshop.getOwner(), PersonTypeEnum.OWNER);

		if (owner == null) {
			model.addAttribute("addWorkshopError", "admin.add.error.owner");
			return new ModelAndView("redirect:/getAddWorkshopView", model);
		}
		workshop.setOwner(owner);
		workshop.setStatus(WorkshopStatusEnum.ACTIVE);
		Workshop newWorkshop = service.saveWorkshop(workshop);

		if (newWorkshop == null) {
			model.addAttribute("addWorkshopError", "admin.workshop.add.error");
			return new ModelAndView("redirect:/getAddWorkshopView", model);
		}

		model.addAttribute("addWorkshopSuccess", "admin.workshop.add.success");
		return new ModelAndView("redirect:/getAddWorkshopView", model);
	}

	@PostMapping("/submitNewControlPoint")
	public ModelAndView addVehicleControlPoint(
			@ModelAttribute("vehicleControlPointModel") @Valid VehicleControlPoint vehicleControlPoint,
			BindingResult result, ModelMap model) {

		if (result.hasErrors()) {
			model.addAttribute("controlPointErrorForm", "admin.control_point.add.error.form");
			return new ModelAndView("add-control-point");
		}

		Person owner = homeService.getUser(vehicleControlPoint.getOwner(), PersonTypeEnum.OWNER);

		if (owner == null) {
			model.addAttribute("addControlPointError", "admin.add.error.owner");
			return new ModelAndView("redirect:/getAddControlPointView", model);
		}
		vehicleControlPoint.setOwner(owner);
		vehicleControlPoint.setStatus(VehicleControlPointStatusEnum.ACTIVE);
		VehicleControlPoint newVehicleControlPoint = service.saveVehicleControlPoint(vehicleControlPoint);

		if (newVehicleControlPoint == null) {
			model.addAttribute("addControlPointError", "admin.control_point.add.error");
			return new ModelAndView("redirect:/getAddControlPointView", model);
		}

		model.addAttribute("addControlPointSuccess", "admin.control_point.add.success");
		return new ModelAndView("redirect:/getAddControlPointView", model);
	}

	@RequestMapping("/adminDeleteControlPoint/{controlPointId}")
	public ModelAndView deleteControlPoint(@PathVariable("controlPointId") long controlPointId, ModelMap model) {

		VehicleControlPoint deletedControlPoint = service.deleteControlPoint(controlPointId);

		if (deletedControlPoint == null) {
			model.addAttribute("deleteControlPointError", "admin.control_point.delete.error");
			return new ModelAndView("redirect:/adminPage", model);
		}

		model.addAttribute("deleteControlPointSuccess", "admin.control_point.delete.success");
		return new ModelAndView("redirect:/adminPage", model);
	}

	@RequestMapping("/adminDeleteWorkshop/{workshopId}")
	public ModelAndView deleteWorkshop(@PathVariable("workshopId") long workshopId, ModelMap model) {

		Workshop deletedWorkshop = service.deleteWorkshop(workshopId);

		if (deletedWorkshop == null) {
			model.addAttribute("deleteWorkshopError", "admin.workshop.delete.error");
			return new ModelAndView("redirect:/adminPage", model);
		}

		model.addAttribute("deleteWorkshopSuccess", "admin.workshop.delete.success");
		return new ModelAndView("redirect:/adminPage", model);
	}

	@RequestMapping("/adminResolveRequest/{requestId}")
	public ModelAndView resolveRequest(@PathVariable("requestId") long requestId, ModelMap model) {

		service.resolveRequest(requestId);
		model.addAttribute("requestResolveSuccess", "admin.request.resolve.success");
		return new ModelAndView("redirect:/adminPage", model);
	}
}
