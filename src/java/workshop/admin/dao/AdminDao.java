package workshop.admin.dao;

import java.util.List;

import workshop.model.Address;
import workshop.model.Person;
import workshop.model.Request;
import workshop.model.VehicleControlPoint;
import workshop.model.Workshop;

public interface AdminDao {
	public Workshop findWorkshop(Workshop workshop);

	public Workshop findWorkshopById(long id);

	public Workshop persistWorkshop(Workshop workshop);

	public <T> T persistElement(Class<T> persistentClass, T element, String name);

	public List<Workshop> getWorkshops(String name);

	public Address persistWorkshopAddress(Address address, long workshopId);

	public Workshop updateWorkshop(Workshop workshop);

	public Person findOwner(long id, String name);

	public Workshop deleteWorkshop(long workshopId);

	public VehicleControlPoint deleteControlPoint(long controlPointId);

	public List<Request> getRequests(long adminId);

	public void resolveRequest(long requestId);

	public VehicleControlPoint findControlPointById(long id);

	public VehicleControlPoint updateControlPoint(VehicleControlPoint controlPoint);
}
