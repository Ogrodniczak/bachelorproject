package workshop.admin.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.transaction.Transactional;

import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import workshop.admin.dao.AdminDao;
import workshop.model.Address;
import workshop.model.Person;
import workshop.model.Request;
import workshop.model.RequestStatusEnum;
import workshop.model.VehicleControlPoint;
import workshop.model.Workshop;

@Repository
@Transactional
public class AdminDaoImpl implements AdminDao {

	private static final EntityManagerFactory entityManagerFactory = Persistence
			.createEntityManagerFactory("persistenceUnit");

	@PreDestroy
	public void finish() {
		entityManagerFactory.close();
	}

	private EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}

	@Override
	public Workshop persistWorkshop(Workshop workshop) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();

		try {
			tx.begin();
			if (findWorkshop(workshop) == null) {
				Person owner = workshop.getOwner();
				if (owner != null) {
					owner.getWorkshopsOwned().add(workshop);
					em.merge(owner);
				}
				em.persist(workshop);
				tx.commit();
			} else {
				workshop = null;
			}
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return null;
		} finally {
			em.close();
		}
		return workshop;
	}

	@Override
	public Workshop findWorkshopById(long id) {
		EntityManager em = getEntityManager();
		Workshop foundWorkshop = null;
		try {
			foundWorkshop = em.find(Workshop.class, id);
			Hibernate.initialize(foundWorkshop.getStations());
			Hibernate.initialize(foundWorkshop.getServices());
			Hibernate.initialize(foundWorkshop.getAccessories());
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return foundWorkshop;
	}

	@Override
	public VehicleControlPoint findControlPointById(long id) {
		EntityManager em = getEntityManager();
		VehicleControlPoint controlPoint = null;
		try {
			controlPoint = em.find(VehicleControlPoint.class, id);
			Hibernate.initialize(controlPoint.getStations());
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return controlPoint;
	}

	@Override
	public Workshop findWorkshop(Workshop workshop) {
		EntityManager em = getEntityManager();
		Workshop foundWorkshop = null;
		try {
			foundWorkshop = em
					.createQuery("SELECT w FROM Workshop w WHERE w.name = :name OR w.id = :id", Workshop.class)
					.setParameter("name", workshop.getName()).setParameter("id", workshop.getId()).getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return foundWorkshop;
	}

	@Override
	public List<Workshop> getWorkshops(String name) {
		EntityManager em = getEntityManager();
		List<Workshop> foundWorkshops = new ArrayList<>();
		try {
			if (name != null && !name.equals("")) {
				foundWorkshops = em
						.createQuery("SELECT w FROM Workshop w WHERE w.name LIKE :name", Workshop.class)
						.setParameter("name", name + "%").getResultList();
			} else {
				foundWorkshops = em.createQuery("SELECT w FROM Workshop w", Workshop.class).getResultList();
			}
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return foundWorkshops;
	}

	@Override
	public Address persistWorkshopAddress(Address address, long workshopId) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		Workshop workshop = null;
		try {
			if ((workshop = findWorkshopById(workshopId)) != null) {
				workshop.setAddress(address);
				em.persist(address);
				tx.commit();
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return null;
		} finally {
			em.close();
		}
		return address;
	}

	@Override
	public <T> T persistElement(Class<T> persistentClass, T element, String name) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		try {
			if (findElementByName(persistentClass, name) == null) {
				em.persist(element);
				tx.commit();
			} else {
				element = null;
			}
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return null;
		} finally {
			em.close();
		}
		return element;
	}

	private <T> T findElementByName(Class<T> persistentClass, String name) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		T element = null;
		try {
			element = em.createQuery(
					"SELECT item FROM " + persistentClass.getSimpleName() + " item WHERE item.name = :name",
					persistentClass).setParameter("name", name).getSingleResult();
		} catch (NoResultException e) {
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return element;
	}

	@Override
	public Workshop updateWorkshop(Workshop workshop) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		Workshop updatedWorkshop = null;
		try {
			updatedWorkshop = em.merge(workshop);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return null;
		} finally {
			em.close();
		}
		return updatedWorkshop;
	}

	@Override
	public Person findOwner(long id, String name) {
		EntityManager em = getEntityManager();
		Person person;
		try {
			person = em
					.createQuery("SELECT person FROM Person person WHERE person.id = :id OR person.userName = :name",
							Person.class)
					.setParameter("id", id).setParameter("name", name).getSingleResult();
		} catch (Exception e) {
			e.printStackTrace(System.err);
			person = null;
		} finally {
			em.close();
		}
		return person;
	}

	@Override
	public Workshop deleteWorkshop(long workshopId) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		Workshop workshopReference;
		tx.begin();
		try {
			workshopReference = em.getReference(Workshop.class, workshopId);
			em.remove(workshopReference);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return null;
		} finally {
			em.close();
		}
		return workshopReference;
	}

	@Override
	public VehicleControlPoint deleteControlPoint(long controlPointId) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		VehicleControlPoint controlPoint;
		tx.begin();
		try {
			controlPoint = em.find(VehicleControlPoint.class, controlPointId);
			em.remove(controlPoint);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return null;
		} finally {
			em.close();
		}
		return controlPoint;
	}

	@Override
	public List<Request> getRequests(long adminId) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		List<Request> foundRequests = new ArrayList<>();
		try {
			foundRequests = em.createQuery(
					"SELECT req FROM Request req WHERE req.status = :status AND :adminId = ANY (SELECT recipient.id FROM req.recipients recipient)",
					Request.class).setParameter("status", RequestStatusEnum.ACTIVE).setParameter("adminId", adminId)
					.getResultList();
			tx.commit();
		} catch (NoResultException e) {
			return foundRequests;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return foundRequests;
	}

	@Override
	public void resolveRequest(long requestId) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		Request request = null;
		try {
			request = em.find(Request.class, requestId);
			request.setStatus(RequestStatusEnum.CLOSED);
			em.merge(request);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}

	}

	@Override
	public VehicleControlPoint updateControlPoint(VehicleControlPoint controlPoint) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		VehicleControlPoint updatedControlPoint = null;
		try {
			updatedControlPoint = em.merge(controlPoint);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return null;
		} finally {
			em.close();
		}
		return updatedControlPoint;
	}
}
