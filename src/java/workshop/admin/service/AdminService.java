package workshop.admin.service;

import java.util.List;

import workshop.model.Address;
import workshop.model.Person;
import workshop.model.Request;
import workshop.model.VehicleControlPoint;
import workshop.model.Workshop;

public interface AdminService {
	public List<Workshop> getWorkshops(String name);

	public Workshop getWorkshopById(long workshopId);

	public Workshop saveWorkshop(Workshop workshop);

	public VehicleControlPoint saveVehicleControlPoint(VehicleControlPoint vehicleControlPoint);

	public Workshop deleteWorkshop(long workshopId);

	public VehicleControlPoint deleteControlPoint(long controlPointId);

	public Address saveWorkshopAddress(Address address, long workshopId);

	public Workshop updateWorkshop(Workshop workshop);

	public Person findOwner(long id, String name);

	public List<Request> getRequests(long adminId);

	public void resolveRequest(long requestId);

	public VehicleControlPoint getControlPointById(long controlPointId);

	public VehicleControlPoint updateControlPoint(VehicleControlPoint controlPoint);
}
