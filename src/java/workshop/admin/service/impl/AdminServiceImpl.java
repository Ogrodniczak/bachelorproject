package workshop.admin.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import workshop.admin.dao.AdminDao;
import workshop.admin.service.AdminService;
import workshop.model.Address;
import workshop.model.Person;
import workshop.model.Request;
import workshop.model.VehicleControlPoint;
import workshop.model.Workshop;

@Service
public class AdminServiceImpl implements AdminService {

	@Autowired
	@Qualifier("adminDaoImpl")
	private AdminDao dao;

	@Override
	public List<Workshop> getWorkshops(String name) {
		return dao.getWorkshops(name);
	}

	@Override
	public Workshop getWorkshopById(long workshopId) {
		return dao.findWorkshopById(workshopId);
	}

	@Override
	public Workshop saveWorkshop(Workshop workshop) {
		return dao.persistElement(Workshop.class, workshop, workshop.getName());
	}

	@Override
	public Workshop deleteWorkshop(long workshopId) {
		return dao.deleteWorkshop(workshopId);
	}

	@Override
	public Address saveWorkshopAddress(Address address, long workshopId) {
		return dao.persistWorkshopAddress(address, workshopId);
	}

	@Override
	public VehicleControlPoint saveVehicleControlPoint(VehicleControlPoint vehicleControlPoint) {
		return dao.persistElement(VehicleControlPoint.class, vehicleControlPoint, vehicleControlPoint.getName());
	}

	@Override
	public Workshop updateWorkshop(Workshop workshop) {
		return dao.updateWorkshop(workshop);
	}

	@Override
	public Person findOwner(long id, String name) {
		return dao.findOwner(id, name);
	}

	@Override
	public List<Request> getRequests(long adminId) {
		return dao.getRequests(adminId);
	}

	@Override
	public void resolveRequest(long requestId) {
		dao.resolveRequest(requestId);
	}

	@Override
	public VehicleControlPoint deleteControlPoint(long controlPointId) {
		return dao.deleteControlPoint(controlPointId);
	}

	@Override
	public VehicleControlPoint getControlPointById(long controlPointId) {
		return dao.findControlPointById(controlPointId);
	}

	@Override
	public VehicleControlPoint updateControlPoint(VehicleControlPoint controlPoint) {
		return dao.updateControlPoint(controlPoint);
	}
}
