package workshop.admin.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import workshop.model.Workshop;

public class WorkshopValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return Workshop.class.equals(clazz);
	}

	@Override
	public void validate(Object object, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "validation.empty");

		Workshop workshop = (Workshop) object;
		if (workshop.getName().length() > 25) {
			errors.rejectValue("name", "validation.max_25_chars");
		}
		if (workshop.getOwner().getId() == 0 && StringUtils.isBlank(workshop.getOwner().getUserName())) {
			errors.rejectValue("owner.id", "validation.no_owner_data");
			errors.rejectValue("owner.userName", "validation.no_owner_data");
		}
	}

}
