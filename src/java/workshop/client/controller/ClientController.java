package workshop.client.controller;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import workshop.client.service.ClientServiceInterface;
import workshop.model.Calendar;
import workshop.model.CarAccessory;
import workshop.model.CarService;
import workshop.model.Order;
import workshop.model.OrderStatusEnum;
import workshop.model.Person;
import workshop.model.PersonTypeEnum;
import workshop.model.Request;
import workshop.model.Station;
import workshop.model.VehicleControlPoint;
import workshop.model.Visit;
import workshop.model.VisitStatusEnum;
import workshop.model.Workshop;

@Controller
public class ClientController {

	@Autowired
	@Qualifier("clientServiceImpl")
	private ClientServiceInterface service;

	@GetMapping("/clientPage")
	public ModelAndView clientPage(HttpServletRequest request, HttpSession session, ModelMap model) {
		if (!authenticateUser(session))
			return new ModelAndView("index");

		ModelAndView mv = new ModelAndView("client");
		Person client = (Person) session.getAttribute("sessionUser");

		String cancelOrderSuccess = request.getParameter("cancelOrderSuccess");
		String cancelOrderError = request.getParameter("cancelOrderError");

		if (cancelOrderSuccess != null) {
			model.addAttribute("cancelOrderSuccess", cancelOrderSuccess);
		}
		if (cancelOrderError != null) {
			model.addAttribute("cancelOrderError", cancelOrderError);
		}

		List<Request> notifications = service.getNotifications(client.getId());
		if (notifications.isEmpty()) {
			model.addAttribute("getNotificationsResponse", "client.notification.none");
		}

		List<Order> currentOrders = service.getCurrentOrders(client.getId());
		if (currentOrders.isEmpty()) {
			model.addAttribute("getCurrentOrdersResponse", "client.orders_current.none");
		}

		List<Order> previousOrders = service.getPreviousOrders(client.getId());
		if (previousOrders.isEmpty()) {
			model.addAttribute("getPreviousOrdersResponse", "client.orders_previous.none");
		}
		model.addAttribute("currentOrders", currentOrders);
		model.addAttribute("previousOrders", previousOrders);
		model.addAttribute("notifications", notifications);
		mv.addObject(model);
		return mv;
	}

	@GetMapping("/getCurrentOrders/{clientId}")
	public ModelAndView getCurrentOrders(ModelMap model, @PathVariable("clientId") long clientId) {
		List<Order> currentOrders = service.getCurrentOrders(clientId);
		if (currentOrders.isEmpty()) {
			model.addAttribute("getCurrentOrdersResponse", "client.orders_current.none");
		}
		model.addAttribute("currentOrders", currentOrders);
		model.addAttribute("showCurrentOrders", true);
		return new ModelAndView("redirect:/clientPage", model);
	}

	@RequestMapping("/getPreviousOrders/{clientId}")
	public ModelAndView getPreviousOrders(ModelMap model, @PathVariable("clientId") long clientId) {
		List<Order> previousOrders = service.getPreviousOrders(clientId);
		if (previousOrders.isEmpty()) {
			model.addAttribute("getPreviousOrdersResponse", "client.orders_previous.none");
		}
		model.addAttribute("previousOrders", previousOrders);
		model.addAttribute("showPreviousOrders", true);
		return new ModelAndView("redirect:/clientPage", model);
	}

	@RequestMapping("/clientRemoveNotification/{notificationId}")
	public ModelAndView removeNotification(@PathVariable("notificationId") long notificationId, ModelMap model) {
		if (!service.deleteNotification(notificationId)) {
			model.addAttribute("removeNotificationError", "client.add.notification.error");
		}
		return new ModelAndView("redirect:/clientPage", model);
	}

	@RequestMapping("addServiceToCart/{view}/{serviceId}")
	public ModelAndView addServiceToCart(@PathVariable("view") String viewType,
			@PathVariable("serviceId") long serviceId, HttpSession session, ModelMap model) {

		Person user = (Person) session.getAttribute("sessionUser");
		ModelAndView mv = null;
		CarService carService = service.getServiceById(serviceId);

		if (viewType.equals("service-view")) {
			model.addAttribute("service", service);
			mv = new ModelAndView("redirect:/serviceView/" + serviceId);
		} else {
			mv = new ModelAndView("redirect:/workshopView/" + carService.getWorkshop().getId());
			Workshop workshop = service.getWorkshopById(carService.getWorkshop().getId());
			model.addAttribute("workshopModel", workshop);
			model.addAttribute("accessories", workshop.getAccessories());
			model.addAttribute("services", workshop.getServices());
			model.addAttribute("nrOfStations", workshop.getStations().size());

			if (workshop.getAccessories().isEmpty()) {
				model.addAttribute("getEmptyAccessoriesResponse", "home.accessory.none");
			}
			if (workshop.getServices().isEmpty()) {
				model.addAttribute("getEmptyServicesResponse", "home.service.none");
			}
		}

		if (!authenticateUser(session)) {
			model.addAttribute("addServiceError", "client.login.error");
			mv.addObject(model);
			return mv;
		}

		if (service.addServiceToCart(user.getId(), serviceId)) {
			model.addAttribute("addServiceSuccess", "client.add.service");
		} else {
			model.addAttribute("addServiceError", "client.add.service.error");
		}
		mv.addObject(model);
		return mv;
	}

	@RequestMapping("addAccessoryToCart/{view}/{accessoryId}")
	public ModelAndView addAccessoryToCart(@PathVariable("view") String viewType,
			@PathVariable("accessoryId") long accessoryId, HttpSession session, ModelMap model) {

		Person user = (Person) session.getAttribute("sessionUser");
		ModelAndView mv = null;
		CarAccessory accessory = service.getAccessoryById(accessoryId);

		if (viewType.equals("accessory-view")) {
			model.addAttribute("accessory", accessory);
			mv = new ModelAndView("redirect:/accessoryView/" + accessoryId);
		} else {
			mv = new ModelAndView("redirect:/workshopView/" + accessory.getWorkshop().getId());
			Workshop workshop = service.getWorkshopById(accessory.getWorkshop().getId());

			model.addAttribute("workshopModel", workshop);
			model.addAttribute("accessories", workshop.getAccessories());
			model.addAttribute("services", workshop.getServices());
			model.addAttribute("nrOfStations", workshop.getStations().size());

			if (workshop.getAccessories().isEmpty()) {
				model.addAttribute("getEmptyAccessoriesResponse", "home.accessory.none");
			}
			if (workshop.getServices().isEmpty()) {
				model.addAttribute("getEmptyServicesResponse", "home.service.none");
			}
		}

		if (!authenticateUser(session)) {
			model.addAttribute("addAccessoryError", "client.login.error");
			mv.addObject(model);
			return mv;
		}

		if (service.addAccessoryToCart(user.getId(), accessoryId)) {
			model.addAttribute("addAccessorySuccess", "client.add.accessory");
		} else {
			model.addAttribute("addAccessoryError", "client.add.accessory.error");
		}
		mv.addObject(model);
		return mv;
	}

	@GetMapping("visitDetails/{visitId}")
	public ModelAndView getVisitDetails(ModelMap model, @PathVariable("visitId") long visitId) {
		Visit visit = service.getVisitById(visitId);
		List<LocalTime> hours = visit.getHours();
		String visitHours = hours.get(0) + " - " + hours.get(hours.size() - 1).plusHours(1);
		model.addAttribute("visit", visit);
		model.addAttribute("visitHours", visitHours);
		return new ModelAndView("visit-view", model);
	}

	private void appendMessages(ModelAndView mv, HttpServletRequest request) {
		String reserveVisitOrderError = request.getParameter("reserveVisitOrderError");
		String reserveVisitLoginError = request.getParameter("reserveVisitLoginError");
		String reserveVisitTimeError = request.getParameter("reserveVisitTimeError");
		String reserveVisitError = request.getParameter("reserveVisitError");
		String reserveVisitSuccess = request.getParameter("reserveVisitSuccess");
		String cancelVisitLoginError = request.getParameter("cancelVisitLoginError");
		String cancelVisitError = request.getParameter("cancelVisitError");
		String cancelVisitSuccess = request.getParameter("cancelVisitSuccess");

		if (reserveVisitOrderError != null) {
			mv.addObject("reserveVisitOrderError", reserveVisitOrderError);
		}
		if (reserveVisitLoginError != null) {
			mv.addObject("reserveVisitLoginError", reserveVisitLoginError);
		}
		if (reserveVisitTimeError != null) {
			mv.addObject("reserveVisitTimeError", reserveVisitTimeError);
		}
		if (reserveVisitError != null) {
			mv.addObject("reserveVisitError", reserveVisitError);
		}
		if (reserveVisitSuccess != null) {
			mv.addObject("reserveVisitSuccess", reserveVisitSuccess);
		}
		if (cancelVisitLoginError != null) {
			mv.addObject("cancelVisitLoginError", cancelVisitLoginError);
		}
		if (cancelVisitError != null) {
			mv.addObject("cancelVisitError", cancelVisitError);
		}
		if (cancelVisitSuccess != null) {
			mv.addObject("cancelVisitSuccess", cancelVisitSuccess);
		}
	}

	@GetMapping("clientGetCalendarView/{item}/{itemId}/{orderId}/{index}")
	public ModelAndView getCalendarView(@PathVariable("item") String item, @PathVariable("itemId") long itemId,
			@PathVariable("orderId") Long orderId, @PathVariable("index") int index, HttpSession session,
			HttpServletRequest request, String reservationResponse) {

		Map<Calendar, Map<LocalTime, Visit>> calendarMap;
		ModelAndView mv = new ModelAndView("calendar-view");
		appendMessages(mv, request);
		if (item.equalsIgnoreCase("workshop")) {
			Workshop workshop = service.fetchWorkshop(itemId);
			for (Station station : workshop.getStations()) {
				calendarMap = service.getStationCalendars(station.getId(), index);
				if (!calendarMap.isEmpty()) {
					station.setCalendarVisitMap(calendarMap);
				}
			}
			mv.addObject("stations", workshop.getStations());
			mv.addObject("workshopId", itemId);
			mv.addObject("itemName", workshop.getName());
			mv.addObject("item", item);
		} else if (item.equalsIgnoreCase("controlPoint")) {
			VehicleControlPoint controlPoint = service.fetchControlPoint(itemId);
			for (Station station : controlPoint.getStations()) {
				calendarMap = service.getStationCalendars(station.getId(), index);
				if (!calendarMap.isEmpty()) {
					station.setCalendarVisitMap(calendarMap);
				}
			}
			mv.addObject("stations", controlPoint.getStations());
			mv.addObject("controlPointId", itemId);
			mv.addObject("itemName", controlPoint.getName());
			mv.addObject("item", item);
		} else {
			return new ModelAndView("index");
		}
		List<LocalTime> allHours = new ArrayList<>();
		for (int hour = 6; hour < 19; hour++) {
			allHours.add(LocalTime.of(hour, 0));
		}
		mv.addObject("allHours", allHours);
		if (authenticateUser(session) && orderId != null && orderId != 0) {
			Person client = (Person) session.getAttribute("sessionUser");
			Order order = service.getOrderById(client.getId(), orderId);
			if (order != null) {
				mv.addObject("order", order);
			}
		}
		if (!StringUtils.isBlank(reservationResponse)) {
			mv.addObject("reservationResponse", reservationResponse);
		}
		mv.addObject("currentIndex", index);
		return mv;
	}

	@RequestMapping("reserveVisit/{calendarId}/{localTimeId}/{orderId}/{currentIndex}")
	public ModelAndView reserveVisit(ModelMap model, HttpSession session, @PathVariable("calendarId") long calendarId,
			@PathVariable("localTimeId") String timeString, @PathVariable(name = "orderId") Long orderId,
			@PathVariable("currentIndex") int index) {

		Calendar day = service.getCalendarById(calendarId);

		if (day.getStation().getWorkshop() != null) {
			return reserveWorkshopVisit(model, session, day, timeString, orderId, index);
		} else if (day.getStation().getVehicleControlPoint() != null) {
			return reserveControlPointVisit(model, session, day, timeString, index);
		}
		return new ModelAndView("redirect:/");
	}

	@RequestMapping("cancelVisit/{itemName}/{visitId}/{currentIndex}")
	public ModelAndView cancelVisit(ModelMap model, HttpSession session, @PathVariable("itemName") String itemName,
			@PathVariable("visitId") long visitId, @PathVariable("currentIndex") int index) {
		Visit visit = service.getVisitById(visitId);
		if (itemName.equals("workshop")) {
			return cancelWorkshopVisit(model, session, visit, index);
		} else if (itemName.equals("controlPoint")) {
			return cancelControlPointVisit(model, session, visit, index);
		}
		return new ModelAndView("redirect:/");
	}

	@RequestMapping("cancelOrder/{orderId}")
	public ModelAndView cancelOrder(ModelMap model, @PathVariable("orderId") long orderId) {
		if (!service.cancelOrder(orderId)) {
			model.addAttribute("cancelOrderError", "client.order.cancel.error");
		}
		model.addAttribute("cancelOrderSuccess", "client.order.cancel.success");
		return new ModelAndView("redirect:/clientPage", model);
	}

	private ModelAndView cancelWorkshopVisit(ModelMap model, HttpSession session, Visit visit, int index) {
		Workshop workshop = visit.getOrder().getWorkshop();
		if (!authenticateUser(session)) {
			model.addAttribute("cancelVisitLoginError", "client.login.error");
			return new ModelAndView("redirect:/clientGetCalendarView/workshop/" + workshop.getId() + "/"
					+ visit.getOrder().getId() + "/" + index, model);
		}

		if (!service.removeVisit(visit)) {
			model.addAttribute("cancelVisitError", "client.visit.cancel.error");
		} else {
			model.addAttribute("cancelVisitSuccess", "client.visit.cancel.success");
		}
		return new ModelAndView("redirect:/clientGetCalendarView/workshop/" + workshop.getId() + "/"
				+ visit.getOrder().getId() + "/" + index, model);
	}

	private ModelAndView cancelControlPointVisit(ModelMap model, HttpSession session, Visit visit, int index) {
		VehicleControlPoint controlPoint = visit.getDayOfVisit().getStation().getVehicleControlPoint();
		if (!authenticateUser(session)) {
			model.addAttribute("cancelVisitLoginError", "client.login.error");
			return new ModelAndView(
					"redirect:/clientGetCalendarView/controlPoint/" + controlPoint.getId() + "/0/" + index, model);
		}

		if (!service.removeVisit(visit)) {
			model.addAttribute("cancelVisitError", "client.visit.cancel.error");
		} else {
			model.addAttribute("cancelVisitSuccess", "client.visit.cancel.success");
		}
		return new ModelAndView("redirect:/clientGetCalendarView/controlPoint/" + controlPoint.getId() + "/0/" + index,
				model);
	}

	private ModelAndView reserveWorkshopVisit(ModelMap model, HttpSession session, Calendar day, String timeString,
			Long orderId, int index) {
		Workshop workshop = day.getStation().getWorkshop();
		if (!authenticateUser(session)) {
			model.addAttribute("reserveVisitLoginError", "client.login.error");
			return new ModelAndView(
					"redirect:/clientGetCalendarView/workshop/" + workshop.getId() + "/" + orderId + "/" + index);
		}

		if (orderId == null || orderId == 0) {
			model.addAttribute("reserveVisitOrderError", "client.visit.reserve.error.order");
			return new ModelAndView(
					"redirect:/clientGetCalendarView/workshop/" + workshop.getId() + "/" + orderId + "/" + index,
					model);
		}

		Person user = (Person) session.getAttribute("sessionUser");
		Order order = service.getOrderById(user.getId(), orderId);
		LocalTime time = null;

		try {
			time = LocalTime.parse(timeString);
		} catch (DateTimeParseException e) {
			return new ModelAndView(
					"redirect:/clientGetCalendarView/workshop/" + workshop.getId() + "/" + orderId + "/" + index);
		}

		Set<LocalTime> hoursBooked = new HashSet<>();
		List<LocalTime> visitHours = new ArrayList<>();
		visitHours.add(time);

		for (Visit visit : day.getVisits()) {
			hoursBooked.addAll(visit.getHours());
		}

		for (int i = 60; i < order.getTime(); i += 60) {
			LocalTime hour = time.plusHours(1);
			if (hoursBooked.contains(hour)) {
				model.addAttribute("reserveVisitTimeError", "client.visit.reserve.time");
				return new ModelAndView(
						"redirect:/clientGetCalendarView/workshop/" + workshop.getId() + "/" + orderId + "/" + index,
						model);
			}
			visitHours.add(hour);
		}

		Visit visit = new Visit();
		visit.setStatus(VisitStatusEnum.SCHEDULED);
		visit.setDayOfVisit(day);
		visit.setHours(visitHours);
		visit.setClient(user);

		order.setStatus(OrderStatusEnum.SCHEDULED);

		visit.setOrder(order);
		order.setVisit(visit);

		if (service.persistVisit(visit) == null) {
			model.addAttribute("reserveVisitError", "client.visit.reserve.error");
			return new ModelAndView(
					"redirect:/clientGetCalendarView/workshop/" + workshop.getId() + "/" + orderId + "/" + index,
					model);
		}

		model.addAttribute("reserveVisitSuccess", "client.visit.reserve.success");
		return new ModelAndView("redirect:/clientGetCalendarView/workshop/" + workshop.getId() + "/0/" + index, model);
	}

	private ModelAndView reserveControlPointVisit(ModelMap model, HttpSession session, Calendar day, String timeString,
			int index) {
		VehicleControlPoint controlPoint = day.getStation().getVehicleControlPoint();
		if (!authenticateUser(session)) {
			model.addAttribute("reserveVisitLoginError", "client.login.error");
			return new ModelAndView(
					"redirect:/clientGetCalendarView/controlPoint/" + controlPoint.getId() + "/0/" + index);
		}

		Person user = (Person) session.getAttribute("sessionUser");
		LocalTime time = null;

		try {
			time = LocalTime.parse(timeString);
		} catch (DateTimeParseException e) {
			return new ModelAndView(
					"redirect:/clientGetCalendarView/controlPoint/" + controlPoint.getId() + "/0/" + index);
		}

		Visit visit = new Visit();
		visit.setStatus(VisitStatusEnum.SCHEDULED);
		visit.setDayOfVisit(day);
		visit.setHours(Arrays.asList(time));
		visit.setClient(user);

		if (service.persistVisit(visit) == null) {
			model.addAttribute("reserveVisitError", "client.visit.reserve.error");
			return new ModelAndView(
					"redirect:/clientGetCalendarView/controlPoint/" + controlPoint.getId() + "/0/" + index);
		}

		model.addAttribute("reserveVisitSuccess", "client.visit.reserve.success");
		return new ModelAndView("redirect:/clientGetCalendarView/controlPoint/" + controlPoint.getId() + "/0/" + index);
	}

	private boolean authenticateUser(HttpSession session) {
		Person user = (Person) session.getAttribute("sessionUser");
		if (user == null || !user.getType().equals(PersonTypeEnum.CLIENT)) {
			return false;
		}
		return true;
	}
}
