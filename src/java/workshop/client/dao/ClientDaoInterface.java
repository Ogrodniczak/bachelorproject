package workshop.client.dao;

import java.util.List;

import workshop.model.Calendar;
import workshop.model.Order;
import workshop.model.Request;
import workshop.model.Visit;

public interface ClientDaoInterface {
	public List<Order> getCurrentOrders(long clientId);

	public List<Order> getPreviousOrders(long clientId);

	public boolean addServiceToCart(long clientId, long serviceId);

	public boolean addAccessoryToCart(long clientId, long accessoryId);

	public Order getOrderById(long clientId, long orderId);

	public Calendar getCalendarById(long calendarId);

	public Visit getVisitById(long visitId);

	public boolean removeVisit(Visit visit, Request request);

	public Visit persistVisit(Visit visit, Request request);

	public void updateOrder(Order order);

	public List<Request> getNotifications(long clientId);

	public boolean cancelOrder(long orderId);
}
