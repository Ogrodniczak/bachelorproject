package workshop.client.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.transaction.Transactional;

import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import workshop.client.dao.ClientDaoInterface;
import workshop.model.Calendar;
import workshop.model.CarAccessory;
import workshop.model.CarService;
import workshop.model.Order;
import workshop.model.OrderStatusEnum;
import workshop.model.Person;
import workshop.model.Request;
import workshop.model.RequestStatusEnum;
import workshop.model.Visit;

@Repository
@Transactional
public class ClientDaoImpl implements ClientDaoInterface {

	private static final EntityManagerFactory entityManagerFactory = Persistence
			.createEntityManagerFactory("persistenceUnit");

	private EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}
	
	@PreDestroy
	public void finish() {
		entityManagerFactory.close();
	}
	
	@Override
	public List<Order> getCurrentOrders(long clientId) {
		EntityManager em = getEntityManager();
		List<Order> foundOrders = new ArrayList<>();
		try {
			foundOrders = em.createQuery(
					"SELECT o FROM Order o WHERE o.client.id = :clientId AND (o.status = :new OR o.status = :scheduled)",
					Order.class).setParameter("clientId", clientId).setParameter("new", OrderStatusEnum.NEW)
					.setParameter("scheduled", OrderStatusEnum.SCHEDULED).getResultList();
			for (Order o : foundOrders) {
				Hibernate.initialize(o.getAccessories());
				Hibernate.initialize(o.getServices());
				if(o.getVisit() != null) {
					Hibernate.initialize(o.getVisit().getHours());
				}
			}
		} catch (NoResultException e) {
			return foundOrders;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return foundOrders;
	}

	@Override
	public List<Order> getPreviousOrders(long clientId) {
		EntityManager em = getEntityManager();
		List<Order> foundOrders = new ArrayList<>();
		try {
			foundOrders = em
					.createQuery("SELECT o FROM Order o WHERE o.client.id = :clientId AND o.status = :status",
							Order.class)
					.setParameter("clientId", clientId).setParameter("status", OrderStatusEnum.TERMINATED)
					.getResultList();
			for (Order o : foundOrders) {
				Hibernate.initialize(o.getAccessories());
				Hibernate.initialize(o.getServices());
				if(o.getVisit() != null) {
					Hibernate.initialize(o.getVisit().getHours());
				}
			}
		} catch (NoResultException e) {
			return foundOrders;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return foundOrders;
	}

	@Override
	public boolean addServiceToCart(long clientId, long serviceId) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();

		CarService foundService = em.find(CarService.class, serviceId);
		Order order = null;
		try {
			if (!foundService.isAvailable()) {
				throw new Exception("[EXCEPTION] - Could not add to cart - service is not available");
			}
			order = em
					.createQuery("SELECT o FROM Order o WHERE o.client.id = :clientId AND o.workshop.id = :workshopId"
							+ " AND o.status = :status", Order.class)
					.setParameter("clientId", clientId).setParameter("workshopId", foundService.getWorkshop().getId())
					.setParameter("status", OrderStatusEnum.NEW).getSingleResult();
			Map<CarService, Integer> services = order.getServices();
			if (services.containsKey(foundService)) {
				services.put(foundService, services.get(foundService) + 1);
			} else {
				services.put(foundService, 1);
			}
			order.setCost(order.getCost() + foundService.getCost() * (100 - foundService.getDiscount()) * 0.01);
			order.setTime(order.getTime() + foundService.getTime());
			em.merge(order);
			tx.commit();
		} catch (NoResultException e) {
			order = new Order();
			Person client = em.find(Person.class, clientId);
			order.setClient(client);
			Map<CarService, Integer> services = new HashMap<>();
			services.put(foundService, 1);
			order.setServices(services);
			order.setCost(foundService.getCost() * (100 - foundService.getDiscount()) * 0.01);
			order.setStatus(OrderStatusEnum.NEW);
			order.setWorkshop(foundService.getWorkshop());
			order.setTime(foundService.getTime());
			em.persist(order);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return false;
		} finally {
			em.close();
		}
		
		return true;
	}

	@Override
	public boolean addAccessoryToCart(long clientId, long accessoryId) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();

		CarAccessory foundAccessory = em.find(CarAccessory.class, accessoryId);
		Order order = null;
		try {
			if (!foundAccessory.isAvailable() || foundAccessory.getAmount() == 0) {
				throw new Exception("[EXCEPTION] - Could not add to cart - accessory is not available");
			}
			order = em
					.createQuery("SELECT o FROM Order o WHERE o.client.id = :clientId AND o.workshop.id = :workshopId"
							+ " AND o.status = :status", Order.class)
					.setParameter("clientId", clientId).setParameter("workshopId", foundAccessory.getWorkshop().getId())
					.setParameter("status", OrderStatusEnum.NEW).getSingleResult();

			Map<CarAccessory, Integer> accessories = order.getAccessories();
			if (accessories.containsKey(foundAccessory)) {
				accessories.put(foundAccessory, accessories.get(foundAccessory) + 1);
			} else {
				accessories.put(foundAccessory, 1);
			}

			order.setCost(order.getCost() + foundAccessory.getCost() * (100 - foundAccessory.getDiscount()) * 0.01);
			em.merge(order);
			tx.commit();

		} catch (NoResultException e) {
			order = new Order();
			Person client = em.find(Person.class, clientId);
			order.setClient(client);
			Map<CarAccessory, Integer> accessories = new HashMap<>();
			accessories.put(foundAccessory, 1);
			order.setAccessories(accessories);
			order.setCost(foundAccessory.getCost() * (100 - foundAccessory.getDiscount()) * 0.01);
			order.setStatus(OrderStatusEnum.NEW);
			order.setWorkshop(foundAccessory.getWorkshop());
			em.persist(order);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return false;
		} finally {
			em.close();
		}
		return true;
	}

	@Override
	public Order getOrderById(long clientId, long orderId) {
		EntityManager em = getEntityManager();
		Order order;
		try {
			order = em
					.createQuery("SELECT o FROM Order o where o.id = :orderId AND"
							+ " o.client.id = :clientId AND o.status != :status", Order.class)
					.setParameter("orderId", orderId).setParameter("clientId", clientId)
					.setParameter("status", OrderStatusEnum.TERMINATED).getSingleResult();
			Hibernate.initialize(order.getAccessories());
			Hibernate.initialize(order.getServices());
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return null;
		} finally {
			em.close();
		}
		return order;
	}

	@Override
	public Calendar getCalendarById(long calendarId) {
		EntityManager em = getEntityManager();
		Calendar calendar = null;
		try {
			calendar = em.find(Calendar.class, calendarId);
			for(Visit v : calendar.getVisits()) {
				v.getHours().size();
			}
		} catch(Exception e) {
			e.printStackTrace(System.err);
			return null;
		} finally {
			em.close();
		}
		return calendar;
	}

	@Override
	public Visit getVisitById(long visitId) {
		EntityManager em = getEntityManager();
		Visit visit = null;
		try {
			visit = em.find(Visit.class, visitId);
			Hibernate.initialize(visit.getHours());
			if(visit.getOrder() != null) {
				Hibernate.initialize(visit.getOrder().getAccessories());
				Hibernate.initialize(visit.getOrder().getServices());
			}
		} catch(Exception e) {
			e.printStackTrace(System.err);
			return null;
		} finally {
			em.close();
		}
		return visit;
	}

	@Override
	public boolean removeVisit(Visit visit, Request request) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		try {
			Visit visitReference = em.getReference(Visit.class, visit.getId());
			em.remove(visitReference);
			em.remove(visitReference.getOrder());
			em.persist(request);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return false;
		} finally {
			em.close();
		}
		return true;
	}

	@Override
	public Visit persistVisit(Visit visit, Request request) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		try {
			em.merge(visit);
			em.persist(request);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return null;
		} finally {
			em.close();
		}
		return visit;
	}

	@Override
	public void updateOrder(Order order) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		try {
			em.merge(order);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
	}

	@Override
	public List<Request> getNotifications(long clientId) {
		EntityManager em = getEntityManager();
		List<Request> foundRequests = new ArrayList<>();
		try {
			foundRequests = em.createQuery(
					"SELECT r FROM Request r WHERE :clientId = ANY (SELECT recipient.id FROM r.recipients recipient) AND r.status = :status",
					Request.class).setParameter("clientId", clientId).setParameter("status", RequestStatusEnum.ACTIVE)
					.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			em.close();
		}
		return foundRequests;
	}

	@Override
	public boolean cancelOrder(long orderId) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		try {
			Order orderReference = em.getReference(Order.class, orderId);
			em.remove(orderReference);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return false;
		} finally {
			em.close();
		}
		return true;
	}
}
