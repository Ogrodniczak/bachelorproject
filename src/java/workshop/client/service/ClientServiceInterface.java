package workshop.client.service;

import java.time.LocalTime;
import java.util.List;
import java.util.Map;

import workshop.model.Calendar;
import workshop.model.CarAccessory;
import workshop.model.CarService;
import workshop.model.Order;
import workshop.model.Person;
import workshop.model.Request;
import workshop.model.VehicleControlPoint;
import workshop.model.Visit;
import workshop.model.Workshop;

public interface ClientServiceInterface {
	public List<Order> getCurrentOrders(long clientId);

	public List<Order> getPreviousOrders(long clientId);

	public boolean sendRequest(String content, Person user);

	public boolean addServiceToCart(long clientId, long serviceId);

	public boolean addAccessoryToCart(long clientId, long accessoryId);

	public CarAccessory getAccessoryById(long id);

	public CarService getServiceById(long serviceId);

	public Workshop getWorkshopById(long workshopId);

	public Workshop fetchWorkshop(long workshopId);
	
	public VehicleControlPoint getControlPointById(long controlPointId);

	public VehicleControlPoint fetchControlPoint(long controlPointId);
	
	public Map<Calendar, Map<LocalTime, Visit>> getStationCalendars(long stationId, int index);

	public Order getOrderById(long clientId, long orderId);

	public Calendar getCalendarById(long calendarId);

	public Visit getVisitById(long visitId);

	public Visit persistVisit(Visit visit);

	public boolean removeVisit(Visit visit);

	public void updateOrder(Order order);

	public List<Request> getNotifications(long clientId);

	public boolean deleteNotification(long notificationId);

	public boolean cancelOrder(long orderId);
}
