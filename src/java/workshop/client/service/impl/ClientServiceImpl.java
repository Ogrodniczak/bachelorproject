package workshop.client.service.impl;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import workshop.MailSender;
import workshop.client.dao.ClientDaoInterface;
import workshop.client.service.ClientServiceInterface;
import workshop.home.dao.HomePageDao;
import workshop.model.Calendar;
import workshop.model.CarAccessory;
import workshop.model.CarService;
import workshop.model.Order;
import workshop.model.Person;
import workshop.model.PersonTypeEnum;
import workshop.model.Request;
import workshop.model.RequestStatusEnum;
import workshop.model.Station;
import workshop.model.VehicleControlPoint;
import workshop.model.Visit;
import workshop.model.Workshop;
import workshop.owner.dao.OwnerDaoInterface;

@Service
public class ClientServiceImpl implements ClientServiceInterface {

	@Autowired
	@Qualifier("clientDaoImpl")
	private ClientDaoInterface dao;

	@Autowired
	@Qualifier("ownerDaoImpl")
	private OwnerDaoInterface ownerDao;

	@Autowired
	@Qualifier("homePageDaoImpl")
	private HomePageDao homePageDao;

	@Override
	public List<Order> getCurrentOrders(long clientId) {
		return dao.getCurrentOrders(clientId);
	}

	@Override
	public List<Order> getPreviousOrders(long clientId) {
		return dao.getPreviousOrders(clientId);
	}

	@Override
	public boolean sendRequest(String content, Person user) {
		List<Person> recipients = ownerDao.getRecipients(PersonTypeEnum.ADMIN);
		Request request = new Request();
		request.setContent(content);
		request.setStatus(RequestStatusEnum.ACTIVE);
		request.setDate(LocalDateTime.now().withNano(0));
		request.setSender(user);
		request.setRecipients(recipients);
		ownerDao.sendRequest(request);
		MailSender.sendMail(request);
		return true;
	}

	@Override
	public boolean addServiceToCart(long clientId, long serviceId) {
		return dao.addServiceToCart(clientId, serviceId);
	}

	@Override
	public boolean addAccessoryToCart(long clientId, long accessoryId) {
		return dao.addAccessoryToCart(clientId, accessoryId);
	}

	@Override
	public CarAccessory getAccessoryById(long id) {
		return homePageDao.getItemById(id, CarAccessory.class);
	}

	@Override
	public CarService getServiceById(long serviceId) {
		return homePageDao.getItemById(serviceId, CarService.class);
	}

	@Override
	public Workshop getWorkshopById(long workshopId) {
		return ownerDao.findWorkshopById(workshopId);
	}

	@Override
	public VehicleControlPoint getControlPointById(long controlPointId) {
		return ownerDao.getControlPointById(controlPointId);
	}

	@Override
	public Map<Calendar, Map<LocalTime, Visit>> getStationCalendars(long stationId, int index) {
		List<Calendar> calendars = ownerDao.getStationCalendars(stationId, index);
		Map<Calendar, Map<LocalTime, Visit>> calendarVisitMap = new TreeMap<>(new CalendarComparator());
		Map<LocalTime, Visit> visitHourMap = new TreeMap<>();
		for (Calendar calendar : calendars) {
			for (Visit visit : calendar.getVisits()) {
				visit.getHours().forEach(hour -> visitHourMap.put(hour, visit));
			}
			calendar.getHours().forEach(hour -> visitHourMap.putIfAbsent(hour, null));
			calendarVisitMap.put(calendar, new TreeMap<>(visitHourMap));
			visitHourMap.clear();
		}

		return calendarVisitMap;
	}

	private class CalendarComparator implements Comparator<Calendar> {
		@Override
		public int compare(Calendar a, Calendar b) {
			if (a.getDate().isBefore(b.getDate())) {
				return -1;
			}
			return 1;
		}
	}

	@Override
	public Order getOrderById(long clientId, long orderId) {
		return dao.getOrderById(clientId, orderId);
	}

	@Override
	public Calendar getCalendarById(long calendarId) {
		return dao.getCalendarById(calendarId);
	}

	@Override
	public Visit persistVisit(Visit visit) {
		Request request = new Request();
		Station station = visit.getDayOfVisit().getStation();
		Person client = visit.getClient();
		Person owner;
		ResourceBundle bundle = ResourceBundle.getBundle("language");
		String subject;

		if (station.getWorkshop() == null) {
			owner = station.getVehicleControlPoint().getOwner();
			subject = MessageFormat.format(bundle.getString("notification.visit.reserve.control_point"),
					station.getVehicleControlPoint().getName());
		} else {
			owner = station.getWorkshop().getOwner();
			subject = MessageFormat.format(bundle.getString("notification.visit.reserve.workshop"),
					station.getWorkshop().getName());
		}

		List<LocalTime> hours = visit.getHours();
		String content = MessageFormat.format(bundle.getString("notification.visit.reserve"), client.getFirstName(),
				client.getSurname(), client.getUserName()) + "<br>" + bundle.getString("date") + ": "
				+ visit.getDayOfVisit().getDate() + "<br>" + bundle.getString("hours") + ": " + hours.get(0) + " - "
				+ hours.get(hours.size() - 1).plusHours(1);

		request.setSubject(subject);
		request.setContent(content);
		request.setDate(LocalDateTime.now().withNano(0));
		request.setSender(client);

		request.setRecipients(Arrays.asList(owner));
		request.setStatus(RequestStatusEnum.ACTIVE);

		switch (MailSender.sendMail(request)) {
		case DELIVERED:
			System.out.println(
					"[MAIL - SUCCESS] Mail with request to reserve visit " + visit.getId() + " sent successfully");
			break;
		case ERROR:
			System.err.println("[MAIL - ERROR] Unexpected error occurred while sending the mail message");
			break;
		case NO_RECIPIENTS:
			System.err.println("[MAIL - ERROR] Mail cannot be sent - no recipients provided");
			break;
		case NO_SENDER:
			System.err.println("[MAIL - ERROR] Mail cannot be sent - no sender provided");
			break;
		default:
			break;
		}

		return dao.persistVisit(visit, request);
	}

	@Override
	public Visit getVisitById(long visitId) {
		return dao.getVisitById(visitId);
	}

	@Override
	public boolean removeVisit(Visit visit) {
		Request request = new Request();
		Station station = visit.getDayOfVisit().getStation();
		Person client = visit.getClient();
		Person owner;
		ResourceBundle bundle = ResourceBundle.getBundle("language");
		String subject;

		if (station.getWorkshop() == null) {
			owner = station.getVehicleControlPoint().getOwner();
			subject = MessageFormat.format(bundle.getString("notification.visit.cancel.control_point"),
					station.getVehicleControlPoint().getName());
		} else {
			owner = station.getWorkshop().getOwner();
			subject = MessageFormat.format(bundle.getString("notification.visit.cancel.workshop"),
					station.getWorkshop().getName());
		}

		List<LocalTime> hours = visit.getHours();
		String content = MessageFormat.format(bundle.getString("notification.visit.cancel"), client.getFirstName(),
				client.getSurname(), client.getUserName()) + "<br>" + bundle.getString("date") + ": "
				+ visit.getDayOfVisit().getDate() + "<br>" + bundle.getString("hours") + ": " + hours.get(0) + " - "
				+ hours.get(hours.size() - 1).plusHours(1);

		request.setSubject(subject);
		request.setContent(content);
		request.setDate(LocalDateTime.now().withNano(0));
		request.setSender(client);

		request.setRecipients(Arrays.asList(owner));
		request.setStatus(RequestStatusEnum.ACTIVE);
		if (!dao.removeVisit(visit, request)) {
			return false;
		}

		switch (MailSender.sendMail(request)) {
		case DELIVERED:
			System.out.println(
					"[MAIL - SUCCESS] Mail with request to cancel visit " + visit.getId() + " sent successfully");
			break;
		case ERROR:
			System.err.println("[MAIL - ERROR] Unexpected error occurred while sending the mail message");
			break;
		case NO_RECIPIENTS:
			System.err.println("[MAIL - ERROR] Mail cannot be sent - no recipients provided");
			break;
		case NO_SENDER:
			System.err.println("[MAIL - ERROR] Mail cannot be sent - no sender provided");
			break;
		default:
			break;
		}
		return true;
	}

	@Override
	public void updateOrder(Order order) {
		dao.updateOrder(order);
	}

	@Override
	public List<Request> getNotifications(long clientId) {
		return dao.getNotifications(clientId);
	}

	@Override
	public boolean deleteNotification(long notificationId) {
		return ownerDao.deleteNotification(notificationId);
	}

	@Override
	public boolean cancelOrder(long orderId) {
		return dao.cancelOrder(orderId);
	}

	@Override
	public Workshop fetchWorkshop(long workshopId) {
		return ownerDao.fetchWorkshop(workshopId);
	}

	@Override
	public VehicleControlPoint fetchControlPoint(long controlPointId) {
		return ownerDao.fetchControlPoint(controlPointId);
	}
}
