package workshop.home.controller;

import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import workshop.home.model.LoginModel;
import workshop.home.model.RegistrationModel;
import workshop.home.service.HomePageService;
import workshop.home.validator.RegisterFormValidator;
import workshop.model.CarAccessory;
import workshop.model.CarService;
import workshop.model.Category;
import workshop.model.Person;
import workshop.model.PersonTypeEnum;
import workshop.model.VehicleControlPoint;
import workshop.model.Workshop;

import javax.validation.Valid;

@Controller
@SessionAttributes("sessionUser")
public class HomePageController {

	@Autowired
	@Qualifier("homePageServiceImpl")
	private HomePageService service;

	private List<CarAccessory> discountAccessories;
	private List<CarAccessory> bestsellers;

	@Autowired
	private RegisterFormValidator validator;

	@InitBinder("registerForm")
	private void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@PostConstruct
	public void init() {
		discountAccessories = service.getDiscountAccessories();
		bestsellers = service.getBestsellers();
	}

	@GetMapping("/")
	public ModelAndView getIndexView(ModelMap model, HttpServletRequest request) {
		setDiscounts(request);
		setBestsellers(request);
		return new ModelAndView("index", "categories", Arrays.asList(Category.values()));
	}

	private void setDiscounts(HttpServletRequest request) {
		request.setAttribute("discounts", discountAccessories);
	}

	private void setBestsellers(HttpServletRequest request) {
		request.setAttribute("bestsellers", bestsellers);
	}

	@GetMapping("/getLoginView")
	public ModelAndView getLoginView(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView("login");
		mv.addObject("loginForm", new LoginModel());

		String loginError = request.getParameter("loginError");

		if (loginError != null) {
			mv.addObject("loginError", loginError);
		}

		return mv;
	}

	@GetMapping("/about")
	public ModelAndView getAbout() {
		return new ModelAndView("about");
	}

	@GetMapping("/getRegistrationView")
	public ModelAndView getRegistrationView(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView("register");
		mv.addObject("registerForm", new RegistrationModel());

		String registerSuccess = request.getParameter("registerSuccess");
		String registerError = request.getParameter("registerError");

		if (registerSuccess != null) {
			mv.addObject("registerSuccess", registerSuccess);
		}
		if (registerError != null) {
			mv.addObject("registerError", registerError);
		}

		return mv;
	}

	@PostMapping("/submitLogin")
	public ModelAndView logIn(@ModelAttribute("loginForm") LoginModel loginModel, ModelMap model,
			HttpServletResponse response, HttpServletRequest request) {

		Person user = service.getUserByCredentials(loginModel.getUserName(), loginModel.getPassword());
		if (user == null) {
			model.addAttribute("loginError", "login.error.user");
			return new ModelAndView("redirect:/getLoginView", model);
		}

		model.addAttribute("sessionUser", user);
		switch (user.getType()) {
		default:
		case GUEST: {
			return new ModelAndView("redirect:/");
		}
		case CLIENT: {
			return new ModelAndView("redirect:/clientPage");
		}
		case OWNER: {
			return new ModelAndView("redirect:/ownerPage");
		}
		case ADMIN: {
			return new ModelAndView("redirect:/adminPage");
		}
		}
	}

	@PostMapping("/submitRegister")
	public ModelAndView register(@ModelAttribute("registerForm") @Valid RegistrationModel registerModel,
			BindingResult result, ModelMap model) {
		if (result.hasErrors()) {
			model.addAttribute("registerError", "register.error");
			return new ModelAndView("register");
		}
		if (registerModel.getUserType().equals(PersonTypeEnum.GUEST)) {
			result.rejectValue("userType", "error", "You must choose the user type");
			model.addAttribute("registerError", "register.error");
			return new ModelAndView("register");
		}

		Person newUser = new Person();
		newUser.setFirstName(registerModel.getFirstName());
		newUser.setSurname(registerModel.getSurname());
		newUser.setUserName(registerModel.getUserName());
		newUser.setEmailAddress(registerModel.getEmailAddress());
		newUser.setPhoneNumber(registerModel.getPhoneNumber());
		newUser.setType(registerModel.getUserType());
		newUser.setPassword(registerModel.getPassword());

		if (service.addUser(newUser) != null)
			model.addAttribute("registerSuccess", "register.success");
		else
			model.addAttribute("registerError", "register.error.user");
		return new ModelAndView("redirect:/getRegistrationView", model);
	}

	@GetMapping("/logOut")
	public ModelAndView logOut(SessionStatus status, ModelMap model) {
		model.addAttribute("sessionUser", null);
		status.setComplete();
		return new ModelAndView("redirect:/", model);
	}

	@PostMapping("/getWorkshopsByName")
	public ModelAndView filterWorkshopsByName(HttpServletRequest request, ModelMap model) {
		String name = request.getParameter("workshopName");
		model.addAttribute("showWorkshops", true);
		List<Workshop> workshops = service.getWorkshops(name);
		model.addAttribute("workshops", workshops);
		if (workshops.isEmpty()) {
			model.addAttribute("getWorkshopsResponse", "home.workshop.none");
			return getIndexView(model, request);
		}
		return getIndexView(model, request);
	}

	@PostMapping("/getVehicleControlPoint")
	public ModelAndView filterVehicleControlPointsByName(HttpServletRequest request, ModelMap model) {
		String name = request.getParameter("controlPointName");
		model.addAttribute("showControlPointsResponse", true);
		List<VehicleControlPoint> controlPoints = service.getControlPoints(name);
		model.addAttribute("controlPoints", controlPoints);
		if (controlPoints.isEmpty()) {
			model.addAttribute("getControlPointsResponse", "home.control_point.none");
			return getIndexView(model, request);
		}
		return getIndexView(model, request);
	}

	@PostMapping("/getAccessories")
	public ModelAndView getAccessories(HttpServletRequest request, ModelMap model) {
		String name = request.getParameter("accessoryName");
		String category = request.getParameter("accessoryCategory");
		model.addAttribute("showAccessories", true);
		List<CarAccessory> accessories = service.getAccessories(name, "price", category);
		model.addAttribute("carAccessories", accessories);
		if (accessories.isEmpty()) {
			model.addAttribute("getAccessoriesResponse", "home.accessory.none");
			return getIndexView(model, request);
		}
		return getIndexView(model, request);
	}

	@PostMapping("/getServices")
	public ModelAndView getServices(HttpServletRequest request, ModelMap model) {
		String name = request.getParameter("serviceName");
		String category = request.getParameter("serviceCategory");
		model.addAttribute("showServices", true);
		List<CarService> services = service.getServices(name, "price", category);
		model.addAttribute("carServices", services);
		if (services.isEmpty()) {
			model.addAttribute("getServicesResponse", "home.service.none");
			return getIndexView(model, request);
		}
		return getIndexView(model, request);
	}

	@GetMapping("/accessoryView/{accessoryId}")
	public ModelAndView getAccessoryView(HttpServletRequest request, ModelMap model,
			@PathVariable("accessoryId") long accessoryId) {
		CarAccessory accessory = service.getAccessoryById(accessoryId);
		ModelAndView mv = new ModelAndView("accessory-view");
		model.addAttribute("accessory", accessory);

		String addAccessorySuccess = request.getParameter("addAccessorySuccess");
		String addAccessoryError = request.getParameter("addAccessoryError");

		if (addAccessorySuccess != null) {
			model.addAttribute("addAccessorySuccess", addAccessorySuccess);
		}
		if (addAccessoryError != null) {
			model.addAttribute("addAccessoryError", addAccessoryError);
		}
		mv.addObject(model);
		return mv;
	}

	@GetMapping("/serviceView/{serviceId}")
	public ModelAndView getServiceView(HttpServletRequest request, ModelMap model,
			@PathVariable("serviceId") long serviceId) {
		CarService carService = service.getServiceById(serviceId);
		ModelAndView mv = new ModelAndView("service-view");
		model.addAttribute("service", carService);

		String addServiceSuccess = request.getParameter("addServiceSuccess");
		String addServiceError = request.getParameter("addServiceError");

		if (addServiceSuccess != null) {
			model.addAttribute("addServiceSuccess", addServiceSuccess);
		}
		if (addServiceError != null) {
			model.addAttribute("addServiceError", addServiceError);
		}
		mv.addObject(model);
		return mv;
	}

	@GetMapping("/workshopView/{workshopId}")
	public ModelAndView getWorkshopView(HttpServletRequest request, @PathVariable("workshopId") long workshopId) {
		Workshop workshop = service.getWorkshopById(workshopId);
		ModelAndView mv = new ModelAndView("workshop-view", "workshopModel", workshop);
		mv.addObject("accessories", workshop.getAccessories());
		mv.addObject("services", workshop.getServices());
		mv.addObject("nrOfStations", workshop.getStations().size());

		String addServiceSuccess = request.getParameter("addServiceSuccess");
		String addServiceError = request.getParameter("addServiceError");
		String addAccessorySuccess = request.getParameter("addAccessorySuccess");
		String addAccessoryError = request.getParameter("addAccessoryError");

		if (workshop.getAccessories().isEmpty()) {
			mv.addObject("getEmptyAccessoriesResponse", "home.accessory.none");
		}
		if (workshop.getServices().isEmpty()) {
			mv.addObject("getEmptyServicesResponse", "home.service.none");
		}
		if (addServiceSuccess != null) {
			mv.addObject("addServiceSuccess", addServiceSuccess);
		}
		if (addServiceError != null) {
			mv.addObject("addServiceError", addServiceError);
		}
		if (addAccessorySuccess != null) {
			mv.addObject("addAccessorySuccess", addAccessorySuccess);
		}
		if (addAccessoryError != null) {
			mv.addObject("addAccessoryError", addAccessoryError);
		}

		return mv;
	}

	@GetMapping("/controlPointView/{controlPointId}")
	public ModelAndView getControlPointView(@PathVariable("controlPointId") long controlPointId) {
		VehicleControlPoint controlPoint = service.getControlPointById(controlPointId);
		ModelAndView mv = new ModelAndView("control-point-view", "controlPoint", controlPoint);
		mv.addObject("nrOfStations", controlPoint.getStations().size());
		return mv;
	}
}
