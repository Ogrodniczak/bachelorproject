package workshop.home.dao;

import java.util.List;

import workshop.model.CarAccessory;
import workshop.model.CarService;
import workshop.model.Person;
import workshop.model.PersonTypeEnum;
import workshop.model.VehicleControlPoint;
import workshop.model.Workshop;

public interface HomePageDao {
	public Person persistPerson(Person person);

	public Person findPerson(Person person, PersonTypeEnum type);

	public Person findPersonById(long id);

	public Person findPersonByCredentials(String userName, String password);

	public List<Workshop> getWorkshops(String name);

	public List<VehicleControlPoint> getControlPoints(String name);

	public <T> List<T> getItemsByName(String name, Class<T> persistentClass);

	public <T> T getItemById(long id, Class<T> persistentClass);

	public List<CarAccessory> getAccessories(boolean filterDiscounts, boolean filterBestsellers, String name,
			String sortBy, String category);

	public List<CarService> getServices(String name, String sortBy, String category);
}
