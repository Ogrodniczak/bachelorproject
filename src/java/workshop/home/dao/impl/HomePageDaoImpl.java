package workshop.home.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import workshop.home.dao.HomePageDao;
import workshop.model.CarAccessory;
import workshop.model.CarService;
import workshop.model.Category;
import workshop.model.Person;
import workshop.model.PersonTypeEnum;
import workshop.model.VehicleControlPoint;
import workshop.model.Workshop;

@Repository
@Transactional
public class HomePageDaoImpl implements HomePageDao {

	private static final int RESULT_SIZE = 5;

	private static final EntityManagerFactory entityManagerFactory = Persistence
			.createEntityManagerFactory("persistenceUnit");

	private EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}

	@PreDestroy
	public void finish() {
		entityManagerFactory.close();
	}

	@Override
	public Person persistPerson(Person person) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		
		try {
			tx.begin();
			if (findPerson(person, person.getType()) == null) {
				em.persist(person);
				tx.commit();
			} else {
				person = null;
			}
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return null;
		} finally {
			em.close();
		}
		return person;
	}

	@Override
	public Person findPersonById(long id) {
		EntityManager em = getEntityManager();
		Person foundPerson = null;
		try {
			foundPerson = em.find(Person.class, id);
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return foundPerson;
	}

	@Override
	public Person findPersonByCredentials(String userName, String password) {
		EntityManager em = getEntityManager();
		Person foundPerson = null;
		try {
			foundPerson = em
					.createQuery("SELECT p FROM Person p WHERE p.userName = :userName AND p.password = :password",
							Person.class)
					.setParameter("userName", userName).setParameter("password", password).getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return foundPerson;
	}

	@Override
	public Person findPerson(Person person, PersonTypeEnum type) {
		EntityManager em = getEntityManager();
		Person foundPerson = null;
		try {
			foundPerson = em.createQuery(
					"SELECT p FROM Person p WHERE (p.userName = :userName OR p.id = :id OR p.password = :password OR p.emailAddress = :emailAddress) AND p.type = :type",
					Person.class).setParameter("userName", person.getUserName()).setParameter("id", person.getId())
					.setParameter("password", person.getPassword())
					.setParameter("emailAddress", person.getEmailAddress()).setParameter("type", type)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return foundPerson;
	}

	@Override
	public List<Workshop> getWorkshops(String name) {
		EntityManager em = getEntityManager();
		List<Workshop> foundWorkshops = new ArrayList<>();
		try {
			if (name != null && !name.equals("")) {
				foundWorkshops = em
						.createQuery("SELECT w FROM Workshop w WHERE w.name LIKE :name", Workshop.class)
						.setParameter("name", name + "%").getResultList();
			} else {
				foundWorkshops = em.createQuery("SELECT w FROM Workshop w", Workshop.class).getResultList();
			}
			for (Workshop w : foundWorkshops) {
				w.getStations().size();
			}
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return foundWorkshops;
	}

	@Override
	public List<VehicleControlPoint> getControlPoints(String name) {
		EntityManager em = getEntityManager();
		List<VehicleControlPoint> foundControlPoints = new ArrayList<>();
		try {
			if (name != null && !name.equals("")) {
				foundControlPoints = em
						.createQuery("SELECT cp FROM VehicleControlPoint cp WHERE cp.name LIKE :name",
								VehicleControlPoint.class)
						.setParameter("name", name + "%").getResultList();
			} else {
				foundControlPoints = em
						.createQuery("SELECT cp FROM VehicleControlPoint cp", VehicleControlPoint.class)
						.getResultList();
			}
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return foundControlPoints;
	}

	@Override
	public <T> T getItemById(long id, Class<T> persistentClass) {
		EntityManager em = getEntityManager();
		T foundItem = null;

		try {
			foundItem = em
					.createQuery("SELECT item FROM " + persistentClass.getSimpleName() + " item WHERE item.id = :id",
							persistentClass)
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return foundItem;
	}

	@Override
	public <T> List<T> getItemsByName(String name, Class<T> persistentClass) {
		EntityManager em = getEntityManager();
		List<T> foundItems = new ArrayList<>();

		try {
			if (name != null && !name.equals("")) {
				foundItems = em
						.createQuery("SELECT item FROM " + persistentClass.getSimpleName()
								+ " item WHERE item.name LIKE :name", persistentClass)
						.setParameter("name", name + "%").getResultList();
			} else {
				foundItems = em
						.createQuery("SELECT item FROM " + persistentClass.getSimpleName() + " item", persistentClass)
						.getResultList();

			}
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return foundItems;
	}

	@Override
	public List<CarAccessory> getAccessories(boolean filterDiscounts, boolean filterBestsellers, String name,
			String sortBy, String category) {
		EntityManager em = getEntityManager();
		List<CarAccessory> foundAccessories = new ArrayList<>();
		try {
			if (filterDiscounts) {
				foundAccessories = em
						.createQuery("SELECT acc FROM CarAccessory acc WHERE acc.discount > 0 ORDER BY acc.discount",
								CarAccessory.class)
						.setMaxResults(RESULT_SIZE).getResultList();
			} else if (filterBestsellers) {
				foundAccessories = em
						.createQuery("SELECT acc FROM CarAccessory acc WHERE acc.amountSold > 0 ORDER BY amount_sold",
								CarAccessory.class)
						.setMaxResults(RESULT_SIZE).getResultList();
			} else {
				if (category.equals("ALL")) {
					foundAccessories = em
							.createQuery("SELECT acc FROM CarAccessory acc WHERE acc.name LIKE :name ORDER BY :sortBy",
									CarAccessory.class)
							.setParameter("name", name + "%").setParameter("sortBy", sortBy).getResultList();
				} else {
					foundAccessories = em.createQuery(
							"SELECT acc FROM CarAccessory acc WHERE acc.name LIKE :name AND acc.category = :category ORDER BY :sortBy",
							CarAccessory.class).setParameter("name", name + "%")
							.setParameter("category", Category.valueOf(Category.class, category))
							.setParameter("sortBy", sortBy).getResultList();
				}
			}
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return foundAccessories;
	}

	@Override
	public List<CarService> getServices(String name, String sortBy, String category) {
		EntityManager em = getEntityManager();
		List<CarService> foundServices = new ArrayList<>();
		try {
			if (category.equals("ALL")) {
				foundServices = em
						.createQuery("SELECT s FROM CarService s WHERE s.name LIKE :name ORDER BY :sortBy",
								CarService.class)
						.setParameter("name", name + "%").setParameter("sortBy", sortBy).getResultList();
			} else {
				foundServices = em.createQuery(
						"SELECT s FROM CarService s WHERE s.name LIKE :name AND s.category = :category ORDER BY :sortBy",
						CarService.class).setParameter("name", name + "%")
						.setParameter("category", Category.valueOf(Category.class, category))
						.setParameter("sortBy", sortBy).getResultList();
			}
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return foundServices;
	}
}
