package workshop.home.model;

import workshop.model.PersonTypeEnum;
import lombok.Data;

@Data
public class RegistrationModel {
	private long id;
	private String firstName;
	private String surname;
	private String userName;
	private PersonTypeEnum userType;
	private String emailAddress;
	private String phoneNumber;
	private String password;
	private String confirmPassword;
}
