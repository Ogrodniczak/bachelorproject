package workshop.home.service;

import java.util.List;

import workshop.home.dao.HomePageDao;
import workshop.model.CarAccessory;
import workshop.model.CarService;
import workshop.model.Person;
import workshop.model.PersonTypeEnum;
import workshop.model.VehicleControlPoint;
import workshop.model.Workshop;

public interface HomePageService {

	public Person addUser(Person person);

	public Person getUser(Person person, PersonTypeEnum type);

	public Person getUserByCredentials(String userName, String password);

	public List<Workshop> getWorkshops(String name);

	public List<VehicleControlPoint> getControlPoints(String name);

	public void setDao(HomePageDao dao);

	public CarAccessory getAccessoryById(long id);

	public List<CarAccessory> getAccessories(String name, String sortBy, String category);

	public List<CarAccessory> getDiscountAccessories();

	public List<CarAccessory> getBestsellers();

	public List<CarService> getServices(String name, String sortBy, String category);

	public CarService getServiceById(long serviceId);

	public Workshop getWorkshopById(long workshopId);

	public VehicleControlPoint getControlPointById(long controlPointId);
}
