package workshop.home.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import workshop.admin.dao.AdminDao;
import workshop.home.dao.HomePageDao;
import workshop.home.service.HomePageService;
import workshop.model.CarAccessory;
import workshop.model.CarService;
import workshop.model.Person;
import workshop.model.PersonTypeEnum;
import workshop.model.VehicleControlPoint;
import workshop.model.Workshop;

@Service
public class HomePageServiceImpl implements HomePageService {

	@Autowired
	@Qualifier("homePageDaoImpl")
	private HomePageDao dao;

	@Autowired
	@Qualifier("adminDaoImpl")
	private AdminDao adminDao;

	public void setDao(HomePageDao dao) {
		this.dao = dao;
	}

	@Override
	public Person addUser(Person person) {
		return dao.persistPerson(person);
	}

	@Override
	public Person getUser(Person person, PersonTypeEnum type) {
		return dao.findPerson(person, type);
	}

	@Override
	public Person getUserByCredentials(String userName, String password) {
		return dao.findPersonByCredentials(userName, password);
	}

	@Override
	public List<Workshop> getWorkshops(String name) {
		return dao.getWorkshops(name);
	}

	@Override
	public List<VehicleControlPoint> getControlPoints(String name) {
		return dao.getControlPoints(name);
	}

	@Override
	public List<CarAccessory> getAccessories(String name, String sortBy, String category) {
		return dao.getAccessories(false, false, name, sortBy, category);
	}

	@Override
	public List<CarAccessory> getDiscountAccessories() {
		return dao.getAccessories(true, false, null, null, null);
	}

	@Override
	public List<CarAccessory> getBestsellers() {
		return dao.getAccessories(false, true, null, null, null);
	}

	@Override
	public CarAccessory getAccessoryById(long id) {
		return dao.getItemById(id, CarAccessory.class);
	}

	@Override
	public List<CarService> getServices(String name, String sortBy, String category) {
		return dao.getServices(name, sortBy, category);
	}

	@Override
	public CarService getServiceById(long serviceId) {
		return dao.getItemById(serviceId, CarService.class);
	}

	@Override
	public Workshop getWorkshopById(long workshopId) {
		return adminDao.findWorkshopById(workshopId);
	}

	@Override
	public VehicleControlPoint getControlPointById(long controlPointId) {
		return adminDao.findControlPointById(controlPointId);
	}
}
