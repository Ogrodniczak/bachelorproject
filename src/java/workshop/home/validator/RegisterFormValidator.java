package workshop.home.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import workshop.home.model.RegistrationModel;
import workshop.model.PersonTypeEnum;

public class RegisterFormValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return RegistrationModel.class.equals(clazz);
	}

	@Override
	public void validate(Object object, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "validation.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "surname", "validation.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userName", "validation.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userType", "validation.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "emailAddress", "validation.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phoneNumber", "validation.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "validation.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmPassword", "validation.empty");

		RegistrationModel model = (RegistrationModel) object;

		if (model.getFirstName().length() > 20) {
			errors.rejectValue("firstName", "validation.max_20_chars");
		}
		if (model.getSurname().length() > 20) {
			errors.rejectValue("surname", "validation.max_20_chars");
		}
		if (model.getUserName().length() > 15) {
			errors.rejectValue("userName", "validation.max_15_chars");
		}
		if (model.getUserType() == null || model.getUserType().equals(PersonTypeEnum.GUEST)) {
			errors.rejectValue("userType", "validation.no_user_type");
		}
		if (model.getEmailAddress().length() > 25) {
			errors.rejectValue("emailAddress", "validation.max_25_chars");
		}
		if (model.getPhoneNumber().length() > 15) {
			errors.rejectValue("phoneNumber", "validation.max_15_chars");
		}
		if (model.getPassword().length() > 15) {
			errors.rejectValue("password", "validation.max_15_chars");
		}
		if (model.getConfirmPassword().length() > 15) {
			errors.rejectValue("confirmPassword", "validation.max_15_chars");
		}
		if (!model.getPassword().equals(model.getConfirmPassword())) {
			errors.rejectValue("password", "validation.password.no_match");
		}
	}

}
