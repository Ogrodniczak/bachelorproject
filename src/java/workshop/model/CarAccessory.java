package workshop.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@DynamicUpdate
@Table(name = "accessory")
public class CarAccessory {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String name;
	
	private String description;
	
	private boolean available;
	
	private Double discount;
	
	private Double cost;
	
	private Integer amount;

	@Column(name = "amount_sold")
	private int amountSold;

	@Column(name = "image")
	private byte[] image;

	@ManyToOne
	@JoinColumn(name = "fk_workshop")
	private Workshop workshop;

	@Enumerated
	private Category category;
}
