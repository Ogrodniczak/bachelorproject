package workshop.model;

public enum Category {
	ALL, AIRBAGS, ALARMS, ALTERNATORS, BATTERIES, BONNETS, BRAKES, BELTS, BUMPERS, CABLES, CONTROLLERS, COOLING, DOORS, ELECTRONICS, ENGINES, FASTENERS, FLOOR, FUEL, FUSES, LIGNTING, METERS, MIRRORS, MISCELLANEOUS, OILS, PAINT, PILLARS, SEATS, SENSORS, STARTERS, STEERING, SUN_VISOR, SWITCHES, QUARTER_PANEL, WASHER, WHEELS, WINDOWS
}
