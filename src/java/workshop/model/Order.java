package workshop.model;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "orders")
public class Order {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ElementCollection
	private Map<CarAccessory, Integer> accessories = new HashMap<>();

	@ElementCollection
	private Map<CarService, Integer> services = new HashMap<>();

	private double cost;

	@Enumerated(EnumType.STRING)
	private OrderStatusEnum status;

	@OneToOne(cascade = CascadeType.ALL)
	private Visit visit;

	@ManyToOne
	@JoinColumn(name = "fk_client")
	private Person client;

	@ManyToOne
	@JoinColumn(name = "fk_workshop")
	private Workshop workshop;

	private double time;
}
