package workshop.model;

public enum OrderStatusEnum {
	NEW, SCHEDULED, TERMINATED
}
