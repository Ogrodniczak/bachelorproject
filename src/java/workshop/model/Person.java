package workshop.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "person")
public class Person {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "first_name")
	private String firstName;
	
	private String surname;
	
	@Column(name = "user_name")
	private String userName;
	
	@Column(name = "email_address")
	private String emailAddress;
	
	@Column(name = "phone_number")
	private String phoneNumber;
	
	@Enumerated(EnumType.STRING)
	private PersonTypeEnum type;
	
	private String password;
	
	@OneToMany(mappedBy="owner")
	@Column(name = "workshops")
	private Set<Workshop> workshopsOwned = new HashSet<>();
	
	@OneToMany(mappedBy="owner")
	@Column(name = "control_points")
	private Set<VehicleControlPoint> controlPoint = new HashSet<>();

	@OneToOne
	private Request requestFrom;	
}
