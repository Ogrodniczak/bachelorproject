package workshop.model;

public enum PersonTypeEnum {
	GUEST, CLIENT, OWNER, ADMIN
}
