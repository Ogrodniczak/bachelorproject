package workshop.model;

import java.time.LocalTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "station")
public class Station {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private int number;

	@OneToMany(mappedBy = "station", orphanRemoval = true)
	private Set<Calendar> days = new HashSet<>();

	@ManyToOne
	@JoinColumn(name = "fk_workshop")
	private Workshop workshop;

	@ManyToOne
	@JoinColumn(name = "fk_control_point")
	private VehicleControlPoint vehicleControlPoint;

	@Transient
	private Map<Calendar, Map<LocalTime, Visit>> calendarVisitMap = new HashMap<>();
}
