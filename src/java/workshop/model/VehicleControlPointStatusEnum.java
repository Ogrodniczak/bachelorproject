package workshop.model;

public enum VehicleControlPointStatusEnum {
	ACTIVE, REMOVED
}
