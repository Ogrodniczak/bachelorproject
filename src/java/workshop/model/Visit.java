package workshop.model;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "visit")
public class Visit {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ElementCollection
	List<LocalTime> hours = new ArrayList<>();

	@ManyToOne
	@JoinColumn(name = "fk_calendar")
	Calendar dayOfVisit;

	@Enumerated(EnumType.STRING)
	VisitStatusEnum status;

	@OneToOne(cascade = CascadeType.ALL)
	Order order;

	@OneToOne
	Person client;
}
