package workshop.model;

public enum VisitStatusEnum {
	NEW, SCHEDULED, TERMINATED, UNAVAILABLE, OLD
}
