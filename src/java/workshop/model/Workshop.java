package workshop.model;

import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;

import org.springframework.format.annotation.DateTimeFormat;

import workshop.LocalTimeConverter;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Workshop {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String name;

	private String description;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	private Address address;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "workshop", orphanRemoval = true)
	private Set<CarAccessory> accessories = new HashSet<>();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "workshop", orphanRemoval = true)
	private Set<CarService> services = new HashSet<>();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "workshop", orphanRemoval = true)
	@OrderBy("number")
	private Set<Station> stations = new HashSet<>();

	@ManyToOne
	@JoinColumn(name = "fk_owner")
	private Person owner;

	@Enumerated(EnumType.STRING)
	private WorkshopStatusEnum status;

	@Column(name = "opening_hour")
	@Convert(converter = LocalTimeConverter.class)
	@DateTimeFormat(pattern = "HH:mm")
	private LocalTime openingHour;

	@Column(name = "closing_hour")
	@Convert(converter = LocalTimeConverter.class)
	@DateTimeFormat(pattern = "HH:mm")
	private LocalTime closingHour;
}
