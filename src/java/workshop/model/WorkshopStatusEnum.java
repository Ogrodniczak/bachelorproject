package workshop.model;

public enum WorkshopStatusEnum {
	ACTIVE, REMOVED
}
