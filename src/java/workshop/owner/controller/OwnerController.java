package workshop.owner.controller;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import workshop.model.Address;
import workshop.model.Calendar;
import workshop.model.CarAccessory;
import workshop.model.CarService;
import workshop.model.Category;
import workshop.model.Person;
import workshop.model.PersonTypeEnum;
import workshop.model.Request;
import workshop.model.Station;
import workshop.model.VehicleControlPoint;
import workshop.model.Visit;
import workshop.model.VisitStatusEnum;
import workshop.model.Workshop;
import workshop.owner.service.OwnerServiceInterface;
import workshop.owner.validator.AccessoryValidator;
import workshop.owner.validator.ServiceValidator;

@Controller
public class OwnerController {

	private byte[] uploadedImage;

	@Autowired
	@Qualifier("ownerServiceImpl")
	private OwnerServiceInterface service;

	@Autowired
	private AccessoryValidator accesssoryValidator;

	@Autowired
	private ServiceValidator serviceValidator;

	@InitBinder("accessoryModel")
	private void initAccessoryBinder(WebDataBinder binder) {
		binder.setValidator(accesssoryValidator);
	}

	@InitBinder("serviceModel")
	private void initServiceBinder(WebDataBinder binder) {
		binder.setValidator(serviceValidator);
	}

	@RequestMapping("/ownerPage")
	public ModelAndView ownerPage(HttpServletRequest request, HttpSession session, ModelMap model) {
		if (!authenticateOwner(session))
			return new ModelAndView("redirect:/");

		Person owner = (Person) session.getAttribute("sessionUser");
		session.setAttribute("sessionWorkshopId", null);
		session.setAttribute("sessionAccessoryId", null);
		uploadedImage = null;
		session.setAttribute("uploadedImage", null);
		ModelAndView mv = new ModelAndView("owner");

		String delWorkshopSuccess = request.getParameter("deleteWorkshopSuccess");
		String delWorkshopError = request.getParameter("deleteWorkshopError");
		String delControlPointSuccess = request.getParameter("deleteControlPointSuccess");
		String delControlPointError = request.getParameter("deleteControlPointError");
		String delNotificationSuccess = request.getParameter("deleteNotificationSuccess");
		String delNotificationError = request.getParameter("deleteNotificationError");
		String requestResponseSuccess = request.getParameter("requestResponseSuccess");
		String requestResponseError = request.getParameter("requestResponseError");

		if (delWorkshopSuccess != null) {
			mv.addObject("deleteWorkshopSuccess", delWorkshopSuccess);
		}
		if (delWorkshopError != null) {
			mv.addObject("deleteWorkshopError", delWorkshopError);
		}
		if (delControlPointSuccess != null) {
			mv.addObject("deleteControlPointSuccess", delControlPointSuccess);
		}
		if (delControlPointError != null) {
			mv.addObject("deleteControlPointError", delControlPointError);
		}
		if (delNotificationSuccess != null) {
			mv.addObject("deleteNotificationSuccess", delNotificationSuccess);
		}
		if (delNotificationError != null) {
			mv.addObject("deleteNotificationError", delNotificationError);
		}
		if (requestResponseSuccess != null) {
			mv.addObject("requestResponseSuccess", requestResponseSuccess);
		}
		if (requestResponseError != null) {
			mv.addObject("requestResponseError", requestResponseError);
		}

		model.addAttribute("requestModel", new Request());
		List<Request> notifications = service.getNotifications(owner.getId());
		if (notifications.isEmpty()) {
			model.addAttribute("getNotificationsResponse", "owner.notification.none");
		}
		model.addAttribute("notifications", notifications);
		mv.addObject(model);
		return mv;
	}

	@RequestMapping("/ownerRemoveNotification/{notificationId}")
	public ModelAndView removeNotification(@PathVariable("notificationId") long notificationId, ModelMap model) {
		if (!service.deleteNotification(notificationId)) {
			model.addAttribute("deleteNotificationError", "owner.notification.delete.error");
		}
		model.addAttribute("deleteNotificationSuccess", "owner.notification.delete.success");
		return new ModelAndView("redirect:/ownerPage", model);
	}

	@PostMapping("/ownerAddRequest")
	public ModelAndView addRequest(@ModelAttribute("requestModel") Request requestModel, ModelMap model,
			HttpServletRequest request, HttpSession session) {
		Person user = (Person) session.getAttribute("sessionUser");
		if (!service.sendRequest(requestModel, user)) {
			model.addAttribute("requestResponseError", "owner.request.error");
		}
		model.addAttribute("requestResponseSuccess", "owner.request.success");
		return new ModelAndView("redirect:/ownerPage", model);
	}

	@PostMapping("/getOwnerWorkshops/{ownerId}")
	public ModelAndView getOwnerWorkshops(HttpServletRequest request, ModelMap model,
			@PathVariable("ownerId") long ownerId, HttpSession session, RedirectAttributes redirect) {
		model.addAttribute("showWorkshops", true);
		List<Workshop> workshops = service.getWorkshops(ownerId);
		if (workshops.isEmpty()) {
			model.addAttribute("getWorkshopsResponse", "home.workshop.none");
			return ownerPage(request, session, model);
		}
		model.addAttribute("workshops", workshops);
		return ownerPage(request, session, model);
	}

	@RequestMapping("ownerDeleteService/{workshopId}/{serviceId}")
	public ModelAndView deleteService(@PathVariable("workshopId") long workshopId,
			@PathVariable("serviceId") long serviceId, ModelMap model) {
		CarService deletedService = service.deleteService(serviceId);

		if (deletedService == null) {
			model.addAttribute("deleteServiceError", "owner.service.delete.error");
			return new ModelAndView("redirect:/ownerEditWorkshop/" + workshopId);
		}

		model.addAttribute("deleteServiceSuccess", "owner.service.delete.success");
		return new ModelAndView("redirect:/ownerEditWorkshop/" + workshopId);
	}

	@RequestMapping("ownerDeleteAccessory/{workshopId}/{accessoryId}")
	public ModelAndView deleteAccessory(@PathVariable("workshopId") long workshopId,
			@PathVariable("accessoryId") long accessoryId, ModelMap model) {
		CarAccessory deletedAccessory = service.deleteAccessory(accessoryId);

		if (deletedAccessory == null) {
			model.addAttribute("deleteAccessoryError", "owner.accessory.delete.error");
			return new ModelAndView("redirect:/ownerEditWorkshop/" + workshopId);
		}
		model.addAttribute("deleteAccessorySuccess", "owner.accessory.delete.success");
		return new ModelAndView("redirect:/ownerEditWorkshop/" + workshopId);
	}

	@GetMapping("/ownerGetCalendarView/{item}/{itemId}/{index}")
	public ModelAndView getCalendarView(HttpServletRequest request, @PathVariable("item") String item,
			@PathVariable("itemId") long itemId, @PathVariable("index") int index, HttpSession session) {
		Map<Calendar, Map<LocalTime, Visit>> calendarMap = new HashMap<>();
		ModelAndView mv = new ModelAndView("owner-calendar-view");
		if (item.equalsIgnoreCase("workshop")) {
			Workshop workshop = service.fetchWorkshop(itemId);
			for (Station station : workshop.getStations()) {
				calendarMap = service.getStationCalendars(station.getId(), index);
				if (!calendarMap.isEmpty()) {
					station.setCalendarVisitMap(calendarMap);
				}
			}
			mv.addObject("stations", workshop.getStations());
			mv.addObject("workshopId", itemId);
		} else if (item.equalsIgnoreCase("controlPoint")) {
			VehicleControlPoint controlPoint = service.fetchControlPoint(itemId);
			for (Station station : controlPoint.getStations()) {
				calendarMap = service.getStationCalendars(station.getId(), index);
				if (!calendarMap.isEmpty()) {
					station.setCalendarVisitMap(calendarMap);
				}
			}
			mv.addObject("stations", controlPoint.getStations());
			mv.addObject("controlPointId", itemId);
		} else {
			return ownerPage(request, session, null);
		}
		List<LocalTime> allHours = new ArrayList<>();
		for (int hour = 6; hour < 19; hour++) {
			allHours.add(LocalTime.of(hour, 0));
		}
		mv.addObject("allHours", allHours);
		mv.addObject("item", item);
		mv.addObject("itemId", itemId);
		mv.addObject("currentIndex", index);
		return mv;
	}

	@GetMapping("/ownerAddStation/{item}/{itemId}")
	public ModelAndView addStation(@PathVariable("item") String item, @PathVariable("itemId") long itemId,
			ModelMap model, HttpSession session) {
		if (service.addStation(item, itemId) != null) {
			model.addAttribute("addStationSuccess", "owner.station.add.success");
		} else {
			model.addAttribute("addStationError", "owner.station.add.error");
		}
		if (item.equalsIgnoreCase("workshop")) {
			return new ModelAndView("redirect:/ownerEditWorkshop/" + itemId, model);
		} else if (item.equalsIgnoreCase("controlPoint")) {
			return new ModelAndView("redirect:/ownerEditControlPoint/" + itemId, model);
		} else
			return new ModelAndView("redirect:/ownerPage");
	}

	@RequestMapping("/ownerRemoveStation/{item}/{itemId}")
	public ModelAndView removeWorkshopStation(@PathVariable("item") String item, @PathVariable("itemId") long itemId,
			ModelMap model, HttpSession session) {
		if (service.removeStation(item, itemId) != null) {
			model.addAttribute("deleteStationSuccess", "owner.station.delete.success");
		} else {
			model.addAttribute("deleteStationError", "owner.station.delete.error");
		}
		if (item.equalsIgnoreCase("workshop")) {
			return new ModelAndView("redirect:/ownerEditWorkshop/" + itemId, model);
		} else if (item.equalsIgnoreCase("controlPoint")) {
			return new ModelAndView("redirect:/ownerEditControlPoint/" + itemId, model);
		} else
			return new ModelAndView("redirect:/ownerPage");
	}

	@RequestMapping("/disableVisitHour/{calendarId}/{localTimeId}/{currentIndex}")
	public ModelAndView disableVisitHour(@PathVariable("calendarId") long calendarId,
			@PathVariable("localTimeId") String timeString, @PathVariable("currentIndex") int index,
			HttpSession session) {
		Calendar day = service.getCalendarById(calendarId);
		try {
			LocalTime time = LocalTime.parse(timeString);
			Visit visit = new Visit();
			visit.setStatus(VisitStatusEnum.UNAVAILABLE);
			visit.setDayOfVisit(day);
			visit.setHours(Arrays.asList(time));
			service.updateCalendar(false, visit);
		} catch (DateTimeParseException e) {
			return new ModelAndView("redirect:/");
		}
		if (day.getStation().getWorkshop() != null) {
			return new ModelAndView(
					"redirect:/ownerGetCalendarView/workshop/" + day.getStation().getWorkshop().getId() + "/" + index);
		}
		return new ModelAndView("redirect:/ownerGetCalendarView/controlPoint/"
				+ day.getStation().getVehicleControlPoint().getId() + "/" + index);
	}

	@RequestMapping("/enableVisitHour/{calendarId}/{localTime}/{currentIndex}")
	public ModelAndView enableVisitHour(@PathVariable("calendarId") long calendarId,
			@PathVariable("localTime") String timeString, @PathVariable("currentIndex") int index,
			HttpSession session) {
		Calendar day = service.getCalendarById(calendarId);
		try {
			LocalTime time = LocalTime.parse(timeString);
			Set<Visit> visits = day.getVisits();
			Visit visitToRemove = null;
			for (Visit v : visits) {
				if (v.getHours().contains(time)) {
					visitToRemove = v;
					break;
				}
			}
			service.updateCalendar(true, visitToRemove);
		} catch (DateTimeParseException e) {
			return new ModelAndView("redirect:/");
		}
		if (day.getStation().getWorkshop() != null) {
			return new ModelAndView(
					"redirect:/ownerGetCalendarView/workshop/" + day.getStation().getWorkshop().getId() + "/" + index);
		}
		return new ModelAndView("redirect:/ownerGetCalendarView/controlPoint/"
				+ day.getStation().getVehicleControlPoint().getId() + "/" + index);
	}

	@RequestMapping("/ownerCancelVisit/{visitId}/{currentIndex}")
	public ModelAndView cancelVisit(HttpServletRequest request, @PathVariable("visitId") long visitId,
			@PathVariable("currentIndex") int index) {
		Visit visit = service.getVisitById(visitId);
		service.cancelVisit(request, visit);
		return new ModelAndView("redirect:/ownerGetCalendarView/workshop/"
				+ visit.getDayOfVisit().getStation().getWorkshop().getId() + "/" + index);
	}

	@RequestMapping("/generateStationCalendar/{stationId}")
	public ModelAndView generateStationCalendar(@PathVariable("stationId") long stationId, HttpSession session) {
		Station station = service.getStationById(stationId);
		service.generateStationCalendars(stationId);
		if (station.getWorkshop() != null) {
			return new ModelAndView("redirect:/ownerGetCalendarView/workshop/" + station.getWorkshop().getId() + "/-1");
		}
		return new ModelAndView(
				"redirect:/ownerGetCalendarView/controlPoint/" + station.getVehicleControlPoint().getId() + "/-1");
	}

	@RequestMapping("/ownerDeleteWorkshop/{workshopId}")
	public ModelAndView deleteWorkshop(@PathVariable("workshopId") long workshopId, ModelMap model,
			RedirectAttributes redirect) {

		if (!service.deleteWorkshop(workshopId)) {
			model.addAttribute("deleteWorkshopError", "owner.workshop.delete.error");
			return new ModelAndView("redirect:/ownerPage", model);
		}

		model.addAttribute("deleteWorkshopSuccess", "owner.workshop.delete.success");

		return new ModelAndView("redirect:/ownerPage", model);
	}

	@RequestMapping("/ownerDeleteControlPoint/{controlPointId}")
	public ModelAndView deleteControlPoint(@PathVariable("controlPointId") long controlPointId, ModelMap model,
			RedirectAttributes redirect) {

		if (!service.deleteControlPoint(controlPointId)) {
			model.addAttribute("deleteControlPointError", "owner.control_point.delete.error");
			return new ModelAndView("redirect:/ownerPage", model);
		}

		model.addAttribute("deleteControlPointSuccess", "owner.control_point.delete.success");

		return new ModelAndView("redirect:/ownerPage", model);
	}

	@RequestMapping("/ownerTerminateVisit/{item}/{itemId}/{visitId}/{currentIndex}")
	public ModelAndView terminateVisit(HttpSession session, @PathVariable("item") String item,
			@PathVariable("itemId") long itemId, @PathVariable("visitId") long visitId,
			@PathVariable("currentIndex") int currentIndex, ModelMap model) {
		Person user = (Person) session.getAttribute("sessionUser");
		if (!service.terminateVisit(user.getId(), visitId)) {
			model.addAttribute("terminateVisitError", "owner.visit.terminate.error");
		} else {
			model.addAttribute("terminateVisitSuccess", "owner.visit.terminate.success");
		}
		return new ModelAndView("redirect:/ownerGetCalendarView/" + item + "/" + itemId + "/" + currentIndex);
	}

	@PostMapping("/getOwnerControlPoints/{ownerId}")
	public ModelAndView getOwnerControlPoints(HttpServletRequest request, ModelMap model,
			@PathVariable("ownerId") long ownerId, HttpSession session) {
		model.addAttribute("showControlPoints", true);
		List<VehicleControlPoint> controlPoints = service.getControlPoints(ownerId);
		if (controlPoints.isEmpty()) {
			model.addAttribute("getControlPointsResponse", "home.control_point.none");
			return ownerPage(request, session, model);
		}
		model.addAttribute("controlPoints", controlPoints);
		return ownerPage(request, session, model);
	}

	@PostMapping("/saveService/{workshopId}")
	public ModelAndView saveService(@PathVariable("workshopId") long workshopId, ModelMap model,
			@ModelAttribute("serviceModel") @Valid CarService carService, BindingResult result) {

		if (result.hasErrors()) {
			model.addAttribute("saveServiceError", "owner.service.save.error");
			return new ModelAndView("owner-save-service");
		}

		if (carService.getWorkshop() == null) {
			Workshop workshop = service.getWorkshopById(workshopId);
			carService.setWorkshop(workshop);
		}
		model.addAttribute("serviceModel", carService);
		if (service.saveCarService(carService) == null) {
			model.addAttribute("saveServiceError", "owner.service.save.error");
			return new ModelAndView("redirect:/ownerGetSaveServiceView/" + workshopId + "/" + carService.getId(),
					model);
		}
		model.addAttribute("saveServiceSuccess", "owner.service.save.success");
		return new ModelAndView("redirect:/ownerGetSaveServiceView/" + workshopId + "/" + carService.getId(), model);
	}

	@RequestMapping("/uploadImage/{workshopId}/{accessoryId}")
	public ModelAndView uploadImage(@PathVariable("workshopId") long workshopId,
			@PathVariable(name = "accessoryId", required = false) long accessoryId, HttpServletRequest request) {
		byte[] image = (byte[]) request.getAttribute("uploadedImage");
		uploadedImage = image;
		return getSaveAccessoryView(request, workshopId, accessoryId);
	}

	@PostMapping("/saveAccessory/{workshopId}")
	public ModelAndView saveAccessory(HttpServletRequest request, @PathVariable("workshopId") long workshopId,
			ModelMap model, @ModelAttribute("accessoryModel") @Valid CarAccessory carAccessory, BindingResult result) {

		if (result.hasErrors()) {
			model.addAttribute("saveAccessoryError", "owner.accessory.save.error");
			return new ModelAndView("owner-save-accessory");
		}

		if (uploadedImage != null) {
			carAccessory.setImage(uploadedImage);
			uploadedImage = null;
		}
		if (carAccessory.getWorkshop() == null) {
			Workshop workshop = service.getWorkshopById(workshopId);
			carAccessory.setWorkshop(workshop);
		}
		if (service.saveCarAccessory(carAccessory) == null) {
			model.addAttribute("saveAccessoryError", "owner.accessory.save.error");
			return new ModelAndView("owner-save-accessory", "accessoryModel", carAccessory);
		}
		model.addAttribute("saveAccessorySuccess", "owner.accessory.save.success");
		return new ModelAndView("owner-save-accessory", "accessoryModel", carAccessory);
	}

	@GetMapping("/ownerGetSaveServiceView/{workshopId}/{serviceId}")
	public ModelAndView getSaveServiceView(HttpServletRequest request, @PathVariable("workshopId") long workshopId,
			@PathVariable("serviceId") Long serviceId) {
		ModelAndView mv = new ModelAndView("owner-save-service");
		CarService carService;
		if (serviceId == null || serviceId == 0) {
			carService = new CarService();
		} else {
			carService = service.getServiceById(serviceId);

		}

		String saveServiceSuccess = request.getParameter("saveServiceSuccess");
		String saveServiceError = request.getParameter("saveServiceError");

		if (saveServiceSuccess != null) {
			mv.addObject("saveServiceSuccess", saveServiceSuccess);
		}
		if (saveServiceError != null) {
			mv.addObject("saveServiceError", saveServiceError);
		}

		mv.addObject("serviceModel", carService);
		mv.addObject("workshopId", workshopId);
		mv.addObject("categories", Arrays.asList(Category.values()));
		return mv;
	}

	@GetMapping("/ownerGetSaveAccessoryView/{workshopId}/{accessoryId}")
	public ModelAndView getSaveAccessoryView(HttpServletRequest request, @PathVariable("workshopId") long workshopId,
			@PathVariable(name = "accessoryId", required = false) Long accessoryId) {

		HttpSession session = request.getSession();
		session.setAttribute("sessionWorkshopId", workshopId);
		session.setAttribute("sessionAccessoryId", accessoryId);
		ModelAndView mv = new ModelAndView("owner-save-accessory");
		CarAccessory carAccessory;

		if (accessoryId != null && accessoryId != 0) {
			carAccessory = service.getAccessoryById(accessoryId);
		} else {
			carAccessory = new CarAccessory();
		}
		if (uploadedImage != null) {
			session.setAttribute("uploadedImage", uploadedImage);
			mv.addObject("uploadedImage", uploadedImage);
		}

		String saveAccessorySuccess = request.getParameter("saveAccessorySuccess");
		String saveAccessoryError = request.getParameter("saveAccessoryError");

		if (saveAccessorySuccess != null) {
			mv.addObject("saveAccessorySuccess", saveAccessorySuccess);
		}
		if (saveAccessoryError != null) {
			mv.addObject("saveAccessoryError", saveAccessoryError);
		}

		mv.addObject("accessoryModel", carAccessory);
		mv.addObject("workshopId", workshopId);
		mv.addObject("categories", Arrays.asList(Category.values()));

		return mv;
	}

	@RequestMapping("/ownerEditControlPoint/{controlPointId}")
	public ModelAndView getEditControlPointView(HttpServletRequest request,
			@PathVariable("controlPointId") long controlPointId, ModelMap model, VehicleControlPoint controlPoint) {

		VehicleControlPoint editControlPoint = ((controlPoint == null || controlPoint.getId() == 0)
				? service.getControlPointById(controlPointId)
				: controlPoint);
		Address editAddressModel = editControlPoint.getAddress();

		String editControlPointSuccess = request.getParameter("editControlPointSuccess");
		String editControlPointError = request.getParameter("editControlPointError");
		String addStationSuccess = request.getParameter("addStationSuccess");
		String addStationError = request.getParameter("addStationError");

		if (editControlPointSuccess != null) {
			model.addAttribute("editControlPointSuccess", editControlPointSuccess);
		}
		if (editControlPointError != null) {
			model.addAttribute("editControlPointError", editControlPointError);
		}
		if (addStationSuccess != null) {
			model.addAttribute("addStationSuccess", addStationSuccess);
		}
		if (addStationError != null) {
			model.addAttribute("addStationError", addStationError);
		}

		model.addAttribute("controlPoint", editControlPoint);
		model.addAttribute("addressModel", editAddressModel);

		return new ModelAndView("owner-edit-control-point", "controlPoint", editControlPoint);
	}

	@RequestMapping("/ownerEditWorkshop/{workshopId}")
	public ModelAndView getEditWorkshopView(HttpServletRequest request, @PathVariable("workshopId") long workshopId,
			ModelMap model, Workshop workshop) {
		if (uploadedImage != null) {
			uploadedImage = null;
		}
		Workshop editWorkshopModel = ((workshop == null || workshop.getId() == 0) ? service.getWorkshopById(workshopId)
				: workshop);
		Address editAddressModel = editWorkshopModel.getAddress();

		Set<CarService> services = editWorkshopModel.getServices();
		Set<CarAccessory> accessories = editWorkshopModel.getAccessories();

		if (services.isEmpty()) {
			model.addAttribute("getEmptyServicesResponse", "home.service.none");
		}

		if (accessories.isEmpty()) {
			model.addAttribute("getEmptyAccessoriesResponse", "home.accessory.none");
		}

		String editWorkshopSuccess = request.getParameter("editWorkshopSuccess");
		String editWorkshopError = request.getParameter("editWorkshopError");
		String deleteServiceSuccess = request.getParameter("deleteServiceSuccess");
		String deleteServiceError = request.getParameter("deleteServiceError");
		String deleteAccessorySuccess = request.getParameter("deleteAccessorySuccess");
		String deleteAccessoryError = request.getParameter("deleteAccessoryError");
		String addStationSuccess = request.getParameter("addStationSuccess");
		String addStationError = request.getParameter("addStationError");
		String deleteStationSuccess = request.getParameter("deleteStationSuccess");
		String deleteStationError = request.getParameter("deleteStationError");

		if (editWorkshopSuccess != null) {
			model.addAttribute("editWorkshopSuccess", editWorkshopSuccess);
		}
		if (editWorkshopError != null) {
			model.addAttribute("editWorkshopError", editWorkshopError);
		}
		if (deleteServiceSuccess != null) {
			model.addAttribute("deleteServiceSuccess", deleteServiceSuccess);
		}
		if (deleteServiceError != null) {
			model.addAttribute("deleteServiceError", deleteServiceError);
		}
		if (deleteAccessorySuccess != null) {
			model.addAttribute("deleteAccessorySuccess", deleteAccessorySuccess);
		}
		if (deleteAccessoryError != null) {
			model.addAttribute("deleteAccessoryError", deleteAccessoryError);
		}
		if (addStationSuccess != null) {
			model.addAttribute("addStationSuccess", addStationSuccess);
		}
		if (addStationError != null) {
			model.addAttribute("addStationError", addStationError);
		}
		if (deleteStationSuccess != null) {
			model.addAttribute("deleteStationSuccess", deleteStationSuccess);
		}
		if (deleteStationError != null) {
			model.addAttribute("deleteStationError", deleteStationError);
		}

		model.addAttribute("services", services);
		model.addAttribute("accessories", accessories);
		model.addAttribute("workshopModel", editWorkshopModel);
		model.addAttribute("addressModel", editAddressModel);

		return new ModelAndView("owner-edit-workshop", model);
	}

	@PostMapping("/updateWorkshopData/{workshopId}")
	public ModelAndView updateWorkshopData(@PathVariable("workshopId") long workshopId,
			@ModelAttribute("workshopModel") Workshop workshop, ModelMap model) {
		workshop.setId(workshopId);
		workshop = service.updateWorkshop(workshop);
		model.addAttribute("workshopModel", workshop);
		if (workshop == null) {
			model.addAttribute("editWorkshopError", "owner.workshop.edit.error");
			return new ModelAndView("redirect:/ownerEditWorkshop/" + workshopId, model);
		}

		model.addAttribute("editWorkshopSuccess", "owner.workshop.edit.success");
		return new ModelAndView("redirect:/ownerEditWorkshop/" + workshopId, model);
	}

	@PostMapping("/updateControlPointData/{controlPointId}")
	public ModelAndView updateControlPointData(@PathVariable("controlPointId") long controlPointId,
			@ModelAttribute("controlPoint") VehicleControlPoint controlPointModel, ModelMap model) {

		controlPointModel.setId(controlPointId);
		controlPointModel = service.updateControlPoint(controlPointModel);
		model.addAttribute("controlPoint", controlPointModel);
		if (controlPointModel == null) {
			model.addAttribute("editControlPointError", "owner.control_point.edit.error");
			return new ModelAndView("redirect:/ownerEditControlPoint/" + controlPointId, model);
		}

		model.addAttribute("editControlPointSuccess", "owner.control_point.edit.success");
		return new ModelAndView("redirect:/ownerEditControlPoint/" + controlPointId, model);
	}

	private boolean authenticateOwner(HttpSession session) {
		Person user = (Person) session.getAttribute("sessionUser");
		if (user == null || !user.getType().equals(PersonTypeEnum.OWNER)) {
			System.err.println("[AUTHORIZATION] User is not allowed to enter this page!");
			return false;
		}
		return true;
	}

}
