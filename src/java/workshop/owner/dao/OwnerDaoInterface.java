package workshop.owner.dao;

import java.util.List;

import workshop.model.Calendar;
import workshop.model.CarAccessory;
import workshop.model.CarService;
import workshop.model.Person;
import workshop.model.PersonTypeEnum;
import workshop.model.Request;
import workshop.model.Station;
import workshop.model.VehicleControlPoint;
import workshop.model.Visit;
import workshop.model.Workshop;

public interface OwnerDaoInterface {
	public List<Workshop> getWorkshops(long ownerId);

	public List<VehicleControlPoint> getControlPoints(long ownerId);

	public Workshop findWorkshopById(long workshopId);

	public VehicleControlPoint getControlPointById(long controlPointId);

	public Workshop updateWorkshop(Workshop workshop);

	public VehicleControlPoint updateControlPoint(VehicleControlPoint controlPoint);

	public boolean sendRequest(Request request);

	public CarService saveCarService(CarService carService);

	public CarAccessory saveCarAccessory(CarAccessory accessory);

	public Station addWorkshopStation(long workshopId);

	public Station removeWorkshopStation(long workshopId);

	public Station addControlPointStation(long controlPointId);

	public Station removeControlPointStation(long controlPointId);

	public void generateStationCalendars(long stationId);

	public List<Calendar> getStationCalendars(long stationId, int index);

	public void updateCalendar(boolean remove, Visit visit);

	public Calendar getCalendarById(long id);

	public CarAccessory getAccessoryById(long id);

	public CarService getServiceById(long id);

	public CarService deleteService(long id);

	public CarAccessory deleteAccessory(long id);

	public Station getStationById(long stationId);

	public List<Person> getRecipients(PersonTypeEnum type);

	public List<Request> getNotifications(long ownerId);

	public boolean deleteNotification(long notificationId);

	public Workshop deleteWorkshop(long workshopId);

	public VehicleControlPoint deleteControlPoint(long controlPointId);

	public boolean terminateVisit(long ownerId, long visitId);

	public Workshop fetchWorkshop(long workshopId);
	
	public VehicleControlPoint fetchControlPoint(long controlPointId);
}
