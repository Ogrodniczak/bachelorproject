package workshop.owner.dao.impl;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.transaction.Transactional;

import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import workshop.model.Calendar;
import workshop.model.CarAccessory;
import workshop.model.CarService;
import workshop.model.OrderStatusEnum;
import workshop.model.Person;
import workshop.model.PersonTypeEnum;
import workshop.model.Request;
import workshop.model.RequestStatusEnum;
import workshop.model.Station;
import workshop.model.VehicleControlPoint;
import workshop.model.VehicleControlPointStatusEnum;
import workshop.model.Visit;
import workshop.model.VisitStatusEnum;
import workshop.model.Workshop;
import workshop.model.WorkshopStatusEnum;
import workshop.owner.dao.OwnerDaoInterface;

@Repository
@Transactional
public class OwnerDaoImpl implements OwnerDaoInterface {

	private static final EntityManagerFactory entityManagerFactory = Persistence
			.createEntityManagerFactory("persistenceUnit");

	private EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}

	@PreDestroy
	public void finish() {
		entityManagerFactory.close();
	}

	@Override
	public List<Workshop> getWorkshops(long ownerId) {
		EntityManager em = getEntityManager();
		List<Workshop> foundWorkshops = new ArrayList<>();
		try {
			foundWorkshops = em.createQuery("SELECT w FROM Workshop w WHERE w.owner.id = :ownerId", Workshop.class)
					.setParameter("ownerId", ownerId).getResultList();
			for (Workshop w : foundWorkshops) {
				w.getAccessories().size();
				w.getServices().size();
				w.getStations().size();
			}
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return foundWorkshops;
	}

	@Override
	public List<VehicleControlPoint> getControlPoints(long ownerId) {
		EntityManager em = getEntityManager();
		List<VehicleControlPoint> foundControlPoints = new ArrayList<>();
		try {
			foundControlPoints = em.createQuery("SELECT cp FROM VehicleControlPoint cp WHERE cp.owner.id = :ownerId",
					VehicleControlPoint.class).setParameter("ownerId", ownerId).getResultList();
			for (VehicleControlPoint controlPoint : foundControlPoints) {
				controlPoint.getStations().size();
			}
		} catch (NoResultException e) {
			return null;
		} finally {
			em.close();
		}
		return foundControlPoints;
	}

	@Override
	public Workshop updateWorkshop(Workshop workshop) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		Workshop foundWorkshop = null;
		try {
			tx.begin();
			foundWorkshop = em.find(Workshop.class, workshop.getId());
			if (!em.contains(workshop.getAddress())) {
				workshop.getAddress().setWorkshop(foundWorkshop);
				em.persist(workshop.getAddress());
			}
			foundWorkshop.setName(workshop.getName());
			foundWorkshop.setDescription(workshop.getDescription());
			foundWorkshop.setAddress(workshop.getAddress());
			foundWorkshop.setOpeningHour(workshop.getOpeningHour());
			foundWorkshop.setClosingHour(workshop.getClosingHour());
			em.merge(foundWorkshop);
			tx.commit();
		} catch (IllegalArgumentException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return foundWorkshop;
	}

	@Override
	public boolean sendRequest(Request request) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		try {
			em.persist(request);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return false;
		} finally {
			em.close();
		}
		return true;
	}

	@Override
	public Workshop findWorkshopById(long workshopId) {
		EntityManager em = getEntityManager();
		Workshop workshop = null;
		try {
			workshop = em.find(Workshop.class, workshopId);
			Hibernate.initialize(workshop.getAccessories());
			Hibernate.initialize(workshop.getServices());
			Hibernate.initialize(workshop.getStations());

		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return workshop;
	}

	@Override
	public Workshop fetchWorkshop(long workshopId) {
		EntityManager em = getEntityManager();
		Workshop workshop = null;
		try {
			workshop = em.find(Workshop.class, workshopId);
			Hibernate.initialize(workshop.getAccessories());
			Hibernate.initialize(workshop.getServices());
			Hibernate.initialize(workshop.getStations());
			for(Station s : workshop.getStations()) {
				Hibernate.initialize(s.getDays());
			}
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return workshop;
	}
	
	@Override
	public VehicleControlPoint fetchControlPoint(long controlPointId) {
		EntityManager em = getEntityManager();
		VehicleControlPoint controlPoint = null;
		try {
			controlPoint = em.find(VehicleControlPoint.class, controlPointId);
			Hibernate.initialize(controlPoint.getStations());
			for(Station s : controlPoint.getStations()) {
				Hibernate.initialize(s.getDays());
			}
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return controlPoint;
	}

	@Override
	public CarService saveCarService(CarService carService) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		try {
			em.merge(carService);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return null;
		} finally {
			em.close();
		}
		return carService;
	}

	@Override
	public CarAccessory saveCarAccessory(CarAccessory accessory) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		try {
			accessory = em.merge(accessory);
			tx.commit();
		} catch (Exception e) {
			return null;
		} finally {
			em.close();
		}
		return accessory;
	}

	@Override
	public void generateStationCalendars(long stationId) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();

		LocalDate lastDate;
		Station station;
		try {
			try {
				station = em.find(Station.class, stationId);
			} catch (NoResultException e) {
				return;
			}
			try {
				lastDate = em.createQuery(
						"SELECT calendar.date FROM Calendar calendar WHERE calendar.station.id = :stationId ORDER BY calendar.date DESC",
						LocalDate.class).setParameter("stationId", stationId).setMaxResults(1).getSingleResult();
				lastDate = lastDate.plusDays(1);
			} catch (NoResultException e) {
				lastDate = null;
			}

			lastDate = lastDate == null ? LocalDate.now() : lastDate;
			int openingHour;
			int closingHour;

			if (station.getWorkshop() != null) {
				openingHour = station.getWorkshop().getOpeningHour() == null ? 6
						: station.getWorkshop().getOpeningHour().getHour();
				closingHour = station.getWorkshop().getClosingHour() == null ? 18
						: station.getWorkshop().getClosingHour().getHour();
			} else if (station.getVehicleControlPoint() != null) {
				openingHour = station.getVehicleControlPoint().getOpeningHour() == null ? 6
						: station.getVehicleControlPoint().getOpeningHour().getHour();
				closingHour = station.getVehicleControlPoint().getClosingHour() == null ? 18
						: station.getVehicleControlPoint().getClosingHour().getHour();
			} else {
				return;
			}

			for (int day = 0; day < 7; day++) {
				LocalDate date = lastDate.plusDays(day);
				Calendar calendar = new Calendar();
				calendar.setDate(date);
				calendar.setStation(station);
				calendar.setVisits(new HashSet<>());
				for (int hour = openingHour; hour < closingHour; hour++) {
					calendar.getHours().add(LocalTime.of(hour, 0));
				}

				em.persist(calendar);
			}
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
	}

	@Override
	public List<Calendar> getStationCalendars(long stationId, int index) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		List<Calendar> calendars = new ArrayList<>();
		try {
			calendars = em.createQuery(
					"SELECT calendar FROM Calendar calendar WHERE calendar.station.id = :stationId ORDER BY calendar.date",
					Calendar.class).setParameter("stationId", stationId).getResultList();
			//System.out.println("CAL SIZE: " + calendars.si)
			if (calendars.size() > 7) {
				calendars = index == -1 || index > calendars.size()-7 ? calendars.subList(calendars.size() - 7, calendars.size())
						: calendars.subList(index, index + 7);
			}
			List<LocalTime> visitTimes = new ArrayList<>();
			for (Calendar calendar : calendars) {
				if (calendar.getDate().isBefore(LocalDate.now().plusDays(1))) {
					for (Visit v : calendar.getVisits()) {
						if (v.getStatus().equals(VisitStatusEnum.UNAVAILABLE)) {
							v.setStatus(VisitStatusEnum.OLD);
							em.merge(v);
						}
						visitTimes.addAll(v.getHours());
					}
					for (LocalTime time : calendar.getHours()) {
						if (visitTimes.stream().noneMatch(t -> t.equals(time))) {
							Visit v = new Visit();
							v.setStatus(VisitStatusEnum.OLD);
							v.setHours(Arrays.asList(time));
							v.setDayOfVisit(calendar);
							em.persist(v);
						}
					}
					visitTimes.clear();
				}
				calendar.getVisits().forEach(visit -> visit.getHours().size());
				calendar.getHours().size();
			}
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return null;
		} finally {
			em.close();
		}
		return calendars;
	}

	@Override
	public void updateCalendar(boolean remove, Visit visit) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		try {
			if (remove) {
				Visit visitReference = em.getReference(Visit.class, visit.getId());
				em.remove(visitReference);
			} else {
				Visit updatedVisit = em.merge(visit);
				updatedVisit.getHours().size();
			}
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
	}

	@Override
	public Calendar getCalendarById(long id) {
		EntityManager em = getEntityManager();
		Calendar calendar = null;
		try {
			calendar = em.find(Calendar.class, id);
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return null;
		} finally {
			em.close();
		}
		return calendar;
	}

	@Override
	public CarAccessory getAccessoryById(long id) {
		EntityManager em = getEntityManager();
		CarAccessory accessory = null;
		try {
			accessory = em.find(CarAccessory.class, id);
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return null;
		} finally {
			em.close();
		}
		return accessory;
	}

	@Override
	public CarService getServiceById(long id) {
		EntityManager em = getEntityManager();
		CarService service = null;
		try {
			service = em.find(CarService.class, id);
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return null;
		} finally {
			em.close();
		}
		return service;
	}

	@Override
	public CarService deleteService(long id) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		CarService serviceReference = null;
		tx.begin();
		try {
			serviceReference = em.getReference(CarService.class, id);
			em.remove(serviceReference);
			tx.commit();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return serviceReference;
	}

	@Override
	public CarAccessory deleteAccessory(long id) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		CarAccessory accessoryReference = null;
		tx.begin();
		try {
			accessoryReference = em.getReference(CarAccessory.class, id);
			em.remove(accessoryReference);
			tx.commit();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return accessoryReference;
	}

	@Override
	public VehicleControlPoint getControlPointById(long controlPointId) {
		EntityManager em = getEntityManager();
		VehicleControlPoint controlPoint = null;
		try {
			controlPoint = em.find(VehicleControlPoint.class, controlPointId);
			controlPoint.getStations().size();
		} catch (NoResultException e) {
			e.printStackTrace(System.err);
			return null;
		} finally {
			em.close();
		}
		return controlPoint;
	}

	@Override
	public VehicleControlPoint updateControlPoint(VehicleControlPoint controlPoint) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		VehicleControlPoint foundControlPoint = null;
		try {
			tx.begin();
			foundControlPoint = em.find(VehicleControlPoint.class, controlPoint.getId());
			if (!em.contains(controlPoint.getAddress())) {
				controlPoint.getAddress().setVehicleControlPoint(foundControlPoint);
				em.persist(controlPoint.getAddress());
			}
			foundControlPoint.setName(controlPoint.getName());
			foundControlPoint.setDescription(controlPoint.getDescription());
			foundControlPoint.setAddress(controlPoint.getAddress());
			foundControlPoint.setOpeningHour(controlPoint.getOpeningHour());
			foundControlPoint.setClosingHour(controlPoint.getClosingHour());
			em.merge(foundControlPoint);
			tx.commit();
		} catch (IllegalArgumentException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return controlPoint;
	}

	@Override
	public Station removeWorkshopStation(long workshopId) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		Station station = null;
		try {
			tx.begin();
			station = em
					.createQuery("SELECT st FROM Station st WHERE st.workshop.id = :workshopId ORDER BY st.number DESC",
							Station.class)
					.setParameter("workshopId", workshopId).setMaxResults(1).getResultList().get(0);
			em.remove(station);
			tx.commit();
		} catch (IllegalArgumentException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return station;
	}

	@Override
	public Station addWorkshopStation(long workshopId) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		Station station = null;
		try {
			tx.begin();
			Workshop workshop = em.find(Workshop.class, workshopId);
			station = new Station();
			station.setNumber(workshop.getStations().size() + 1);
			station.setWorkshop(workshop);
			workshop.getStations().add(station);
			em.persist(station);
			tx.commit();
		} catch (IllegalArgumentException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return station;
	}

	@Override
	public Station addControlPointStation(long controlPointId) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		Station station = null;
		try {
			tx.begin();
			VehicleControlPoint controlPoint = em.find(VehicleControlPoint.class, controlPointId);
			station = new Station();
			station.setNumber(controlPoint.getStations().size() + 1);
			station.setVehicleControlPoint(controlPoint);
			em.persist(station);
			tx.commit();
		} catch (IllegalArgumentException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return station;
	}

	@Override
	public Station removeControlPointStation(long controlPointId) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		Station station = null;
		try {
			tx.begin();
			station = em
					.createQuery("SELECT st FROM Station st WHERE st.vehicleControlPoint.id = :controlPointId ORDER BY st.number DESC",
							Station.class)
					.setParameter("controlPointId", controlPointId).setMaxResults(1).getResultList().get(0);
			em.remove(station);
			tx.commit();
		} catch (IllegalArgumentException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return station;
	}

	@Override
	public Station getStationById(long stationId) {
		EntityManager em = getEntityManager();
		Station station = null;
		try {
			station = em.find(Station.class, stationId);
			station.getDays().size();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return station;
	}

	@Override
	public List<Person> getRecipients(PersonTypeEnum type) {
		EntityManager em = getEntityManager();
		List<Person> recipients = new ArrayList<>();
		try {
			recipients = em.createQuery("SELECT p FROM Person p WHERE p.type = :type", Person.class)
					.setParameter("type", type).getResultList();
		} catch (NoResultException e) {
			return recipients;
		} finally {
			em.close();
		}
		return recipients;
	}

	@Override
	public List<Request> getNotifications(long ownerId) {
		EntityManager em = getEntityManager();
		List<Request> foundRequests = new ArrayList<>();
		try {
			foundRequests = em.createQuery(
					"SELECT r FROM Request r WHERE :ownerId = ANY (SELECT recipient.id FROM r.recipients recipient) AND r.status = :status",
					Request.class).setParameter("ownerId", ownerId).setParameter("status", RequestStatusEnum.ACTIVE)
					.getResultList();
		} catch (NoResultException e) {
			return foundRequests;
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			em.close();
		}
		return foundRequests;
	}

	@Override
	public boolean deleteNotification(long notificationId) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		Request notificationReference;
		try {
			notificationReference = em.getReference(Request.class, notificationId);
			em.remove(notificationReference);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return false;
		} finally {
			em.close();
		}
		return true;
	}

	@Override
	public Workshop deleteWorkshop(long workshopId) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		Workshop workshop;
		try {
			workshop = em.find(Workshop.class, workshopId);
			workshop.setStatus(WorkshopStatusEnum.REMOVED);
			em.merge(workshop);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return null;
		} finally {
			em.close();
		}
		return workshop;
	}

	@Override
	public VehicleControlPoint deleteControlPoint(long controlPointId) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		VehicleControlPoint controlPoint;
		try {
			controlPoint = em.find(VehicleControlPoint.class, controlPointId);
			controlPoint.setStatus(VehicleControlPointStatusEnum.REMOVED);
			em.merge(controlPoint);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return null;
		} finally {
			em.close();
		}
		return controlPoint;
	}

	@Override
	public boolean terminateVisit(long ownerId, long visitId) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		try {
			Visit visit = em.find(Visit.class, visitId);
			Workshop w = visit.getDayOfVisit().getStation().getWorkshop();
			VehicleControlPoint v = visit.getDayOfVisit().getStation().getVehicleControlPoint();
			if (w != null && w.getOwner().getId() == ownerId) {
				visit.setStatus(VisitStatusEnum.TERMINATED);
				visit.getOrder().setStatus(OrderStatusEnum.TERMINATED);
				Map<CarAccessory, Integer> accessories = visit.getOrder().getAccessories();
				for (CarAccessory a : accessories.keySet()) {
					a.setAmount(a.getAmount() - accessories.get(a));
					a.setAmountSold(a.getAmountSold() + accessories.get(a));
					if (a.getAmount() == 0) {
						a.setAvailable(false);
					}
				}
				Map<CarService, Integer> services = visit.getOrder().getServices();
				for (CarService s : services.keySet()) {
					s.setAmountSold(s.getAmountSold() + services.get(s));
				}
			} else if (v != null && v.getOwner().getId() == ownerId) {
				visit.setStatus(VisitStatusEnum.TERMINATED);
			} else {
				return false;
			}
			em.merge(visit);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return false;
		} finally {
			em.close();
		}
		return true;
	}
}
