package workshop.owner.service;

import java.time.LocalTime;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import workshop.model.Calendar;
import workshop.model.CarAccessory;
import workshop.model.CarService;
import workshop.model.Person;
import workshop.model.Request;
import workshop.model.Station;
import workshop.model.VehicleControlPoint;
import workshop.model.Visit;
import workshop.model.Workshop;

public interface OwnerServiceInterface {
	public List<Workshop> getWorkshops(long ownerId);

	public List<VehicleControlPoint> getControlPoints(long ownerId);

	public boolean deleteWorkshop(long workshopId);

	public boolean sendRequest(Request request, Person user);

	public Workshop getWorkshopById(long workshopId);
	
	public Workshop fetchWorkshop(long workshopId);
	
	public VehicleControlPoint fetchControlPoint(long controlPointId);

	public Station getStationById(long stationId);

	public VehicleControlPoint getControlPointById(long controlPointId);

	public Workshop updateWorkshop(Workshop workshop);

	public VehicleControlPoint updateControlPoint(VehicleControlPoint controlPoint);

	public CarService saveCarService(CarService carService);

	public CarAccessory saveCarAccessory(CarAccessory accessory);

	public void generateStationCalendars(long stationId);

	public Map<Calendar, Map<LocalTime, Visit>> getStationCalendars(long stationId, int index);

	public void updateCalendar(boolean remove, Visit visit);

	public Station addStation(String item, long itemId);

	public Station removeStation(String item, long itemId);

	public Calendar getCalendarById(long calendarId);

	public CarAccessory getAccessoryById(long accessoryId);

	public CarService getServiceById(long serviceId);

	public CarService deleteService(long serviceId);

	public CarAccessory deleteAccessory(long accessoryId);

	public Visit getVisitById(long visitId);

	public void cancelVisit(HttpServletRequest request, Visit visit);

	public List<Request> getNotifications(long id);

	public boolean deleteNotification(long notificationId);

	public boolean deleteControlPoint(long controlPointId);

	public boolean terminateVisit(long id, long visitId);
}
