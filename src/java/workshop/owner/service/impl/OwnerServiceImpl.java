package workshop.owner.service.impl;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import workshop.MailSender;
import workshop.client.dao.ClientDaoInterface;
import workshop.model.Calendar;
import workshop.model.CarAccessory;
import workshop.model.CarService;
import workshop.model.ItemTypeEnum;
import workshop.model.Person;
import workshop.model.PersonTypeEnum;
import workshop.model.Request;
import workshop.model.RequestStatusEnum;
import workshop.model.Station;
import workshop.model.VehicleControlPoint;
import workshop.model.Visit;
import workshop.model.Workshop;
import workshop.owner.dao.OwnerDaoInterface;
import workshop.owner.service.OwnerServiceInterface;

@Service
public class OwnerServiceImpl implements OwnerServiceInterface {

	@Autowired
	@Qualifier("ownerDaoImpl")
	private OwnerDaoInterface dao;

	@Autowired
	@Qualifier("clientDaoImpl")
	private ClientDaoInterface clientDao;

	@Override
	public List<Workshop> getWorkshops(long ownerId) {
		return dao.getWorkshops(ownerId);
	}

	@Override
	public List<VehicleControlPoint> getControlPoints(long ownerId) {
		return dao.getControlPoints(ownerId);
	}

	@Override
	public boolean deleteWorkshop(long workshopId) {
		Workshop workshop = null;
		if ((workshop = dao.deleteWorkshop(workshopId)) != null) {
			Request request = new Request();
			request.setSubject("Workshop removal");
			request.setContent("Request to remove workshop: " + workshop.getId());
			if (!sendRequest(request, workshop.getOwner())) {
				return false;
			}
			switch (MailSender.sendMail(request)) {
			case DELIVERED:
				System.out.println("[MAIL - SUCCESS] Mail with request to remove workshop " + workshop.getId()
						+ " sent successfully");
				break;
			case ERROR:
				System.err.println("[MAIL - ERROR] Unexpected error occurred while sending the mail message");
				break;
			case NO_RECIPIENTS:
				System.err.println("[MAIL - ERROR] Mail cannot be sent - no recipients provided");
				break;
			case NO_SENDER:
				System.err.println("[MAIL - ERROR] Mail cannot be sent - no sender provided");
				break;
			default:
				break;
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean sendRequest(Request request, Person user) {
		List<Person> recipients = dao.getRecipients(PersonTypeEnum.ADMIN);
		request.setSender(user);
		request.setRecipients(recipients);
		request.setDate(LocalDateTime.now().withNano(0));
		request.setStatus(RequestStatusEnum.ACTIVE);
		return dao.sendRequest(request);
	}

	@Override
	public Workshop getWorkshopById(long workshopId) {
		return dao.findWorkshopById(workshopId);
	}

	@Override
	public Workshop updateWorkshop(Workshop workshop) {
		return dao.updateWorkshop(workshop);
	}

	@Override
	public CarService saveCarService(CarService carService) {
		return dao.saveCarService(carService);
	}

	@Override
	public CarAccessory saveCarAccessory(CarAccessory accessory) {
		return dao.saveCarAccessory(accessory);
	}

	@Override
	public void generateStationCalendars(long stationId) {
		dao.generateStationCalendars(stationId);
	}

	@Override
	public Map<Calendar, Map<LocalTime, Visit>> getStationCalendars(long stationId, int index) {
		List<Calendar> calendars = dao.getStationCalendars(stationId, index);
		Map<Calendar, Map<LocalTime, Visit>> calendarVisitMap = new TreeMap<>(new CalendarComparator());
		Map<LocalTime, Visit> visitHourMap = new TreeMap<>();
		for (Calendar calendar : calendars) {
			for (Visit visit : calendar.getVisits()) {
				visit.getHours().forEach(hour -> visitHourMap.put(hour, visit));
			}
			calendar.getHours().forEach(hour -> visitHourMap.putIfAbsent(hour, null));
			calendarVisitMap.put(calendar, new TreeMap<>(visitHourMap));
			visitHourMap.clear();
		}

		return calendarVisitMap;
	}

	private class CalendarComparator implements Comparator<Calendar> {
		@Override
		public int compare(Calendar a, Calendar b) {
			if (a.getDate().isBefore(b.getDate())) {
				return -1;
			}
			return 1;
		}
	}

	@Override
	public void updateCalendar(boolean remove, Visit visit) {
		dao.updateCalendar(remove, visit);
	}

	@Override
	public Calendar getCalendarById(long calendarId) {
		return dao.getCalendarById(calendarId);
	}

	@Override
	public CarAccessory getAccessoryById(long accessoryId) {
		return dao.getAccessoryById(accessoryId);
	}

	@Override
	public CarService getServiceById(long serviceId) {
		return dao.getServiceById(serviceId);
	}

	@Override
	public CarService deleteService(long serviceId) {
		return dao.deleteService(serviceId);
	}

	@Override
	public CarAccessory deleteAccessory(long accessoryId) {
		return dao.deleteAccessory(accessoryId);
	}

	@Override
	public VehicleControlPoint getControlPointById(long controlPointId) {
		return dao.getControlPointById(controlPointId);
	}

	@Override
	public VehicleControlPoint updateControlPoint(VehicleControlPoint controlPoint) {
		return dao.updateControlPoint(controlPoint);
	}

	@Override
	public Station addStation(String item, long itemId) {
		return item.equalsIgnoreCase(ItemTypeEnum.WORKSHOP.toString()) ? dao.addWorkshopStation(itemId)
				: dao.addControlPointStation(itemId);
	}

	@Override
	public Station removeStation(String item, long itemId) {
		return item.equalsIgnoreCase(ItemTypeEnum.WORKSHOP.toString()) ? dao.removeWorkshopStation(itemId)
				: dao.removeControlPointStation(itemId);
	}

	@Override
	public Station getStationById(long stationId) {
		return dao.getStationById(stationId);
	}

	@Override
	public Visit getVisitById(long visitId) {
		return clientDao.getVisitById(visitId);
	}

	@Override
	public void cancelVisit(HttpServletRequest httpRequest, Visit visit) {
		Request request = new Request();
		Station station = visit.getDayOfVisit().getStation();
		Person owner;
		Locale.setDefault(httpRequest.getLocale());
		ResourceBundle bundle = ResourceBundle.getBundle("language");
		String subject;

		if (station.getWorkshop() == null) {
			owner = station.getVehicleControlPoint().getOwner();
			subject = MessageFormat.format(bundle.getString("notification.visit.cancel.control_point"),
					station.getVehicleControlPoint().getName());
		} else {
			owner = station.getWorkshop().getOwner();
			subject = MessageFormat.format(bundle.getString("notification.visit.cancel.workshop"),
					station.getWorkshop().getName());
		}

		List<LocalTime> hours = visit.getHours();
		String content = MessageFormat.format(bundle.getString("notification.visit.cancel"), owner.getFirstName(),
				owner.getSurname(), owner.getUserName()) + "<br>" + bundle.getString("date") + ": "
				+ visit.getDayOfVisit().getDate() + "<br>" + bundle.getString("hours") + ": " + hours.get(0) + " - "
				+ hours.get(hours.size() - 1).plusHours(1);

		request.setSubject(subject);
		request.setContent(content);
		request.setDate(LocalDateTime.now().withNano(0));
		request.setSender(owner);

		request.setRecipients(Arrays.asList(visit.getClient()));
		request.setStatus(RequestStatusEnum.ACTIVE);
		clientDao.removeVisit(visit, request);

		switch (MailSender.sendMail(request)) {
		case DELIVERED:
			System.out.println(
					"[MAIL - SUCCESS] Mail with request to cancel visit " + visit.getId() + " sent successfully");
			break;
		case ERROR:
			System.err.println("[MAIL - ERROR] Unexpected error occurred while sending the mail message");
			break;
		case NO_RECIPIENTS:
			System.err.println("[MAIL - ERROR] Mail cannot be sent - no recipients provided");
			break;
		case NO_SENDER:
			System.err.println("[MAIL - ERROR] Mail cannot be sent - no sender provided");
			break;
		default:
			break;
		}
	}

	@Override
	public List<Request> getNotifications(long ownerId) {
		return dao.getNotifications(ownerId);
	}

	@Override
	public boolean deleteNotification(long notificationId) {
		return dao.deleteNotification(notificationId);
	}

	@Override
	public boolean deleteControlPoint(long controlPointId) {
		VehicleControlPoint controlPoint = null;
		if ((controlPoint = dao.deleteControlPoint(controlPointId)) != null) {
			Request request = new Request();
			request.setSubject("Vehicle control point removal");
			request.setContent("Request to remove vehicle control point: " + controlPoint.getId());
			if (!sendRequest(request, controlPoint.getOwner())) {
				return false;
			}
			switch (MailSender.sendMail(request)) {
			case DELIVERED:
				System.out.println("[MAIL - SUCCESS] Mail with request to remove vehicle control point "
						+ controlPoint.getId() + " sent successfully");
				break;
			case ERROR:
				System.err.println("[MAIL - ERROR] Unexpected error occurred while sending the mail message");
				break;
			case NO_RECIPIENTS:
				System.err.println("[MAIL - ERROR] Mail cannot be sent - no recipients provided");
				break;
			case NO_SENDER:
				System.err.println("[MAIL - ERROR] Mail cannot be sent - no sender provided");
				break;
			default:
				break;
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean terminateVisit(long ownerId, long visitId) {
		return dao.terminateVisit(ownerId, visitId);
	}

	@Override
	public Workshop fetchWorkshop(long workshopId) {
		return dao.fetchWorkshop(workshopId);
	}
	
	@Override
	public VehicleControlPoint fetchControlPoint(long controlPointId) {
		return dao.fetchControlPoint(controlPointId);
	}
}