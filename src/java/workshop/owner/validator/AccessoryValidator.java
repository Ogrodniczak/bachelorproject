package workshop.owner.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import workshop.model.CarAccessory;

public class AccessoryValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return CarAccessory.class.equals(clazz);
	}

	@Override
	public void validate(Object object, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "validation.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "category", "validation.empty");

		CarAccessory a = (CarAccessory) object;
		if (a.getName().length() > 25) {
			errors.rejectValue("name", "validation.max_25_chars");
		}
		if (a.getCost() == null || a.getCost() < 0) {
			errors.rejectValue("cost", "validation.incorrect_amount");
		}
		if (a.getDiscount() == null || a.getDiscount() < 0) {
			errors.rejectValue("discount", "validation.incorrect_amount");
		}
		if (a.getAmount() == null || a.getAmount() < 0) {
			errors.rejectValue("amount", "validation.incorrect_amount");
		}
	}
}
