package workshop.owner.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import workshop.model.CarService;

public class ServiceValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return CarService.class.equals(clazz);
	}

	@Override
	public void validate(Object object, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "validation.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "category", "validation.empty");

		CarService s = (CarService) object;
		if (s.getName().length() > 25) {
			errors.rejectValue("name", "validation.max_25_chars");
		}
		if (s.getTime() == null || s.getTime() <= 0) {
			errors.rejectValue("time", "validation.incorrect_amount");
		}
		if (s.getCost() == null || s.getCost() < 0) {
			errors.rejectValue("cost", "validation.incorrect_amount");
		}
		if (s.getDiscount() == null || s.getDiscount() < 0) {
			errors.rejectValue("discount", "validation.incorrect_amount");
		}
	}
}
