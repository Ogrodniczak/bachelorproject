nav.home = Home
nav.news = News
nav.help = Help
nav.about = About
nav.language = Language
nav.language.english = English (US)
nav.language.polish = Polski (PL)
nav.login = Sing in
nav.logout = Sign out
nav.register = Register

home = Home page
home.welcome = Welcome to FixMyRide 
home.description = Web application for car workshop owners and clients
home.workshops.description = Search for registered workshops. No input results in a complete list of all workshops.
home.control_points.description = Search for registered vehicle control points. No input results in a complete list of all points.
home.car_services.description = Search for car services. Results are sorted by the lowest prices.
home.car_accessories.description = Search for car accessories. Results are sorted by the lowest prices.
home.best_sellers.description = Top selling accessories & services

id= Id
name = Name
description = Description

address = Address
address.street = Street
address.city = City
address.postalcode = Postal code

workshop = Workshop
workshop.edit = Edit workshop
workshops = Workshops

search = Search
rating = Rating
price = Price
details = Details
control_points = Vehicle control points
car_services = Car services
car_accessories = Car accessories
discount = Discount
discounts = Discounts
discounts.description = Highest current discounts on car accessory & equipment
best_sellers = Best sellers

submit = Submit
edit = Edit
delete = Delete
save = Save
reserve = Reserve
cancel = Cancel
show = Show
add = Add
proceed = Proceed

enable = Enable
disable = Disable

login.personalized_page = Click here to enter personalized page
register.login = Click here to log in

first_name = First Name
surname = Surname
user.name = User name
user.type = User type

select = --SELECT--
select.admin = ADMIN
select.owner = OWNER
select.client = CLIENT

email = Email address
phone_number = Phone number
password = Password
password.confirm = Confirm password

admin = Admin
admin.page = Admin page
admin.welcome = Administration
admin.description = Main page for workshops and vehicle control points management. Add, modify and remove elements.
admin.add = Add elements
admin.add.description = Add new workshops and vehicle control points
admin.add.workshop = Add workshop
admin.add.control_point = Add control point
admin.add.control_point.l = Add vehicle control point
admin.back = Click to go back to the admin page

status = Status

request = Request
request.subject = Subject
request.content = Content
request.send = Send request to the administration
request.resolve = Resolve
requests = Requests
requests.current = Current active requests
requests.current.none = No current requests

topic = Topic
date = Date
time = Time
sender = Sender

title = Car Workshop Web Application

info.general = General information
info.footer = Developed by Mateusz Ogrodniczak. All rights reserved

owner = Owner
owner.page = Owner page
owner.description = Main page for the management of your workshops and vehicle control points.
owner.id = Owner id
owner.user_name = Owner user name
owner.phone_number = Owner phone number
owner.email = Owner email address
owner.back = Click to go back to the owner page
owner.back.workshop = Click to go back to the workshop edit view

amount = Amount
amount.sold = Amount sold
amount.left Amount left
sold = Sold

available = Available
availability = Availability
not_available = Not available

order = Order
order.add = Add to order
orders.current = Current orders
orders.previous = Previous orders
orders.history = History of orders

calendar = Calendar
calendar.workshop = Manage visiting hours of your workshop
calendar.control_point = Manage visiting hours of your vehicle control point
calendar.description = Check current visit calendars
calendars = Calendars
calendars.generate = Generate calendar for the next week

station = Station
stations = Stations
stations.amount = Number of stations
stations.none = No stations found

hour.opening = Opening hour
hour.closing = Closing hour
hours = Hours

user = User
user.workshops = Your workshops
user.workshops.list = List of all owned workshops
user.control_points = Your vehicle control points
user.control_points.list = List of all owned vehicle control points

service = Service
service.amount.sold = Repairs
service.save = Save service
services = Services

accessory = Accessory
accessory.image = Accessory image
accessory.save = Save accessory
accessories = Accessories

upload = Upload
upload.select = Select file to upload

client = Client
client.page = Client page
client.description = Manage your current and previous orders.

about.description = FixMyRide is a web application written as a bachelor thesis. It provides four levels of user accessibility: admin page, owner page, client page and guest page. Each type of user is only authorized to access its own sites.
about.technologies = Main technologies used: Java, Java Server Pages, Spring MVC, Hibernate
about.repository = Remote repository: https://gitlab.com/Ogrodniczak/bachelorproject
about.author = Developed by Mateusz Ogrodniczak, 2018. 

notifications = Notifications
notifications.messages = Check received messages

validation.empty = This field is required
validation.max_15_chars = Length of this field cannot exceed 15
validation.max_20_chars = Length of this field cannot exceed 20
validation.max_25_chars = Length of this field cannot exceed 25
validation.password.no_match = Confirmed password does not match
validation.no_user_type = You must choose a user type
validation.no_owner_data = Either owner's id or user name must be filled
validation.incorrect_amount = Incorrect value

visit = Visit
visit.info = Visit details

week.next = Next week
week.previous = Previous week

category = Category

MONDAY = Monday
TUESDAY = Tuesday
WEDNESDAY = Wednesday
THURSDAY = Thursday
FRIDAY = Friday
SATURDAY = Saturday
SUNDAY = Sunday


### MESSAGES - ADMIN
admin.workshop.none = No workshops found
admin.workshop.add.success = Workshop successfully created.
admin.workshop.add.error = Workshop with the given name already exists
admin.workshop.add.error.form = Workshop cannot be added
admin.workshop.delete.success = Workshop successfully removed
admin.workshop.delete.error = Workshop could not be removed

admin.control_point.none = No vehicle control points found
admin.control_point.add.success = Vehicle control point successfully created.
admin.control_point.add.error = Vehicle control point with the given name already exists
admin.control_point.add.error.form = Vehicle control point cannot be added
admin.control_point.delete.success = Vehicle control point successfully removed
admin.control_point.delete.error = Vehicle control point could not be removed

admin.request.none = No current requests found
admin.request.resolve.success = Request resolved successfully
admin.request.resolve.error = Request could not be resolved

admin.add.error.owner = Owner with the given attributes was not found

### MESSAGES - HOME
login.error.user = There is no user with the given credentials
login.success = Logged in successfully

register.error = Registration unsuccessful
register.error.user = User with given credentials already exists
register.success = Registration completed successfully

home.workshop.none = No workshops found
home.control_point.none = No vehicle control points found
home.accessory.none = No car accessories found
home.service.none = No car services found

### MESSAGES - CLIENT
client.notification.none = No current notifications
client.orders_current.none = No current orders
client.orders_previous.none = No previous orders
client.add.notification.error = Notification could not be deleted
client.add.service = Service added to the cart
client.add.service.error = Service could not be added to the cart
client.add.accessory = Accessory added to the cart
client.add.accessory.error = Accessory could not be added to the cart
client.login.error = You must be logged in as a client
client.visit.cancel.success = Visit cancelled successfully
client.visit.cancel.error = Visit could not be cancelled
client.visit.reserve.success = Visit reserved successfully
client.visit.reserve.error = Visit could not be reserved
client.visit.reserve.error.order = Visit cannot be reserved, no order found
client.visit.reserve.time = Visit hours collide with another visit
client.order.cancel.success = Order removed successfully
client.order.cancel.error = Order could not be removed

### MESSAGES - OWNER
owner.notification.none = No current notifications
owner.notification.delete.success = Notification removed successfully
owner.notification.delete.error = Notification could not be deleted
owner.request.success = Request sent successfully
owner.request.error = Request could not be sent
owner.service.save.success = Service saved successfully
owner.service.save.error = Service could not be saved
owner.service.delete.success = Service removed successfully
owner.service.delete.error = Service could not be removed
owner.accessory.save.success = Accessory saved successfully
owner.accessory.save.error = Accessory could not be saved
owner.accessory.delete.success = Accessory removed successfully
owner.accessory.delete.error = Accessory could not be removed
owner.station.add.success = New station added successfully
owner.station.add.error = New station could not be added
owner.station.delete.success = Station removed successfully
owner.station.delete.error = Station could not be removed
owner.workshop.delete.success = Request to delete workshop sent successfully
owner.workshop.delete.error = Removal request could not be sent
owner.workshop.edit.success = Workshop updated successfully
owner.workshop.edit.error = Workshop could not be updated with the given data
owner.control_point.delete.success = Request to delete vehicle control point sent successfully
owner.control_point.delete.error = Removal request could not be sent
owner.control_point.edit.success = Vehicle control point updated successfully
owner.control_point.edit.error = Vehicle control point could not be updated with the given data
owner.address.edit.success = Address updated successfully
owner.address.edit.error = Address could not be updated with the given data

### NOTIFICATIONS
notification.visit.cancel.workshop = Visit cancelled - workshop: {0}
notification.visit.cancel.control_point = Visit cancelled - control point: {0}
notification.visit.cancel = Visit cancelled by {0} {1}, user name {2}.
notification.visit.reserve.workshop = Visit reservation - workshop: {0}
notification.visit.reserve.control_point = Visit reservation - control point: {0}
notification.visit.reserve = Visit reserved by {0} {1}, user name {2}.

### CATEGORIES
ALL = All
AIRBAGS = Airbags
ALARMS = Alarms
ALTERNATORS = Alternators
BATTERIES = Batteries
BONNETS = Bonnets & hoods
BRAKES = Brakes
BELTS = Belts
BUMPERS = Bumpers
CABLES = Cables
CONTROLLERS = Controllers
COOLING = Cooling system
DOORS = Doors
ELECTRONICS = Electronics
ENGINES = Engines
FASTENERS = Fasteners
FLOOR = Floor
FUEL = Fuel
FUSES = Fuses
LIGNTING = Lighting & signals
METERS = Meters & gauges
MIRRORS = Mirrors
MISCELLANEOUS = Miscellaneous
OILS = Oils
PAINT = Paint
PILLARS = Pillars
SEATS = Seats
SENSORS = Sensors
STARTERS = Starters
STEERING = Steering
SUN_VISOR = Sun visors
SWITCHES = Switches
QUARTER_PANEL = Quarter panel
WASHER = Washer
WHEELS = Wheels
WINDOWS = Windows

### STATUS MESSAGES
NEW = New
SCHEDULED = Scheduled
TERMINATED = Terminated
ACTIVE = Active
REMOVED = Removed

### MISCELLANEOUS
TRUE = Yes
FALSE = No
true = Yes
false = No