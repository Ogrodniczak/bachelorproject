<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title><spring:message code="title" /></title>
<link href="resources/css/bootstrap.css" rel="stylesheet"
	type="text/css" />
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<!-- NAVIGATION BAR -->
	<jsp:include page="navbar.jsp"></jsp:include>

	<c:choose>
		<c:when test="${not empty loginError}">
			<div class="alert alert-dismissible alert-primary">
				${loginError}</div>
		</c:when>
		<c:when test="${not empty loginSuccess}">
			<div class="alert alert-dismissible alert-success">
				${loginSuccess} <a href="/CarWorkshop/getLoginView"
					class="alert-link"><spring:message
						code="login.personalized_page" /></a>
			</div>
		</c:when>
	</c:choose>

	<!--  MAIN CONTAINER -->
	<div class="jumbotron">
		<h1 class="display-3 text-container-main">
			<spring:message code="nav.about" />
		</h1>
		<hr class="my-4 text-container-main">
		<div class="text-container-main">
			<p>
				<spring:message code="about.description" />
			</p>
			<p>
				<spring:message code="about.technologies" />
			</p>
			<p>
				<spring:message code="about.repository" />
			</p>
			<p>
				<spring:message code="about.author" />
			</p>
		</div>
	</div>

	<!-- FOOTER -->
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
