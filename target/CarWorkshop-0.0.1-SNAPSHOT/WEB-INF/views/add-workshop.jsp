<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title><spring:message code="title" /></title>
<link href="resources/css/bootstrap.css" rel="stylesheet"
	type="text/css" />
</head>
<body>
	<!-- NAVIGATION BAR -->
	<jsp:include page="navbar.jsp"></jsp:include>

	<div class="jumbotron">
		<h1 class="display-3 text-container-main">
			<spring:message code="admin.add.workshop" />
		</h1>
		<hr class="my-4 text-container-main">
		<c:choose>
			<c:when test="${not empty addWorkshopError}">
				<div
					class="alert alert-dismissible alert-primary text-container-main">
					<spring:message code="${addWorkshopError}" />
				</div>
			</c:when>
			<c:when test="${not empty addWorkshopSuccess}">
				<div
					class="alert alert-dismissible alert-success text-container-main">
					<spring:message code="${addWorkshopSuccess}" />
					<a href="/CarWorkshop/adminPage" class="alert-link"> <spring:message
							code="admin.back" /></a>
				</div>
			</c:when>
			<c:when test="${not empty workshopErrorForm}">
				<div
					class="alert alert-dismissible alert-primary text-container-main">
					<spring:message code="${workshopErrorForm}" />
				</div>
			</c:when>
		</c:choose>

		<form:form action="/CarWorkshop/submitNewWorkshop" method="POST"
			modelAttribute="workshopModel">
			<h2 class="text-container-main">
				<spring:message code="info.general" />
			</h2>
			<div class="Row text-container-main">
				<div class="Column-label-long">
					<form:label class="lead" path="name">
						<spring:message code="name" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text" path="name" />
				</div>
				<div class="Column error">
					<form:errors path="name" />
				</div>
			</div>
			<div class="Row text-container-main">
				<div class="Column-label-long">
					<form:label class="lead" path="description">
						<spring:message code="description" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text"
						path="description" />
				</div>
				<div class="Column error">
					<form:errors path="description" />
				</div>
			</div>
			<div class="Row text-container-main">
				<div class="Column-label-long">
					<form:label class="lead" type="number" path="owner.id">
						<spring:message code="owner.id" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text"
						path="owner.id" />
				</div>
				<div class="Column error">
					<form:errors path="owner.id" />
				</div>
			</div>
			<div class="Row text-container-main">
				<div class="Column-label-long">
					<form:label class="lead" path="owner.userName">
						<spring:message code="owner.user_name" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text"
						path="owner.userName" />
				</div>
				<div class="Column error">
					<form:errors path="owner.userName" />
				</div>
			</div>
			<hr class="my-4 text-container-main">
			<div>
				<input class="btn btn-info btn-lg text-container-main" type="submit"
					value=<spring:message code="save" /> />
			</div>
		</form:form>
	</div>

	<!-- FOOTER -->
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>