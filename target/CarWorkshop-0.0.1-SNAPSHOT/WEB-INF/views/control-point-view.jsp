<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
<title><spring:message code="title" /></title>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/bootstrap.css" />" />
</head>
<body>
	<!-- NAVIGATION BAR -->
	<jsp:include page="navbar.jsp"></jsp:include>

	<!-- MAIN CONTAINER -->
	<div class="jumbotron">
		<h1 class="display-3 text-container-main">${controlPoint.name}</h1>
		<hr class="my-4 text-container-main">

		<div class="text-container-main">
			<h2>
				<spring:message code="info.general" />
			</h2>
			<p>${controlPoint.description }</p>
			<p>
				<spring:message code="address" />
				: ${controlPoint.address.street} ${controlPoint.address.city}
				${controlPoint.address.postalCode}
			</p>
			<p>
				<spring:message code="hour.opening" />
				: ${controlPoint.openingHour }
			</p>
			<p>
				<spring:message code="hour.closing" />
				: ${controlPoint.closingHour }
			</p>
			<p>
				<spring:message code="owner.phone_number" />
				:
				<c:choose>
					<c:when test="${empty controlPoint.owner.phoneNumber }">
						<b style="color: red"><spring:message code="not_available" /></b>
					</c:when>
					<c:otherwise>
		${controlPoint.owner.phoneNumber }
		</c:otherwise>
				</c:choose>
			</p>
			<p>
				<spring:message code="owner.email" />
				: ${controlPoint.owner.emailAddress }
			</p>
			<p>
				<spring:message code="stations.amount" />
				: ${nrOfStations}
			</p>
		</div>
		<hr class="my-4 text-container-main">

		<h2 class="text-container-main">
			&#128197 <spring:message code="calendar" />
		</h2>
		<p class="text-container-main">
			<spring:message code="calendar.description" />
		</p>
		<a
			href="/CarWorkshop/clientGetCalendarView/controlPoint/${controlPoint.id }/0/-1">
			<button class="btn btn-info btn-lg text-container-main"
				id="showCalendar">
				<spring:message code="show" />
			</button>
		</a>
		<hr class="my-4 text-container-main">
	</div>

	<!-- FOOTER -->
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>