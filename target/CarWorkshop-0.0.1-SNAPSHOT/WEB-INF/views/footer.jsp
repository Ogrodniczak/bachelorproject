<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!-- FOOTER -->
<div class="custom-footer">
	<div class="jumbotron" style="background-color: rgb(0, 0, 20);">
		<p class="social-media" align="center">
			<a href="#" class="fa fa-facebook"></a> <a href="#"
				class="fa fa-twitter"></a> <a href="#" class="fa fa-instagram"></a>
			<a href="#" class="fa fa-linkedin"></a> <a href="#"
				class="fa fa-youtube"></a>
		</p>

		<p class="lead" align="center" style="color: white">
			<a href="#" style="color: white"><spring:message code="nav.home" /></a> / <a href="#"
				style="color: white"><spring:message code="nav.news" /></a> / <a href="#" style="color: white">
				<spring:message code="nav.help" /></a>
			/ <a href="#" style="color: white"><spring:message code="nav.about" /></a>
		</p>
		<p align="center" style="color: white">&copy; 
		<spring:message code="info.footer" /></p>
	</div>
</div>