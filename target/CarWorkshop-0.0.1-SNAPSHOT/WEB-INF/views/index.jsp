<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.io.ByteArrayOutputStream"%>
<%@ page import="java.io.File"%>
<%@ page import="javax.imageio.ImageIO"%>
<%@ page import="java.awt.image.BufferedImage"%>
<html>
<head>
<title><spring:message code="title" /></title>
<link href="resources/css/bootstrap.css" rel="stylesheet"
	type="text/css" />
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

	<!-- NAVIGATION BAR -->
	<jsp:include page="navbar.jsp"></jsp:include>

	<!--  MAIN CONTAINER -->
	<div class="jumbotron">
		<h1 class="display-3 text-container-main">
			<spring:message code="home.welcome" />
			<sup>&reg</sup>
		</h1>
		<h3 class="text-container-main">
			<spring:message code="home.description" />
		</h3>
		<hr class="my-4 text-container-main">

		<div class="Row text-container-main">
			<div class="Column">
				<img src="resources/images/workshop_icon.png" class="icon">
			</div>
			<div class="Column">
				<h3>
					<spring:message code="workshops" />
				</h3>
				<p class="lead">
					<spring:message code="home.workshops.description" />
				</p>
				<form:form class="form-inline my-2 my-lg-0"
					action="getWorkshopsByName" method="POST">
					<input class="form-control mr-sm-2" type="text" name="workshopName" />
					<br>
					<div>
						<input class="btn btn-info btn-lg" type="submit"
							value=<spring:message code="home.workshops.description" /> />
					</div>
				</form:form>
			</div>
		</div>
		<c:if test="${showWorkshops == true}">
			<br>
			<c:set var="showWorkshops" scope="session" value="${false}" />
			<c:choose>
				<c:when test="${empty workshops}">
					<div
						class="alert alert-dismissible alert-primary text-container-main">
						<spring:message code="${getWorkshopsResponse }"/></div>
				</c:when>
				<c:otherwise>
					<div class="text-container-main">
						<table class="table table-hover">
							<thead>
								<tr class="table-info">
									<th scope="col"><spring:message code="id" /></th>
									<th scope="col"><spring:message code="name" /></th>
									<th scope="col"><spring:message code="address.street" /></th>
									<th scope="col"><spring:message code="address.city" /></th>
									<th scope="col"><spring:message code="address.postalcode" /></th>
									<th scope="col"><spring:message code="owner" /></th>
									<th scope="col"></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${workshops}" var="workshop">
									<tr class="table-light text-container-main">
										<td><c:out value="${workshop.id}" /></td>
										<td><c:out value="${workshop.name}" /></td>
										<td><c:out value="${workshop.address.street}" /></td>
										<td><c:out value="${workshop.address.city}" /></td>
										<td><c:out value="${workshop.address.postalCode}" /></td>
										<td><c:out value="${workshop.owner.userName}" /></td>
										<td><a href="/CarWorkshop/workshopView/${workshop.id }">
												<button type="button" class="btn btn-outline-info">
													<spring:message code="details" />
												</button>
										</a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</c:otherwise>
			</c:choose>
		</c:if>
		<hr class="my-4 text-container-main">

		<div class="Row text-container-main">
			<div class="Column">
				<img src="resources/images/control-point_icon.png" class="icon">
			</div>
			<div class="Column">
				<h3>
					<spring:message code="control_points" />
				</h3>
				<p class="lead">
					<spring:message code="home.control_points.description" />
				</p>
				<form:form class="form-inline my-2 my-lg-0"
					action="getVehicleControlPoint" method="POST">
					<input class="form-control mr-sm-2" type="text"
						name="controlPointName" />
					<br>
					<div>
						<input class="btn btn-info btn-lg " type="submit"
							value=<spring:message code="search" /> />
					</div>
				</form:form>
			</div>
		</div>
		<c:if test="${showControlPointsResponse == true}">
			<br>
			<c:set var="showControlPointsResponse" scope="session"
				value="${false}" />
			<c:choose>
				<c:when test="${empty controlPoints}">
					<div
						class="alert alert-dismissible alert-primary text-container-main">
						<spring:message code="${getControlPointsResponse }"/></div>
				</c:when>
				<c:otherwise>
					<div class="text-container-main">
						<table class="table table-hover">
							<thead>
								<tr class="table-info">
									<th scope="col"><spring:message code="id" /></th>
									<th scope="col"><spring:message code="name" /></th>
									<th scope="col"><spring:message code="address.street" /></th>
									<th scope="col"><spring:message code="address.city" /></th>
									<th scope="col"><spring:message code="address.postalcode" /></th>
									<th scope="col"><spring:message code="owner" /></th>
									<th scope="col"></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${controlPoints}" var="controlPoint">
									<tr class="table-light text-container-main">
										<td><c:out value="${controlPoint.id}" /></td>
										<td><c:out value="${controlPoint.name}" /></td>
										<td><c:out value="${controlPoint.address.street}" /></td>
										<td><c:out value="${controlPoint.address.city}" /></td>
										<td><c:out value="${controlPoint.address.postalCode}" /></td>
										<td><c:out value="${controlPoint.owner.userName}" /></td>
										<td><a
											href="/CarWorkshop/controlPointView/${controlPoint.id }">
												<button type="button" class="btn btn-outline-info">
													<spring:message code="details" />
												</button>
										</a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</c:otherwise>
			</c:choose>
		</c:if>
		<hr class="my-4 text-container-main">

		<div class="Row text-container-main">
			<div class="Column">
				<img src="resources/images/service_icon.png" class="icon">
			</div>
			<div class="Column">
				<h3>
					<spring:message code="car_services" />
				</h3>
				<p class="lead">
					<spring:message code="home.car_services.description" />
				</p>
				<form:form class="form-inline my-2 my-lg-0" action="getServices"
					method="POST">
					<select class="custom-select lead" style="margin-right: 10px; font-size: 14px" name="serviceCategory">
						<c:forEach items="${categories}" var="category">
							<option value="${category}" class="lead">
								<spring:message code="${category }"/>
							</option>
						</c:forEach>
					</select>
					<input class="form-control mr-sm-2" type="text" name="serviceName"
						width="200" />
					<br>
					<div>
						<input class="btn btn-info btn-lg" type="submit"
							value=<spring:message code="search" /> />
					</div>
				</form:form>
			</div>
		</div>
		<c:if test="${showServices == true}">
			<br>
			<c:set var="showServices" scope="session" value="${false}" />
			<c:choose>
				<c:when test="${empty carServices}">
					<div
						class="alert alert-dismissible alert-primary text-container-main">
						<spring:message code="${getServicesResponse }"/></div>
				</c:when>
				<c:otherwise>
					<div class="text-container-main">
						<table class="table table-hover">
							<thead>
								<tr class="table-info">
									<th scope="col"><spring:message code="id" /></th>
									<th scope="col"><spring:message code="name" /></th>
									<th scope="col"><spring:message code="workshop" /></th>
									<th scope="col"><spring:message code="price" /></th>
									<th scope="col"></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${carServices}" var="carService">
									<tr class="table-light">
										<td><c:out value="${carService.id}" /></td>
										<td><c:out value="${carService.name}" /></td>
										<td><c:out value="${carService.workshop.name }" /></td>
										<td><c:out
												value="${carService.cost * (100 - carService.discount) * 0.01 }" /> &#128</td>
										<td><a href="/CarWorkshop/serviceView/${carService.id }">
												<button type="button" class="btn btn-outline-info">
													<spring:message code="details" />
												</button>
										</a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</c:otherwise>
			</c:choose>
		</c:if>
		<hr class="my-4 text-container-main">

		<div class="Row text-container-main">
			<div class="Column">
				<img src="resources/images/accessory_icon.png" class="icon">
			</div>
			<div class="Column">
				<h3>
					<spring:message code="car_accessories" />
				</h3>
				<p class="lead">
					<spring:message code="home.car_accessories.description" />
				</p>
				<form:form class="form-inline my-2 my-lg-0" action="getAccessories"
					method="POST">
					<select class="custom-select lead" style="margin-right: 10px; font-size: 14px" name="accessoryCategory">
						<c:forEach items="${categories}" var="category">
							<option value="${category}" class="lead">
								<spring:message code="${category }"/>
							</option>
						</c:forEach>
					</select>
					<input class="form-control mr-sm-2" type="text"
						name="accessoryName" width="200" />
					<br>
					<div>
						<input class="btn btn-info btn-lg" type="submit"
							value=<spring:message code="search" /> />
					</div>
				</form:form>
			</div>
		</div>
		<c:if test="${showAccessories == true}">
			<br>
			<c:set var="showAccessories" scope="session" value="${false}" />
			<c:choose>
				<c:when test="${empty carAccessories}">
					<div
						class="alert alert-dismissible alert-primary text-container-main">
						<spring:message code="${getAccessoriesResponse }"/></div>
				</c:when>
				<c:otherwise>
					<div class="text-container-main">
						<table class="table table-hover">
							<thead>
								<tr class="table-info">
									<th scope="col"><spring:message code="id" /></th>
									<th scope="col"><spring:message code="name" /></th>
									<th scope="col"><spring:message code="workshop" /></th>
									<th scope="col"><spring:message code="price" /></th>
									<th scope="col"></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${carAccessories}" var="carAccessory">
									<tr class="table-light">
										<td><c:out value="${carAccessory.id}" /></td>
										<td><c:out value="${carAccessory.name}" /></td>
										<td><c:out value="${carAccessory.workshop.name }" /></td>
										<td><c:out
												value="${carAccessory.cost * (100 - carAccessory.discount) * 0.01}" /> &#128</td>
										<td><a
											href="/CarWorkshop/accessoryView/${carAccessory.id }">
												<button type="button" class="btn btn-outline-info">
													<spring:message code="details" />
												</button>
										</a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</c:otherwise>
			</c:choose>
		</c:if>
		<hr class="my-4 text-container-main">

		<div></div>

		<h3 class="text-container-main">
			<spring:message code="discounts" />
		</h3>
		<p class="lead text-container-main">
			<spring:message code="discounts.description" />
		</p>
		<div class="Row text-container-main">
			<c:forEach items="${discounts}" var="accessory">
				<div class="Column" style="padding-right: 25px">
					<a href="/CarWorkshop/accessoryView/${accessory.id }"><img
						class="img-main"
						src="${pageContext.request.contextPath}/images/${accessory.id}">
					</a>
					<hr class="my-4">
					<p>
						<b>${accessory.name }</b>
					</p>
					<p class="lead">
						<spring:message code="price" />
						:
						<del>${accessory.cost }</del>
						<b>${accessory.cost * (100 - accessory.discount) * 0.01} &#128</b>
					</p>
					<p class="lead">
						<spring:message code="discount" />
						: <b style="color: red;">${accessory.discount}%</b>
					</p>
				</div>
			</c:forEach>
		</div>
		<hr class="my-4 text-container-main">

		<h3 class="text-container-main">
			<spring:message code="best_sellers" />
		</h3>
		<p class="lead text-container-main">
			<spring:message code="home.best_sellers.description" />
		</p>
		<div class="Row text-container-main">
			<c:forEach items="${bestsellers}" var="bestseller">
				<div class="Column" style="padding-right: 25px">
					<a href="/CarWorkshop/accessoryView/${bestseller.id }"> <img
						class="img-main"
						src="${pageContext.request.contextPath}/images/${bestseller.id}">
					</a>
					<hr class="my-4">
					<p>
						<b>${bestseller.name }</b>
					</p>
					<p class="lead">
						<spring:message code="price" />
						: <b>${bestseller.cost * (100 - bestseller.discount) * 0.01} &#128</b>
					</p>
					<p class="lead">
						<spring:message code="sold" />
						: <b>${bestseller.amountSold }</b>
					</p>
				</div>
			</c:forEach>
		</div>

	</div>

	<!-- FOOTER -->
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>