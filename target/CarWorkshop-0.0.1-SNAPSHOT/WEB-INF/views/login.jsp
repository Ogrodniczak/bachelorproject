<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title><spring:message code="title" /></title>
<link href="resources/css/bootstrap.css" rel="stylesheet"
	type="text/css" />
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<!-- NAVIGATION BAR -->
	<jsp:include page="navbar.jsp"></jsp:include>

	<!--  MAIN CONTAINER -->
	<div class="jumbotron">
		<h1 class="display-3 text-container-main">
			<spring:message code="nav.login" />
		</h1>
		<hr class="my-4 text-container-main">
		
		<c:if test="${not empty loginError}">
			<div class="alert alert-dismissible alert-primary text-container-main">
				<spring:message code="${loginError}"/></div>
		</c:if>
		
		<form:form action="submitLogin" method="POST"
			modelAttribute="loginForm">
			<div class="Row text-container-main">
				<div class="Column-label">
					<form:label path="userName">
						<spring:message code="user.name" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2 input-block" type="text"
						path="userName" />
				</div>
				<%-- <form:errors path="userName"></form:errors> --%>
			</div>
			<div class="Row text-container-main">
				<div class="Column-label">
					<form:label path="password">
						<spring:message code="password" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2 input-block" type="password"
						path="password" />
				</div>
			</div>
			<%-- <form:errors path="password"></form:errors> --%>
			<hr class="my-4 text-container-main">
			<div class="Row text-container-main">
				<div class="Column-label">
					<input class="btn btn-info btn-lg" type="submit"
						value=<spring:message code="submit" /> />
				</div>
			</div>
		</form:form>
	</div>

	<!-- FOOTER -->
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
