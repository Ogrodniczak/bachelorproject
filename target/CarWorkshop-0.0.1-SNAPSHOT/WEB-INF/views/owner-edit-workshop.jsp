<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
<title><spring:message code="title" /></title>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/bootstrap.css" />" />
</head>
<body>
	<!-- NAVIGATION BAR -->
	<jsp:include page="navbar.jsp"></jsp:include>

	<!-- MAIN CONTAINER -->
	<div class="jumbotron">
		<h1 class="display-3 text-container-main">
			<spring:message code="workshop" />
			${workshopModel.name}
		</h1>
		<hr class="my-4 text-container-main">
		<c:choose>
			<c:when test="${not empty editWorkshopError}">
				<div
					class="alert alert-dismissible alert-primary text-container-main">
					<spring:message code="${editWorkshopError}"/></div>
			</c:when>
			<c:when test="${not empty editWorkshopSuccess}">
				<div
					class="alert alert-dismissible alert-success text-container-main">
					<spring:message code="${editWorkshopSuccess}"/> <a href="/CarWorkshop/ownerPage"
						class="alert-link"><spring:message code="owner.back" /></a>
				</div>
			</c:when>
			<c:when test="${not empty deleteAccessoryError}">
				<div
					class="alert alert-dismissible alert-primary text-container-main">
					<spring:message code="${deleteAccessoryError}"/></div>
			</c:when>
			<c:when test="${not empty deleteAccessorySuccess}">
				<div
					class="alert alert-dismissible alert-success text-container-main">
					<spring:message code="${deleteAccessorySuccess}"/></div>
			</c:when>
			<c:when test="${not empty deleteServiceError}">
				<div
					class="alert alert-dismissible alert-primary text-container-main">
					<spring:message code="${deleteServiceError}"/></div>
			</c:when>
			<c:when test="${not empty deleteServiceSuccess}">
				<div
					class="alert alert-dismissible alert-success text-container-main">
					<spring:message code="${deleteServiceSuccess}"/></div>
			</c:when>
			<c:when test="${not empty addStationSuccess}">
				<div
					class="alert alert-dismissible alert-success text-container-main">
					<spring:message code="${addStationSuccess}"/></div>
			</c:when>
			<c:when test="${not empty addStationError}">
				<div
					class="alert alert-dismissible alert-primary text-container-main">
					<spring:message code="${addStationError}"/></div>
			</c:when>
			<c:when test="${not empty deleteStationSuccess}">
				<div
					class="alert alert-dismissible alert-success text-container-main">
					<spring:message code="${deleteStationSuccess}"/></div>
			</c:when>
			<c:when test="${not empty deleteStationError}">
				<div
					class="alert alert-dismissible alert-primary text-container-main">
					<spring:message code="${deleteStationError}"/></div>
			</c:when>
		</c:choose>

		<form:form
			action="/CarWorkshop/updateWorkshopData/${workshopModel.id }"
			method="POST" modelAttribute="workshopModel">
			<h3 class="text-container-main">
				<spring:message code="info.general" />
			</h3>
			<div class="Row text-container-main">
				<div class="Column-label-long">
					<form:label class="lead" path="name">
						<spring:message code="name" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text" path="name"
						value="${workshopModel.name }" />
				</div>
				<form:errors path="name" />
			</div>
			<div class="Row text-container-main">
				<div class="Column-label-long">
					<form:label class="lead" path="description">
						<spring:message code="description" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text"
						path="description" value="${workshopModel.description }" />
				</div>
				<form:errors path="description" />
			</div>
			<div class="Row text-container-main">
				<div class="Column-label-long">
					<form:label class="lead" path="openingHour">
						<spring:message code="hour.opening" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text"
						path="openingHour" value="${workshopModel.openingHour}" />
				</div>
				<form:errors path="openingHour" />
			</div>
			<div class="Row text-container-main">
				<div class="Column-label-long">
					<form:label class="lead" path="closingHour">
						<spring:message code="hour.closing" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text"
						path="closingHour" value="${workshopModel.closingHour }" />
				</div>
				<form:errors path="closingHour" />
			</div>
			<br>
			<h3 class="text-container-main">
				<spring:message code="address" />
			</h3>
			<div class="Row text-container-main">
				<div class="Column-label-long">
					<form:label class="lead" path="address.street">
						<spring:message code="address.street" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text"
						path="address.street" value="${workshopModel.address.street }" />
				</div>
				<form:errors path="address.street" />
			</div>
			<div class="Row text-container-main">
				<div class="Column-label-long">
					<form:label class="lead" path="address.city">
						<spring:message code="address.city" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text"
						path="address.city" value="${workshopModel.address.city }" />
				</div>
				<form:errors path="address.city" />
			</div>
			<div class="Row text-container-main">
				<div class="Column-label-long">
					<form:label class="lead" path="address.postalCode">
						<spring:message code="address.postalcode" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text"
						path="address.postalCode"
						value="${workshopModel.address.postalCode }" />
				</div>
				<form:errors path="address.postalCode" />
			</div>
			<div class="Row text-container-main">
				<div class="Column-label">
					<input class="btn btn-info btn-lg" type="submit"
						value=<spring:message code="save"/> />
				</div>
			</div>
		</form:form>
		<hr class="my-4 text-container-main">

		<!-- STATIONS -->
		<h3 class="text-container-main">
			&#9873
			<spring:message code="stations" />
		</h3>
		<p class="lead text-container-main">
			<spring:message code="stations.amount" />
			: ${fn:length(workshopModel.stations)}
		</p>
		<div class="text-container-main">
			<a href="/CarWorkshop/ownerAddStation/workshop/${workshopModel.id }">
				<button class="btn btn-info btn-lg"><spring:message code="add"/></button>
			</a>
			<c:if test="${fn:length(workshopModel.stations) > 0}">
				<a
					href="/CarWorkshop/ownerRemoveStation/workshop/${workshopModel.id }">
					<button class="btn btn-info btn-lg">
						<spring:message code="delete" />
					</button>
				</a>
			</c:if>
		</div>
		<hr class="my-4 text-container-main">

		<!-- SERVICES -->
		<div class="Row text-container-main">
			<h3>
				<spring:message code="services" />
			</h3>
		</div>
		<br>
		<c:choose>
			<c:when test="${empty services}">
				<div class="alert alert-dismissible alert-primary text-container-main">
					<spring:message code="${getEmptyServicesResponse }"/></div>
			</c:when>
			<c:otherwise>
				<div class="text-container-main">
					<table class="table table-hover">
						<thead>
							<tr class="table-info">
								<th scope="col"><spring:message code="id" /></th>
								<th scope="col"><spring:message code="name" /></th>
								<th scope="col"><spring:message code="availability" /></th>
								<th scope="col"><spring:message code="discount" /></th>
								<th scope="col"><spring:message code="price" /></th>
								<th scope="col"><spring:message code="amount.sold" /></th>
								<th scope="col"></th>
								<th scope="col"></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${services}" var="service">
								<tr class="table-light">
									<td><c:out value="${service.id}" /></td>
									<td><c:out value="${service.name}" /></td>
									<td><spring:message code="${service.available}" /></td>
									<td><c:out value="${service.discount}" /></td>
									<td><c:out value="${service.cost * (100 - service.discount) * 0.01 }" /> &#128</td>
									<td><c:out value="${service.amountSold}" /></td>
									<td><a
										href="/CarWorkshop/ownerGetSaveServiceView/${workshopModel.id }/${service.id}"><button
												type="button" class="btn btn-outline-info">
												<spring:message code="edit" />
											</button></a></td>
									<td><a
										href="/CarWorkshop/ownerDeleteService/${workshopModel.id }/${service.id }"
										onclick="return confirm('Are you sure you want to delete this item?')">
											<button type="button" id="deleteService"
												class="btn btn-outline-primary">
												<spring:message code="delete" />
											</button>
									</a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</c:otherwise>
		</c:choose>

		<a href="/CarWorkshop/ownerGetSaveServiceView/${workshopModel.id }/0">
			<button class="btn btn-info btn-lg text-container-main"
				id="addService">
				<spring:message code="add" />
			</button>
		</a>
		<hr class="my-4 text-container-main">

		<!-- ACCESSORIES -->
		<div class="Row text-container-main">
			<h3>
				<spring:message code="accessories" />
			</h3>
		</div>
		<br>
		<c:choose>
			<c:when test="${empty accessories}">
				<div class="alert alert-dismissible alert-primary text-container-main">
					<spring:message code="${getEmptyAccessoriesResponse }"/></div>
			</c:when>
			<c:otherwise>
				<div class="text-container-main">
					<table class="table table-hover">
						<thead>
							<tr class="table-info">
								<th scope="col"><spring:message code="id" /></th>
								<th scope="col"><spring:message code="name" /></th>
								<th scope="col"><spring:message code="availability" /></th>
								<th scope="col"><spring:message code="discount" /></th>
								<th scope="col"><spring:message code="price" /></th>
								<th scope="col"><spring:message code="amount.sold" /></th>
								<th scope="col"></th>
								<th scope="col"></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${accessories}" var="accessory">
								<tr class="table-light">
									<td><c:out value="${accessory.id}" /></td>
									<td><c:out value="${accessory.name}" /></td>
									<td><spring:message code="${accessory.available}" /></td>
									<td><c:out value="${accessory.discount}" /></td>
									<td><c:out value="${accessory.cost * (100 - accessory.discount) * 0.01}" /> &#128</td>
									<td><c:out value="${accessory.amountSold}" /></td>
									<td><a
										href="/CarWorkshop/ownerGetSaveAccessoryView/${workshopModel.id }/${accessory.id}"><button
												type="button" class="btn btn-outline-info">
												<spring:message code="edit" />
											</button></a></td>
									<td><a
										href="/CarWorkshop/ownerDeleteAccessory/${workshopModel.id }/${accessory.id }"
										onclick="return confirm('Are you sure you want to delete this item?')">
											<button type="button" id="deleteService"
												class="btn btn-outline-primary">
												<spring:message code="delete" />
											</button>
									</a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</c:otherwise>
		</c:choose>
		<a
			href="/CarWorkshop/ownerGetSaveAccessoryView/${workshopModel.id }/0">
			<button class="btn btn-info btn-lg text-container-main"
				id="addAccessory">
				<spring:message code="add" />
			</button>
		</a>
		<hr class="my-4 text-container-main">

		<h3 class="text-container-main">
			&#128197 <spring:message code="calendar" />
		</h3>
		<p class="lead text-container-main">
			<spring:message code="calendar.workshop" />
		</p>
		<a
			href="/CarWorkshop/ownerGetCalendarView/workshop/${workshopModel.id }/-1">
			<button class="btn btn-info btn-lg text-container-main"
				id="showCalendar">
				<spring:message code="show" />
			</button>
		</a>
	</div>

	<!-- FOOTER -->
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>