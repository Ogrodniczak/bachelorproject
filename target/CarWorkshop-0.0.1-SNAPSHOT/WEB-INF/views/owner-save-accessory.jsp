<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Car Workshop Web Application</title>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/bootstrap.css" />" />
</head>
<body>
	<!-- NAVIGATION BAR -->
	<jsp:include page="navbar.jsp"></jsp:include>

	<!-- MAIN CONTAINER -->
	<div class="jumbotron">
		<h1 class="display-3 text-container-main">
			<spring:message code="accessory.save" />
		</h1>
		<hr class="my-4 text-container-main">
		<c:choose>
			<c:when test="${not empty saveAccessoryError}">
				<div
					class="alert alert-dismissible alert-primary text-container-main">
					<spring:message code="${saveAccessoryError}" />
				</div>
			</c:when>
			<c:when test="${not empty saveAccessorySuccess}">
				<div
					class="alert alert-dismissible alert-success text-container-main">
					<spring:message code="${saveAccessorySuccess}" />
					<a href="/CarWorkshop/ownerEditWorkshop/${workshopId }"
						class="alert-link"><spring:message code="owner.back.workshop" /></a>
				</div>
			</c:when>
		</c:choose>


		<form:form action="/CarWorkshop/saveAccessory/${workshopId}"
			method="POST" modelAttribute="accessoryModel">
			<form:input type="hidden" path="id" value="${accessoryModel.id }" />
			<div class="Row text-container-main">
				<div class="Column-label">
					<form:label class="lead" path="name">
						<spring:message code="name" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text" path="name"
						value="${accessoryModel.name }" />
				</div>
				<div class="Column error">
					<form:errors path="name" />
				</div>
			</div>
			<div class="Row text-container-main">
				<div class="Column-label">
					<form:label class="lead" path="available">
						<spring:message code="available" />:</form:label>
				</div>
				<div class="Column">
					<form:checkbox path="available" value="${accessoryModel.available}" />
				</div>
			</div>
			<div class="Row text-container-main">
				<div class="Column-label">
					<form:label class="lead" path="discount">
						<spring:message code="discount" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text"
						path="discount" value="${accessoryModel.discount}" />
				</div>
				<div class="Column error">
					<form:errors path="discount" />
				</div>
			</div>
			<div class="Row text-container-main">
				<div class="Column-label">
					<form:label class="lead" path="category">
						<spring:message code="category" />:</form:label>
				</div>
				<div class="Column-input lead">
					<form:select class="custom-select lead" path="category">
						<c:forEach items="${categories}" var="category">
							<form:option value="${category}">
								<spring:message code="${category }" />
							</form:option>
						</c:forEach>
					</form:select>
				</div>
				<div class="Column error">
					<form:errors path="category" />
				</div>
			</div>
			<div class="Row text-container-main">
				<div class="Column-label">
					<form:label class="lead" path="cost">
						<spring:message code="price" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text" path="cost"
						value="${accessoryModel.cost}" />
				</div>
				<div class="Column error">
					<form:errors path="cost" />
				</div>
			</div>
			<div class="Row text-container-main">
				<div class="Column-label">
					<form:label class="lead" path="amount">
						<spring:message code="amount" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text" path="amount"
						value="${accessoryModel.amount}" />
				</div>
				<div class="Column error">
					<form:errors path="amount" />
				</div>
			</div>
			<c:choose>
				<c:when test="${not empty uploadedImage }">
					<div class="Row text-container-main">
						<div class="Column">
							<img src="${pageContext.request.contextPath}/images/0"
								width="125" height="125">
						</div>
					</div>
				</c:when>
				<c:otherwise>
					<c:if test="${not empty accessoryModel.image }">
						<div class="Row text-container-main">
							<div class="Column">
								<img class="icon"
									src="${pageContext.request.contextPath}/images/${accessoryModel.id}"
									width="125" height="125">
							</div>
						</div>
					</c:if>
				</c:otherwise>
			</c:choose>
			<br>
			<div>
				<input class="btn btn-info btn-lg text-container-main" type="submit"
					value=<spring:message code="save"/> />
			</div>
		</form:form>
		<hr class="my-4 text-container-main">

		<div class="text-container-main">
			<h2>
				<spring:message code="accessory.image" />
			</h2>
			<br />
			<form:form method="POST" action="/CarWorkshop/UploadImageServlet"
				enctype="multipart/form-data">
				<input type="hidden" name="workshopIdHidden" value="${workshopId }" />
				<spring:message code="upload.select" />:  
					<input type="file" name="dataFile" id="fileChooser" />
				<br />
				<br />
				<input class="btn btn-info btn-lg" type="submit"
					value=<spring:message code="upload"/> />
			</form:form>
		</div>
		<hr class="my-4 text-container-main">
	</div>

	<!-- FOOTER -->
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>