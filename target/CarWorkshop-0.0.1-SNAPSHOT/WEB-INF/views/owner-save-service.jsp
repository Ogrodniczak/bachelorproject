<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title><spring:message code="title" /></title>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/bootstrap.css" />" />
</head>
<body>
	<!-- NAVIGATION BAR -->
	<jsp:include page="navbar.jsp"></jsp:include>

	<!-- MAIN CONTAINER -->
	<div class="jumbotron">
		<h1 class="display-3 text-container-main">
			<spring:message code="service.save" />
		</h1>
		<hr class="my-4 text-container-main">
		<c:choose>
			<c:when test="${not empty saveServiceError}">
				<div
					class="alert alert-dismissible alert-primary text-container-main">
					<spring:message code="${saveServiceError}" />
				</div>
			</c:when>
			<c:when test="${not empty saveServiceSuccess}">
				<div
					class="alert alert-dismissible alert-success text-container-main">
					<spring:message code="${saveServiceSuccess}" />
					<a href="/CarWorkshop/ownerEditWorkshop/${workshopId }"
						class="alert-link"><spring:message code="owner.back.workshop" /></a>
				</div>
			</c:when>
		</c:choose>

		<form:form action="/CarWorkshop/saveService/${workshopId}"
			method="POST" modelAttribute="serviceModel">
			<form:input type="hidden" path="id" value="${serviceModel.id }" />
			<div class="Row text-container-main">
				<div class="Column-label">
					<form:label class="lead" path="name">
						<spring:message code="name" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text" path="name"
						value="${serviceModel.name }" />
				</div>
				<div class="Column error">
					<form:errors path="name" />
				</div>
			</div>
			<div class="Row text-container-main">
				<div class="Column-label">
					<form:label class="lead" path="category">
						<spring:message code="category" />:</form:label>
				</div>
				<div class="Column-input lead">
					<form:select class="custom-select lead" path="category">
						<c:forEach items="${categories}" var="category">
							<form:option value="${category}">
								<spring:message code="${category }" />
							</form:option>
						</c:forEach>
					</form:select>
				</div>
				<div class="Column error">
					<form:errors path="category" />
				</div>
			</div>
			<div class="Row text-container-main">
				<div class="Column-label">
					<form:label class="lead" path="available">
						<spring:message code="available" />:</form:label>
				</div>
				<div class="Column">
					<form:checkbox path="available" value="${serviceModel.available}" />
				</div>
			</div>
			<div class="Row text-container-main">
				<div class="Column-label">
					<form:label class="lead" path="discount">
						<spring:message code="discount" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text"
						path="discount" value="${serviceModel.discount}" />
				</div>
				<div class="Column error">
				<form:errors path="discount" />
				</div>
			</div>
			<div class="Row text-container-main">
				<div class="Column-label">
					<form:label class="lead" path="cost">
						<spring:message code="price" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text" path="cost"
						value="${serviceModel.cost}" />
				</div>
				<div class="Column error">
					<form:errors path="cost" />
				</div>
			</div>
			<div class="Row text-container-main">
				<div class="Column-label">
					<form:label class="lead" path="time">
						<spring:message code="time" />:</form:label>
				</div>
				<div class="Column">
					<form:input class="form-control mr-sm-2" type="text" path="time"
						value="${serviceModel.time}" />
				</div>
				<div class="Column error">
					<form:errors path="time" />
				</div>
			</div>
			<br>
			<div>
				<input class="btn btn-info btn-lg text-container-main" type="submit"
					value=<spring:message code="save"/> />
			</div>
		</form:form>
		<hr class="my-4 text-container-main">
	</div>

	<!-- FOOTER -->
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>