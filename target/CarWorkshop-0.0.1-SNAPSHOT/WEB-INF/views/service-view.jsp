<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title><spring:message code="title" /></title>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/bootstrap.css" />" />
</head>
<body>
	<!-- NAVIGATION BAR -->
	<jsp:include page="navbar.jsp"></jsp:include>

	<!-- MAIN CONTAINER -->
	<div class="jumbotron">

		<c:if test="${ not empty addServiceSuccess}">
			<div
				class="alert alert-dismissible alert-success text-container-main">
				<spring:message code="${addServiceSuccess }"/></div>
		</c:if>
		<c:if test="${not empty addServiceError  }">
			<div
				class="alert alert-dismissible alert-primary text-container-main">
				<spring:message code="${addServiceError }"/></div>
		</c:if>
		<c:if test="${not empty addAccessorySuccess  }">
			<div
				class="alert alert-dismissible alert-success text-container-main">
				<spring:message code="${addAccessorySuccess }"/></div>
		</c:if>
		<c:if test="${not empty addAccessoryError  }">
			<div
				class="alert alert-dismissible alert-primary text-container-main">
				<spring:message code="${addAccessoryError }"/></div>
		</c:if>

		<c:if test="${service != null}">
			<div class="Row text-container-main">
				<div class="Column">
					<img src="/CarWorkshop/resources/images/service_icon.png"
						class="card border-dark mb-3">
				</div>
				<div class="Column">
					<h1 class="text-info">${service.name }</h1>
					<br>
					<spring:message code="workshop" />
					: <a href="/CarWorkshop/workshopView/${service.workshop.id }"><strong
						class="text-info">${service.workshop.name} <sup>&#9432</sup>
					</strong> </a>
					<p>
						<spring:message code="category"/>: <strong><spring:message code="${service.category }"/></strong>
					</p>
					<p>
						<spring:message code="price" />
						: <strong>${service.cost * (100 - service.discount) * 0.01 } &#128</strong>
					</p> <br>

					<spring:message code="service.amount.sold" />
					: <strong>${service.amountSold }</strong> <br>
					<c:choose>
						<c:when test="${service.available == true}">
							<p style="color: green">
								<strong><spring:message code="available" /></strong>
							</p>
						</c:when>
						<c:otherwise>
							<p style="color: red">
								<strong><spring:message code="not_available" /></strong>
							</p>
						</c:otherwise>
					</c:choose>
					<c:if test="${service.available == true}"><a href="/CarWorkshop/addServiceToCart/service-view/${service.id }">
						<button type="button" class="btn btn-outline-success">
							<spring:message code="order.add" />
						</button>
					</a>
					</c:if>
				</div>
			</div>
		</c:if>
		<hr class="my-4 text-container-main">
	</div>

	<!-- FOOTER -->
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>