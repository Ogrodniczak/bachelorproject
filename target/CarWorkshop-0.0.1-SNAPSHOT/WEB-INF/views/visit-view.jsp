
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title><spring:message code="title" /></title>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/bootstrap.css" />" />
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<!-- NAVIGATION BAR -->
	<jsp:include page="navbar.jsp"></jsp:include>

	<div class="jumbotron">
		<h1 class="display-3 text-container-main">
			<spring:message code="visit.info" />
		</h1>
		<hr class="my-4 text-container-main">

		<div class="Row text-container-main">
			<div class="Column-label-long">
				<spring:message code="date" />
				:
			</div>
			<div class="Column">${visit.dayOfVisit.date }</div>
		</div>
		<div class="Row text-container-main">
			<div class="Column-label-long">
				<spring:message code="hours"/>:
			</div>
			<div class="Column">${visitHours}</div>
		</div>
		<div class="Row text-container-main">
			<div class="Column-label-long">
				<spring:message code="workshop"/>:
			</div>
			<div class="Column">${visit.order.workshop.name}</div>
		</div>
		<div class="Row text-container-main">
			<div class="Column-label-long">
				<spring:message code="accessories"/>:
			</div>
			<div class="Column">
				<c:forEach items="${visit.order.accessories }" var="accessory">
					${accessory.key.name } x${accessory.value} 
				</c:forEach>
			</div>
		</div>
		<div class="Row text-container-main">
			<div class="Column-label-long">
				<spring:message code="services"/>:
			</div>
			<div class="Column">
				<c:forEach items="${visit.order.services }" var="service">
					${service.key } x{$service.value} 
				</c:forEach>
			</div>
		</div>
		<div class="Row text-container-main">
			<div class="Column-label-long">
				<spring:message code="status"/>:
			</div>
			<div class="Column">
				<c:if test="${visit.status == 'SCHEDULED'}">
					<div style="color: green"><spring:message code="SCHEDULED"/></div>
				</c:if>
				<c:if test="${visit.status == 'TERMINATED'}">
					<div style="color: red"><spring:message code="TERMINATED"/></div>
				</c:if>
			</div>
		</div>
		<hr class="my-4 text-container-main">
	</div>
	<!-- FOOTER -->
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>